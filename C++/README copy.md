## 运行环境

### Windows

win下载编译环境地址：

`https://sourceforge.net/projects/mingw-w64/files/mingw-w64/mingw-w64-release/`

选择

 `[x86_64-win32-seh](https://sourceforge.net/projects/mingw-w64/files/Toolchains targetting Win64/Personal Builds/mingw-builds/8.1.0/threads-win32/seh/x86_64-8.1.0-release-win32-seh-rt_v6-rev0.7z)`

或是

`[x86_64-win32-sjlj](https://sourceforge.net/projects/mingw-w64/files/Toolchains targetting Win64/Personal Builds/mingw-builds/8.1.0/threads-win32/sjlj/x86_64-8.1.0-release-win32-sjlj-rt_v6-rev0.7z)`

均可。

## 工具配置

### VS Code

前置：安装 `C++` 的编译环境并配置全局变量。

安装插件：搜索 `C++`

* 基本语法插件：`C/C++`
* 扩展插件：`C/C++ Extension Pack`
* 主题插件：`C/C++ Themes`
* 运行插件：`CMake`
* 运行的工具插件：`CMake Tools`

配置`vscode`中环境：

## 学习路线

### 语言基础

#### 指针和内存管理

#### 基本语法