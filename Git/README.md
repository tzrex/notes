官方文档：`https://git-scm.com/book/zh/v2`。

### 初始化仓库

```bash
# 将当前文件夹创建一个git版本仓库
git init
```

### 状态

```bash
# 查看当前git运行的状态
# 只要当前工作区与最新版本的文件有差异都能监听到
git status
```

### 查看

#### 查看文件修改

```bash
# 可通过该命令查看文件被更改的部分
git diff

# 通常显示的内容：
diff --git a/xx.txt b/xx.txt
## 上面的表示两个不同版本的 xx.txt 文件进行对比
index 60be2d8..49f7672 100644
## 上面的乱码表示文件的 id；后面数字表示文件类型
--- a/xx.txt
+++ b/xx.txt
## 表示放弃了 a 版本的 xx.txt，新增了 b 版本的 xx.txt
@@ -1,+1 @@
XXXXX
YYYYY
## 修改的文件行，以及具体修改信息
```

#### 文件代码对应类型

``` bash
# 100644	普通文件
# 100755	可执行文件
# 120000	符号链接
```

#### 查看历史

```bash
# 可用改命令查看历史提交记录
# 如果展示内容不全，可用键盘的下键追加查看
git log

# 更全面的查看
# 此命令不止能看到提交记录，也能看到回滚记录
git reflog
```

#### 查看Config

```shell
# 查看系统配置
git config --system --list

# 查看当前用户配置
git config --global --list
# 配置用户信息
git config --global user.{信息} "XXX"

# 查看当前仓库配置信息
git config --local --list
```

### 修改

```shell
# 修改用户配置
```

### 提交

```bash
# 从暂存区提交到 git 的版本仓库中
git commit
# 请务必使用 -m 命令将每次操作了什么备注清晰
# 以方便回滚操作
git commit -m "请详细记录"
```

### 回滚

```bash
# 版本回滚，当出现错误或文件被他人恶意删除或其他情况导致时可用此挽回损失

# 通过 log 来查看历史提交及commit的对应ID
git log
# --hard 表示强制执行
git reset --hard <commit版本号>
```

<strong>版本的回滚相当于跳转，可以通过此命令任意跳转到任意版本</strong>。

#### 暂存区回滚

```bash
# 当提交到暂存区发现文件有问题想撤销时，可使用以下操作
git reset HEAD

# 也可以只撤销某个文件的add操作
# 撤销 xx.txt 文件的 git add 操作
git reset HEAD xx.txt

# 对暂存区的 xx.txt 文件的 add 操作进行撤销
# 再 git status 时不会有暂存区的提示
```

#### 文件操作回滚

```bash
# 虽然现在的编译器都有代码提示来告诉你对文件进行了什么操作
# 但不排除某些情况下，我们不知道我们对文件进行了什么操作

# 撤销 xx.txt 文件的操作
git checkout -- xx.txt

# 撤销所有文件的操作
git checkout -- .
```

#### 版本回滚

```bash
# 关键字
reset

# 命令使用
git reset --hard {版本ID}

## 回退至上个版本
git reset --hard HEAD^
git reset --hard HEAD~1

## 查看所有版本
## HEAD{1} 就是回退前的最新版本
git reflog

## 输入最新版本的 id 进行版本恢复
git reset --hard {最新版本号的id}
```

### 连接远程库

#### 创建Key

```bash
# 创建个人对应邮箱的对应 key
# abc@email.com 是你个人的邮箱号
ssh-keygen -t rsa -C "abc@email.com"
```

#### 远程库记录密钥

> 公钥对应的是物理机而不是账号，也就是说你在不同地方想要操作你的远程库，必须保证物理机生成的SSH在远程库中有保存

##### github

```bash
# setting —— SSH and GPG keys —— New SSH key
# Title	写密钥的备注
# Key	辅助你生成的公钥
# 之后点击 “Add SSH key”
```

##### gitee

```bash
# 设置 —— 安全设置 —— ssh公钥
# 标题为该公钥的备注
# 将生成的公钥复制到 "公钥" 文本框中，并 “确定”。
```

#### 关联远程库

##### 创建远程库地址

```bash
# 添加一个 git@xxxxx/yy/zz.git 的远程库地址并命名为 origin
git remote add origin git@xxxxx/yy/zz.git

# 查看远程库地址列表
git remote -v
```

##### 远程库合并到本地库

```bash
# 此操作可以将远程库的文件
# 从名为 origin 的远程库中合并文件到本地的 master 分支
git pull origin master

# 初次操作时会进行一个远程库确认操作， yes 即可
```

#### 推送到远程库

```bash
# 推送到远程库
# 将本地 master 分支推送到 远程 master 分支
git push origin master

# 推送到远程库的指定分支
# 推送到 远程 origin 分支
git push origin master:origin
```

#### 复制远程库文件

```bash
# 此操作也可复制别人的公开库
# 复制 git@xx/yy/zz.git 远程库的文件
git clone git@xx/yy/zz.git

# 也可对复制的进行更名
# 更名为 demo
git clone git@xx/yy/zz.git demo
```

#### 删除远程库

```bash
# 同样时使用 remote 关键字进行操作
# 删除了 origin 这个远程库
git remote rm origin
```

#### 从上游库同步

> 当我们在使用一些开源框架的时候，框架的每一次更新的差异可以使用

```shell
# 添加上游库的地址
## 我使用的是 cool 的开源二封框架
git remote add upreach https://gitee.com/cool-team-official/cool-admin-midway.git

# 从上游仓库获取节点和分支
git fetch upreach

# 切换到本地分支
# 分支看个人情况，通常默认是 master 分支
git checkout master

# 从远端仓库提取数据并尝试合并到当前分支
# 将上游的数据合并到 master 分支
git merge upreach/master

# 上传到远程库
git push
```

