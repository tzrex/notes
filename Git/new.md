## 前言

### 安装

1. 安装地址：`https://git-scm.com/download/win`. 选择自己匹配的版本即可

2. 使用命令行工具下载

   ```shell
   # 使用 winget 工具下载
   winget install --id Git.Git -e --source winget
   ```

### 是什么

**分布式版本控制系统**。

特点：每个参与者本地都有完整的备份，不需要担心仓库被破坏之后产品受损。只需要由任意参与者传递即可恢复

### 注意

`git` 是文本文件的版本管理系统。只管理能通过 记事本 编译器 等能直接修改的文件。

而例如多媒体文件时无法管理的。这类文件 `git` 只会使用新的替换旧的，无法进行回滚操作。

## 开始

### 工作常用指令

```shell
# 初始化，直接创建一个版本控制仓库
git init

# 添加远程仓库
# origin 是保存地址的变量
git remote add origin {lab地址}

# 查看远程库
git remote -v

# 拉去公开仓库项目
git clone {lab 地址}
# 拉去指定分支
git clone -b {分支名} {lab 地址}
# 拉取后修改文件夹名称
git clone {lab 地址} {dirName}

# 添加改动文件
# 添加某个文件
git add {file}
# 添加所有改动文件
git add .

# 提交代码
git commit -m "备注信息"

# 提交至远程仓库
# 将本地 master 分支的最新版本推送至 origin 变量的地址
git push origin master
```

### 用户管理

### 回滚

