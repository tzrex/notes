## docker 概念

### 镜像(image)

**类似于一个模板，可以通过模板创建容器服务。一个镜像可以创建多个容器**

#### 是什么

轻量级、可执行的独立软件包。用来打包软件运行环境和基于运行环境的开发软件，包含运行某个软件所需的所有内容（代码、运行时、库、环境变量、配置文件）

### 容器(container)

**可通过容器独立运行一个或一组应用。通过镜像来创建**

**一个容器可以理解为一个简易的Linux系统**

* 启动
* 停止
* 删除

### 仓库(repository)

**仓库就是存放镜像的地方。分为 公有仓库 和私有仓库**

Docker hub：看理解为 `github` 跟 `git` 的关系

### docker安装

```shell
# 卸载旧版本
yum remove docker \
    docker-client \ 
    docker-client-latest \ 
    docker-common \ 
    docker-latest \ 
    docker-latest-logrotate \ 
    docker-engine
    
# 安装新版本 yum 工具
sudo yum install -y yum-utils

# 设置镜像仓库 (默认官方镜像)
sudo yum-config-manager \
    --add-repo \
    https://download.docker.com/linux/centos/docker-ce.repo

# 安装 docker 相关
yum install docker-ce docker-ce-cli containerd.io

# 启用 docker
sudo systemctl start docker

# 设置开机自启
sudo systemctl enable docker

# 卸载
sudo yum remove docker-ce docker-ce-cli containerd.io
rm rf /var/lib/docker
```

### 启动与重启

```shell
# 启动 docker
systemctl restart docker
```



## Docker 常用命令

**Docker 全部命令可移步官网文档查看：`docs.docker.com`**

### 帮助命令

```shell
docker version # 版本信息
docker info # 显示docker的系统信息
docker {命令} --help # 帮助命令 查看 {命令API} 的所有命令
```

### 镜像命令

```shell
# 查看docker 所有镜像
docker images

# 搜索镜像 例如mysql
docker search mysql

# 下载镜像 例如下载mysql
docker pull mysql

#下载指定版本 例如下载5.7版本
docker pull mysql:5.7

# 删除镜像 (删除所有)
# rmi = remove image
docker rmi -f

#删除镜像 （ID删除）
docker rmi {镜像ID}
```

### 容器命令

**有镜像才能创建容器**！！！所以必须先 pull

容器就是镜像运行起来的服务器

#### 新建并启动

```shell
# 新建命令
docker run {参数} image
## 可选参数
--name="yourName"	启动的容器名称
-d					后台方式运行
-it					使用交互的方式运行，启动并进入容器
-p 访问端口:映射端口	指定容器端口 例如：-p 8080:8080
-p					随机端口
-v					卷挂载（映射）
-e					环境配置

# 查看当前所有运行的容器
docker ps 

# 退出容器并推出
exit

# 推出容器但不停止（快捷键
Ctrl + p + q

# 删除指定容器
docker remove {容器ID}

# 强制删除
docker remove -f {容器id}
```

#### 启动和停止

```shell
docker start {id} # 启动某个容器
docker restart {id} # 重启某个容器
docker stop {id} # 停止某个容器
docker kill {id} # 强制停止某个容器
```

#### 进入容器

```shell
# 方式一：进入容器并开启新终端
docker exec -it {ID} bashShell

# 方式二：进入容器，但不开启终端，直接在进程中执行
docker attach {ID}
```

#### 容器内的操作

* 拷贝文件到主机上

  ```shell
  # docker cp {ID}:容器内路径:主机路径
  # 将容器内 home 文件夹下的文件拷贝到主机 /home/nginx/data 上
  docker cp fdafsa231:/home /home/nginx/data
  
  ```

  

### 常用的其他命令

#### docker操作实例

```shell
# 例子：mysql8.0的启动方法
# -v 路径后面面的 :ro 是设置权限
# ro: read only ;  rw: read waite
docker run -d -p 3310:3306 -v /home/mysql:/mysql/data:ro -e MYSQL_ROOT_PASSWORD=12345678 --name shxs mysql:8.0

# 例子：启动nginx并映射文件
docker run -d -p 4455:80 -v /data/nginx/conf/nginx.conf:/etc/nginx/nginx.conf -v /data/nginx/html:/usr/share/nginx/html -v /data/nginx/logs:/var/log/nginx --name mynginx nginx:1.20
```

#### 基本命令

```shell
# 命令 docker run -d {镜像名}
docker run -d centos;
# 问题：docker ps. 发现 centos停止了
# 问题来源：后台镜像运行时，必须有个前台镜像来映射。当没有前台镜像时，则会自动停止

# 查看日志
docker logs

# 测试连接
curl localhost:8080

# 查看容器详细信息
docker inspect {id}

# 查看所有卷的挂载情况
docker volume ls

# 查看挂载详情
docker volume inspect {VOLUME NAME}

# 进入正在运行的容器
## 进入容器并开启新终端
docker exec -it {ID} bashShell
## 进入容器，不开启终端直接在运行的环境中
docker attach {ID} 

# 查看 docker 网络
docker network ls
```

#### 外网能访问流程

1. 阿里云通行端口号（后台配置）
2. docker 启动容器（run）并配置到允许访问的端口

## Docker 数据卷

### 是什么

**容器之间的数据共享**。Docker容器所产生的数据，同步到本地（其他文件夹）。

保证在删除容器时，数据不会丢失

### 使用方法

> 方式一：使用命令的方式挂载 -v

```shell
# 在 run 时通过 -v 参数来同步数据目录
docker run mysql -v {服务器目录}:{容器内目录}
```

当在此容器内进行操作所产生的数据（文件）会直接在 `local` 中生成。可以理解为 双向绑定。

> 方式二：在 Dockerfile 文件里面进行挂载使用

```shell
# ./ Dockerfile
volume 
```

### 具名挂载匿名挂载

```shell
# 匿名挂载
-v 容器内路径
# 例子
docker run -d -p 4455:80 -v /home/nginx nginx

# 具名挂载
-v 名称:容器内路径
# 例子
docker run -d -p 4455:80 -v juming-nginx:/etc/nginx --name nginx02 nginx
```

可以通过 `docker volume ls` 查看：

```shell
DRIVER    VOLUME NAME
# 匿名挂载的，名称随机
local	acaa5315e0841e9861c85841db7a17ed82577e6ce02ac793bf58f477e16cf561

# 具名挂载的，显示你设置的名称
local     juming-nginx
```

查看挂载的详细信息：

```shell
# 使用 docker volume inspect {VOLUME NAME}

# 打印的信息
[root@4545 ~]# docker volume inspect juming-nginx
[
    {
        "CreatedAt": "2021-07-22T11:36:55+08:00",
        "Driver": "local",
        "Labels": null,
        "Mountpoint": "/var/lib/docker/volumes/juming-nginx/_data",
        "Name": "juming-nginx",
        "Options": null,
        "Scope": "local"
    }
]

# 挂载的具体位置
Mountpoint 项就是具体挂载的位置
```

**拓展**：

```shell
# 在 -v 的挂载路径后面还可使用 :{权限指令} 进行权限设置
# :ro 指设置为只读权限，无法更改。
docker run -d -p 3310:3306 -v /home/mysql:/mysql/data:ro --name shxs mysql:8.0
```

### 容器之间数据同步

> --volumes-from 参数

```shell
# 拉去镜像
docker pull mysql:8.0;

# 启动一个mysql的容器
docker run -d -v /home/data/mysql:/etc/data --name mysql01 mysql:8.0

# 第二个容器与第一个容器的数据同步
# 同步 -v 参数后的文件
docker run -d --name mysql02 --volumes-from mysql01
```

## 认识 Dockerfile

**构建 docker 镜像的构建文件！**通过该脚本可以生成一个镜像

### 概念

`DockerFile`：构建文件，定义了一切的步骤的源代码。

`DockerImages`：通过 `DockerFile` 构建生成的镜像，最终发布和运行。

1. 注意：每一个关键字都必须大写。
2. 从上至下执行。
3. 每一个指令都会创建并提交一个新的镜像层。

#### 构建步骤

1. 编写 `Dockerfile` 文件
2. `dokcer build`  构建自己的镜像
3. `docker run`  运行镜像成容器
4. `docker push`  发布镜像（发布到 `dockerhub` 或者其他第三方docker代理平台）

### 基本指令

```shell
FROM 		# 基础镜像 （拉取的镜像，可前往 hub.docker.com 查看）

MAINTAINER	# 镜像作者（一般为姓名+邮箱）

RUN 		# 镜像（打包的项目）运行时需要的命令（例如npm run dev）

ADD			# 添加的其他包（本地有的）

WORKDIR		# 镜像的工作目录（在哪个目录运行）

VOLUME		# 数据挂载的目录（参考上面的 -v）

EXPOSE		# 暴露端口	有此命令则不需要使用 -p 来设置端口

CMD			# 容器启动时需要的运行命令（只有最后一个生效）

ENTRYPOINT	# 容器启动时需要的运行命令（可追加命令）

ONBUILD		# 当构建一个被继承 DockerFile 就会被执行

COPY		# 将文件复制到镜像中

ENV			# 构建的时候设置环境变量（参考上面的 -e）
```

#### CMD 和 ENTRYPOINT

```shell
CMD			# 指定容器启动的命令，但只有最有一个生效
ENTRYPOINT	# 指定容器启动的命令，可以追加命令
```

**例子**：

```dockerfile
# 编写个简单的 dockerfile 文件
FROM centos:8.0
CMD ["ls", "-a"]
```

执行

```shell
# 默认会寻找名为 dockerfile 的文件
# docker build -f {文件名}
docker build .
docker run {容器} -l	# error
# -l 覆盖了cmd命令。想追加的话，需要更改为 ENTRYPOINT
```

### 命令详情

```shell
# 命令格式：docker build [option] PATH URL
# option detail

```

### 基本实例

> 方式一：通过 shell 命令行书写

``` shell
# 进入你想保存的文件夹
cd /home/docker-demo

# 编写脚本文件
vim dockerfile_01

# nginx实例：
FROM centos8.4					# 拉去镜像
VOLUME ["VOLUME01", "VOLUME02"]	# 对应容器内的路径
CMD echo "___start___"			# 打印操作（使用 CMD 命令）
CMD /bin/bash					# 进入容器默认启动的命令集

# centos实例：
FROM centos8.4
MAINTAINER rex<123456789@qq.com>
ENV {MYPATH} /usr/local
RUN ["yum", "install", "net-tools", "-y"]
EXPOSE 8080:80
CMD echo $MYPATH
CMD /bin/bash

# 退出保存
:wq

# 查看文件 cat
cat dockerfile_01

# 打包生成文件
docker build -f dockerfile_01
```

## Docker 网络

## 发布自己的镜像

### Dockerhub

Docker 官方的镜像库网站。类似于 `git` 和 `github`。

1. 注册并登录

2. 在服务器上提交镜像

   ```shell
   # 在自己的服务器的docker上登陆账号
   # docker login [Options] [SERVICE]
   docker login -u {username} -p {password}
   ```

   如果报 `no such host` 错误

   ```shell
   # 添加docker服务地址
   # 通常在 /etc/reslov.conf 里面
   # 添加: nameserver 8.8.8.8
   ```

3. 提交镜像

   ```shell
   # 查看所有镜像
   docker images
   
   # 推送镜像
   # 可以是 ID 也可以是名字
   docker push  {dockerName}/{镜像}
   ```

   

## Docker Compose

### 作用

**定义、运行多个容器**。批量容器编排

### 重要概念

* 服务，容器，（web；redis；mysql）
* 项目project。  一组关联的容器。（web，MySQL）

### 安装

```bash
# 1.27.4之后不能再使用例如 gitee 之类的存放库
# 下载 compose 安装包。推荐使用国内的镜像或他人维护的镜像。
# 主要是官方镜像太慢了。
sudo curl -L "https://get.daocloud.io/docker/compose/releases/download/1.27.4/docker-compose-$(uname -s)-$(uname -m)" -o /usr/local/bin/docker-compose

# 授权（添加环境变量）
sudo chmod +x /usr/local/bin/docker-compose

# 检查: 通过查看版本号来确定
docker-compose --version
```

### 基本命令

```shell
# 启动 compose 项目
docker-compose up

# 停止 compose 项目（需要在项目执行的目录下）
# 一键启停定义的所有运行起来的容器
docker-compose down

```

### 实例演示

**演示一个简单的使用compose启动python项目**。

#### 创建入口文件

```python
import time
  
import redis
from flask import Flask

app = Flask(__name__)
cache = redis.Redis(host='redis', port=6379)


def get_hit_count():
    retries = 5
    while True:
        try:
            return cache.incr('hits')
        except redis.exceptions.ConnectionError as exc:
            if retries == 0:
                raise exc
            retries -= 1
            time.sleep(0.5)


@app.route('/')
def hello():
    count = get_hit_count()
    return 'Hello World! I have been seen {} times.\n'.format(count)

```

#### 创建python文档

```shell
# 创建文件 requirements.txt
vim requirements.txt

# 输入内容
flask
redis

# 保存退出
:wq
```

#### 创建docker镜像

```shell
# 创建一个 Dockerfile 文件
vim Dockerfile

# 写入内容
# syntax=docker/dockerfile:1
FROM python:3.7-alpine
WORKDIR /code
ENV FLASK_APP=app.py
ENV FLASK_RUN_HOST=0.0.0.0
RUN apk add --no-cache gcc musl-dev linux-headers
COPY requirements.txt requirements.txt
RUN pip install -r requirements.txt
EXPOSE 5000
COPY . .
CMD ["flask", "run"]

```

#### 创建compose文件

```shell
# 创建一个 docker-compose 文件
vim docker-compos.yml

# 写入内容
version: "3.8"
services:
  web:
    build: .
    ports:
      - "5000:5000" 
  redis:
    image: "redis:alpine"

```

### yml规则

