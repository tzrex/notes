# 服务器部署

服务器系统： `ubantu@22（Linux@5.15）`

## 防火墙

> 重启前记得开放22端口或者默认允许外部访问，否则重启后无法连接远程服务器

`ufw` 是一个基于 `iptables` 的一个防火墙工具

### 基本指令

* 查看防火墙状态：`sudo ufw status`，查看时加上编号：`sudo ufw status numbered`
* 开启防火墙：`sudo ufw enable`
* 关闭防火墙：`sudo ufw disable`
* 重启防火墙：`sudo ufw reload`
* 默认允许（拒绝）外部访问：`sudo ufw default allow (deny)`
* 允许（拒绝）访问某个（例如80）端口：`sudo ufw allow 80`（允许）`sudo ufw deny 80`（拒绝）
* 删除策略：`sudo ufw delete {numbered}`

### 开放端口

* 开启 `ssh` 默认端口：`sudo ufw allow 22`
* 开启 `http` 默认端口：`sudo ufw allow 80`
* 开启 `https` 默认端口：`sudo ufw allow 443`

## WEB服务器

> 这里使用的是 `nginx@1.22.1`。

本人的安装路径：

* 源码：`/home/nginx1.22.1`

### 获取

1. 下载压缩包：`curl -O http://nginx.org/download/nginx-1.22.1.tar.gz`
2. 解压压缩包：`tar -zxvf nginx-1.22.1.tar.gz`
3. 删除压缩包：`rm nginx-1.22.1.tar.gz`
4. 进入源码包：`cd ./nginx-1.22.1`

### 安装必须的包

* 安装C语言环境：`sudo apt-get install gcc`

* 安装`perl`函数库：`sudo apt-get install libpcre3 libpcre3-dev`

  编译环境需要开发模式，所以需要再安装 `-dev`，其他同理.

* 安装安全通信包：`sudo apt-get install openssl libssl-dev`

  想使用 `https` 、`http2` 需要此模块

* 安装`zlib` 包：`sudo apt-get install zlib1g zlib1g-dev`

* 安装编译包：`sudo apt-get install make`

  因为解压的包中有一个 `Makefile` 文件，那是官方提供的编译脚本。我们需要 `make` 命令执行

### 编译

> `nginx` 的默认安装位置在 `/usr/local/nginx`下

编译配置项文档地址：`https://nginx.org/en/docs/configure.html`.

```bash
  nginx path prefix: "/usr/local/nginx"
  nginx binary file: "/usr/local/nginx/sbin/nginx"
  nginx modules path: "/usr/local/nginx/modules"
  nginx configuration prefix: "/usr/local/nginx/conf"
  nginx configuration file: "/usr/local/nginx/conf/nginx.conf"
  nginx pid file: "/usr/local/nginx/logs/nginx.pid"
  nginx error log file: "/usr/local/nginx/logs/error.log"
  nginx http access log file: "/usr/local/nginx/logs/access.log"
  nginx http client request body temporary files: "client_body_temp"
  nginx http proxy temporary files: "proxy_temp"
  nginx http fastcgi temporary files: "fastcgi_temp"
  nginx http uwsgi temporary files: "uwsgi_temp"
  nginx http scgi temporary files: "scgi_temp"
```

* 编译

  加载 `https` 和`http2` ：` ./configure --with-http_ssl_module --with-http_v2_module`.

  执行编译：`make`

* 安装：`make install`。（直接安装到默认路径）

### 配置指令

* 编写指令脚本： `vim /lib/systemd/system/nginx.service`. 

  ```bash
  [Unit]
  Description=nginx
  After=network.target
  
  [Service]
  Type=forking
  ExecStart=/usr/local/nginx/sbin/nginx
  ExecReload=/usr/local/nginx/sbin/nginx -s reload
  ExecStop=/usr/local/nginx/sbin/nginx -s quit
  PrivateTmp=true
  
  [Install]
  WantedBy=multi-user.target
  ```

  这样就能使用 `systemctl` 统一管理

* 设置开机自启

  ```shell
  sudo systemctl enable nginx.service
  ```

* 配置 `https`

  1. 从任意厂商获取 `ssl` 证书（阿里云免费证书、`lets encrypt` 都可以）

  2. 上传服务器：`scp ./xxxx.pem {your service}` or `scp ./xxxx.key {your service}`

  3. 将证书配置到 `nginx` 中，`http` 模块或 `http/server` 模块均可。

     ```nginx
     ssl_certificate xxx.pem;
     ssl_certificate_key xxx.key;
     ```

  4. 在监听的 `443` 端口后添加 `ssl` 和 `http2` 的标签

### 基本指令

* 重启：`systemctl restart nginx`
* 停止：`systemctl stop nginx`
* 查看状态：`systemctl status nginx`

## Docker

> Docker 桥接模式会修改 `iptables`，所以不需要修改以 `iptables` 为基础的防火墙

* 清除可能的旧的 `Docker` ：

  ```shell
  # 循环删除下列包
  for pkg in docker.io docker-doc docker-compose podman-docker containerd runc; do sudo apt-get remove $pkg; done
  ```

* 更新 `apt` 的包源：`sudo apt update`

* 添加 `Docker` 包源：

  ```shell
  sudo add-apt-repository "deb [arch=amd64] https://download.docker.com/linux/ubuntu $(lsb_release -cs) stable"
  ```

* 安装 `Docker`：`sudo apt install docker docker-ce`

* 检查安装：`docker --version` 和 `docker compose version`

* 配置开机自启：`sudo systemctl enable docker `

## Git

> 我是习惯性将项目代码存放到服务器的 `/home/project` 目录下

* 下载：`sudo apt install git`
* 与远程代码库建立连接
  * 新建密钥：`ssh-keygen -t rsa -b 2048 -C "your@email"`。之后一路默认
  * 远程库添加公钥 `id_rsa.pub` ，私钥不管它。`cat ~/.ssh/id_rsa.pub`，查看公钥
    * `Github`：浏览器输入 `https://github.com/settings/keys`，之后 `new SSH key` 将上述打印出来的公钥保存
    * `Gitee`：浏览器输入 `https://gitee.com/profile/account_information`，`SSH公钥`，将上述公钥复制至输入框中点击确定。

* 拉取代码走远程库的 `ssh` 选项

## 数据库

> 使用 Docker 构建

* 拉取镜像：`git@github.com:tzRex/installdb.git` 直接部署 `mysql` 和` redis` 。
* 若想添加新的数据库，通过操作容器内的数据库进行新增

# SSL

## 证书

### 服务商处获取

> 此处以阿里云为案例

登录之后搜索：`数字证书管理服务(SSL证书)` 。

1. 点击 `SSL证书` 菜单。
2. 选择 `免费证书` 选项。
3. 首次进入选择 `立即购买`，选择 `个人测试证书（免费证书）` 选项，之后零元购。
4. 选择 `创建证书` ，输入需要证书的子域名。
5. 获取证书：
   * 如果服务器是阿里云的，则可选择 `部署` 操作。
   * 如果部署阿里云的，可点击 `下载` ，选择对应代理服务器软件之后下载。
6. 将证书上传至服务器。
7. 在对应的代理服务器上使用对应证书。

### 免费CA

> 此处使用的是 acme.sh 脚本，当然也可以使用 cartbot 之类的工具

1. 安装脚本

   ```bash
   curl https://get.acme.sh | sh
   ```

2. 首次使用需要注册一个邮箱。

   ```bash
   acme.sh --register-account -m your@example.com
   ```

3. 在服务商处注册 `AccessKey`，我使用的是阿里云。随后给 `acme` 添加参数

   ```bash
   export ALICLOUD_AK="your_access_key_id"
   export ALICLOUD_SK="your_access_key_secret"
   ```

4. 申请证书，泛域名证书需要在服务商处填写 `TXT` 类型的 `_acme-challenge` 子域名作为验证。

   ```bash
   # 此处申请的是泛域名证书
   acme.sh --issue --dns dns_aliyun -d "*.domain.com"
   # 执行该命令后等待一下会有日志提示
   ```

5. 待 `_acme-challenge` 子域名生效后继续执行脚本

   ```bash
   acme.sh --renew --dns dns_aliyun -d "*.domain.com"
   ```

