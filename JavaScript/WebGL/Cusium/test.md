# 概念

## CesiumWidget

![image-cesium-widget](.\cesium-widget.png)

`Cesium` 中包含一个纯粹的 3D 对象：`cesiumWidget` ，其中包含：

* clock：用于记录时间，描述某一帧的绘制内容。

* container：真实的 DOM 对象，挂载的DOM元素

  ```javascript
  // ID名为 cesiumContainer 的dom元素
  const viewer = new Cesium.CesiumWidget("cesiumContainer")
  ```

* canvas：根据 DOM 构建的 `canvas` 类。

* screenSpaceEventHandler：对绘制内容的各项鼠标交互事件的集合，以控制和操控三维场景。

* scene：整个三维场景的对象

## scene

> 包含所有三维对象。

`Cesium` 中有一些内置的环境图元对象：`globe(地球)`、`skybox(星空、天空盒)`、`sun(太阳)`、`moon(月亮)` 等等。以及两个由用户自行控制存放对象的数组：

`primitives(普通对象)`、`groundPrimitives(贴地对象)`。

* 图元：`Cesium` 用于绘制三位对象的**独立**的结构。
* `Globe`：绘制全球地形，**地形高程信息**和**影像图层**，也就是地球表皮。影像图层可以多个，但地形高程只能有一个。`Globe` 也是一个图元对象

### Primitives

> 图元对象集合（在 Cesium 中所有可见的图像都属于涂元对象）

所有图元对象都由一个 `update` 函数。

包含：

* `Globe`：见上笔记。
* `Model(glTF)`：模型，
* `Primitive`：图元的一种，可以理解为图元对象的基元
  * `Geometry`：
  * `Appearance`：
* `Billborads/Labels/Points`：用户自定义的 图片、文字、点（线和多边形其实也是由多个点绘制而来的）的集合。
* `ViewportQuad`：类似于 `div` 元素，不过是存在于三维空间中

# 使用