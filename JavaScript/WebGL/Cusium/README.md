## 是什么

基于 `JavaScript` 编写的 **三维地理空间平台**。

支持 2D、2.5D、3D 的地图形式展示。

可自行绘制图形、高亮区域等。且支持绝大多数浏览器和移动端

## 下载和使用

### 地址

源码地址：`https://www.cesium.com/` ——> `Platform` ——> `Download`

​	或者 `npm install cesium`

教程地址：`http://cesium.xin/`、`https://cesium.com/learn/cesiumjs-learn/`。

### 使用

> 需要注册账号以获取 cesium 的 token

**获取方法**：点击 `sign up` 注册账号并登录，点击导航栏中的 `Access Tokens` 进行创建。

* 在项目中使用只需要打包好的 `cesium` 源码即可。

  源码位置：`Build/Cesium/Cesium.js`.

  样式文件：`Build/Cesium/Widgets/widgets.css`.

* 查看项目文档目录只需将下载好的源码中的 `index.html` 打开即可查看

  `cesium` 只能在 `Node` 项目中运行（ `vueCli` 本质也是一个）

#### 获取权限

## 关键对象

### Viewer

展示地图的视窗以及基础控件，也可以配置一些基础的场景选项。

```javascript
const viewer = new Cesium.Viewer("Container", {
    animation: false,
    timeline: false
})
```



### Scene

所有3D对象的容器，可对**整个场景进行修改**（光源、图层样式、地理数据），也可以**在地图上绘制图形**。更重要的是控制这整个场景的交互（鼠标事件、相机事件）

### Entity

由 `Primitive` 封装而来，但 `Entity` 不属于 `Scene` ，`Scene` 相较而言封装程度高，使开发者专注于数据的呈现而不必关心底层可视化机制，可构建时间动态可视化的结构

在使用中主要用于加载实体模型、几何图形，对其进行样式设置、动效修改等。



### DataSourceCollection

加载矢量数据的主要方式之一，支持加载矢量数据集和外部文件的调用。

主要有三种调用方法：`CzmlDataSource`、`kmlDataSource`、`GeoJsonDataSource`

## 坐标系

### WGS84弧度坐标系

> 对应 `Cartographic` 对象

#### 角度与弧度的换算

弧度 = Π / 180 * 经纬度角度

经纬度角度 = 180 / Π * 弧度

### 笛卡尔空间直角坐标系

> 对应 `Cartesian3` 对象

↑ ：Z轴 ；→ ：Y轴 ；↓ ：X轴

```javascript
new Cesium.Cartesian3(x,y,z)
```

### 平面坐标系

> 对应 `Cartesian2` 对象

→ ：X轴；↓：Y轴

```javascript
new Cesium.Cartesian2(x,y)
```

### 4D 笛卡尔坐标系

> 对应 `Cartesian4` 对象

## 相机系统

> 鼠标与视域的交互（放大缩小拉伸等）

**更多 API 请阅读：**`https://cesium.com/learn/cesiumjs/ref-doc/Camera.html?classFilter=camer`。

```javascript
/**
* 相机系统 
*/
// setView : 该效果是直接切换，没有动画效果
viewer.camera.setView({
    // 位置
    destination: Cesium.Cartesian3.fromDegrees(0, 0, 1000),
    // 视口观看方向
    orientation: {
        heading: Cesium.Math.toRadians(0),
        pitch: Cesium.Math..toRadians(-90),
        roll: 0
    }
});
// flyTo : 拥有动画，也能设置动画时间
viewer.camera.flyTo({
    destination: Cesium.Cartesian3.fromDegrees(0, 0, 1000),
    orientation: {
        heading: Cesium.Math.toRadians(0),
        pitch: Cesium.Math..toRadians(-90),
        roll: 0
    },
    duration:5 // 动画持续时间
})
// lookAt : 锁定某个区域，可旋转放大和缩小
viewer.camera.lookAt(
    // 第一个参数：只需要经纬度不需要高度
    Cesium.Cartesian3.fromDegrees(0, 0),
    // 第二个参数：视口方向和固定的高度
    new Cesium.HeadingPitchRange(
    	Cesium.Math.toRadians(50),
    	Cesium.Math.toRadians(-90),
        2500
    )
)
// viewBoundingSphere : 通常用于对某些建筑进行漫游观察时使用（动态观测）

```

## 地图操作

### 建筑体的添加和使用
