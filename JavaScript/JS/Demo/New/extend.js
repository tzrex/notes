function Persion(name, age, welcome) {
  this.name = name;
  this.age = age;
  this.welcome = welcome
}
Persion.prototype.setWelcome = function(text) {
  this.welcome = text
}
Persion.prototype.say = function() {
  console.log(this.name + " to say ", this.welcome)
}

// es6之前
function Chinese(city, ...args) {
  Persion.call(this,...args)
  this.city = city
}
Chinese.prototype = new Persion()

const rex = new Chinese("成都", "Rex", 24, "nihao")

rex.say("hello")
console.log(rex.city)

// es6之后
class English extends Persion {
  constructor(city, ...args) {
    super(...args)
    this.city = city
  }
}

const Flover = new English("弗罗里达", "Flover", 25, "hello")

Flover.say()
console.log(Flover.city)
