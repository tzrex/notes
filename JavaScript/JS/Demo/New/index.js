/**
 * new 执行的五部曲
 * 1. 创建一个新对象
 * 2. 将对象与构造函数的原型进行绑定
 * 3. 绑定作用域指向
 * 4. 初始化新对象的属性
 * 5. 返回对象的实例。（如果是基本类型则返回基本类型对应的对象）
 */
function newFunc(func, ...args) {
  const obj = Object.create(func.protptype)
  const res = func.apply(obj, args)
  if (typeof res === 'object' && res != null) {
    return res
  }
  return obj
}
