/**
 * 数组的操作
 */
const arr = [1, 2, 3, 4]

// 增加
arr.push("5", 9) // 末尾添加
arr.unshift(0, 5, 6, 7) // 开头添加

console.log("array.add ==>", arr)

// 修改
arr[8] = 8

console.log("array.edit ==>", arr)

// 删除
arr.shift() // 删除首项
arr.pop() // 删除末尾

console.log("array.del ==>", arr)

// 查看。直接打印数组既可看到所有元素
const oneIndex = arr.indexOf(2) // 从头开始查找元素位置，不存在返回 -1
const twoIndex = arr.lastIndexOf(7) // 从末尾开始查找元素位置，不存在返回 -1
const isHas = arr.includes(3) // 检查是否拥有该元素，返回布尔值
const arrFind = arr.find((item)=> item == 7) // 返回匹配的第一个元素，不匹配则返回 undefind
const arrFilter = arr.filter((item) => item > 7) // 查询符合条件的所有元素

console.log("array.finds ==>", oneIndex, twoIndex, isHas, arrFind, arrFilter)
console.log("array.all ==>", arr)

// 其中有个 splice 方法可以说是涵盖增删改的全部操作
arr.splice(1, 2) // 从下标1开始删除两个
arr.splice(1, 0, ...[10, 11, 12]) // 从下标1开始添加三个元素
arr.splice(1, 1, ...[13, 14, 15]) // 从下标1开始修改一个并添加两个元素

console.log("array.splice ==>", arr)

// 有些是创建了新的数组
const newArr1 = arr.concat(["h", "e"], ["l", "l", "o"]) // 合并多个数组
const newArr2 = arr.map((item) => {
  if (item === 1) return 10
  if (item === 11) return 100
  return item
}) // 遍历重组

console.log("newArr ==>", arr, newArr1, newArr2)
