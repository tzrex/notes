const express = require('express')

const app = express()

app.use((req, res, next) => {
  res.header('Access-Control-Allow-Origin', '*'); // 允许所有来源
  res.header('Access-Control-Allow-Methods', 'GET, POST, PUT, DELETE'); // 允许的HTTP方法
  res.header('Access-Control-Allow-Headers', 'Content-Type, Authorization'); // 允许的请求头
  next();
})

app.get('/', (req, res) => {
  const testObj = {
    name: "Rex",
    age: 24,
    sex: 1
  }
  res.send(testObj);
});

app.get('/get', (req, res) => {
  res.send('this is get api');
});

app.get("/post")

const port = 3000
app.listen(port, () => {
  console.log(`service run localhost:${port}`)
})
