/**
 * 节流函数
 * 将连续的操作按照一定间隔时间内执行一次的规律进行约束，
 * 避免高频的执行导致资源的过度消耗。
 * @param {*} func 
 * @param {*} delay 
 * @returns 
 */
function throttle(func, delay) {
  let bool = false
  return function() {
    if (!bool) {
      bool = true
      func.call(this, arguments)
      setTimeout(() => { bool = false }, delay)
    }
  }
}

function Test() {
  for (var i = 0; i < 10; i++) {
    throttle(()=> { console.log("index==>", i) }, 1000)()
  }
}

Test()