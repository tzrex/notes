/**
 * 防抖函数
 * 将多次重复的操作在停止一定时间后执行一次，
 * 避免无效的执行发生。
 * @param {*} func 
 * @param {*} delay 
 * @returns 
 */
function debounce(func, delay) {
  let timer
  return function() {
    clearTimeout(timer)
    setTimeout(() => {
      func.call(this, arguments)
    }, delay)
  }
}
