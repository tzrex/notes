/**
 * 闭包的基本表现
 * 函数嵌套函数，且内部函数被外部函数返回并保存
 */
function closeFunc() {
  let numb = 0
  return ()=> {
    console.log("inside.function", numb)
    numb++
  }
}

// 此时闭包就产生了
const closeMoudleOne = closeFunc()

closeMoudleOne()
closeMoudleOne()
