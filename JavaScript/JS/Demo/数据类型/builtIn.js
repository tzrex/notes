/**
 * 内置对象。
 */

// parseInt
const int1 = parseInt("120", 2)
const int2 = parseInt("120", 3)
const int3 = parseInt("120", 4)
const int4 = parseInt("120", 5)
const int5 = parseInt("120", 16)
console.log('int==>', int1, int2, int3, int4, int5)

// parseFloat
const float1 = parseFloat("123.0100")
const float2 = parseFloat("00123.0")
console.log('float==>', float1, float2)

// eval 方法：会将参数转化为js代码进行执行
eval(`console.log("hello")`)

// 各类错误
const err = Error("global.error")
const synatxErr = SyntaxError("syntax.error")
const typeErr = TypeError("type.error")
const rangeErr = RangeError("range.error")
const URLErr = URIError("url.error")
console.log("errs==>", err, synatxErr, typeErr, rangeErr, URLErr)

// 数学对象
const pi = Math.PI
const rand = Math.random()
const round = Math.round(pi)
console.log("mathClass", pi, rand, round)

// 日期对象
const time = Date()
const timeClass = new Date()
const year = timeClass.getFullYear()
const month = timeClass.getMonth()
const day = timeClass.getDay()
console.log("time==>", time, timeClass, year, month, day)

/**
 * 各类数据类型
 */
// 数组
const arr = Array(5)
arr[1] = 12
arr[3] = "Rex"
arr.push(1)
console.log("arr==>", arr, arr[0], arr.length)

// 对象。作为方法时等价于直接赋值 {} ，作为常量时是一个方法集合
const obj = Object()
obj.name = "custom"
const assign = Object.assign(obj, {age:24})
const copyAssign = Object.assign({}, {name:"Flover"})
console.log("obj==>", obj, assign, copyAssign)

// 字符串。作为方法使用时等价于直接使用单双引号，字符串能直接调用原型链上的方法
const str = String("hello")
const strLen = str.length
const strHasTo = str.includes("ll", 2)
console.log("str==>", str, strLen, strHasTo)

// 正则表达式。
const reg = RegExp(/^[a-zA-Z1-9]$/)
const regTestStr = "abcdefg123@qq.com"
const regType = typeof reg
const regIsPass = reg.test(regTestStr)
console.log("regExp==>", reg, regType, regIsPass)

// 布尔值
const boolTrue = Boolean(true)
const boolFalse = Boolean(false)
console.log("bool==>", boolFalse, boolTrue)

// 数字。和上述字符串情况类似
const num = Number(123.456)
const numFixed = num.toFixed(1)
console.log("numb==>", num, numFixed, typeof numFixed)

// json 对象。JSON 对象和字符串之间进行转换
const jsonStr = `{"name":"Flex", "age":24}`
const jsonParse = JSON.parse(jsonStr)
const jsonParseType = typeof jsonParse
const jsonStringify = JSON.stringify(jsonParse)
const jsonStringifyType = typeof jsonStringify
console.log("json==>", jsonParse, jsonParseType, jsonStringify, jsonStringifyType)