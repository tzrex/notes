/**
 * 基础类型。此类型属于值传递
 * Undefined：未定义
 * Null：空
 * Boolean：布尔值，只有true和false两个值
 * Number：数字
 * String：字符串
 */
var und = undefined
var nil = null
var bool = true
var numb = 10
var str = "hello"

console.log("基础类型==--==")
console.log("undefined", und, typeof und)
console.log("null", nil, typeof nil)
console.log("boolean", bool, typeof bool)
console.log("number", numb, typeof numb)
console.log("string", str, typeof str)

/**
 * 引用数据类型。此类型都有对应的存储地址，在进行传递时实际上操作的是同一个内存地址上的值
 * Object：对象。万物皆对象，这是从java中而来的设计思想。
 * Array：对象的一种，是一种特殊的对象
 * Function：函数，是 es 语法中的一等公民
 * Date：时间，用于处理日期和时间的对象
 */
var obj = {}
var arr = []
var func1 = function() {
  console.log("this is function one")
}
var func2 = () => {}
var date = new Date()

function change(fun) {
  // 修改 fun 并不会导致 func1 的修改
  fun = () => {
    console.log("change function")
  }
}
change(func1)
func1()

// 给对象添加自定义方法
Date.prototype.format = function(format) {
  const date = this

  const map = {
    'M': date.getMonth() + 1,
    'd': date.getDate(),
    'h': date.getHours(),
    'm': date.getMinutes(),
    's': date.getSeconds(),
    'y': date.getFullYear(),
  }

  return format.replace(/([yMdhms])+/g, function (match, key) {
    return map[key].toString().padStart(match.length, '0');
});
}

console.log("引用类型==--==")
console.log("object", obj, typeof obj)
console.log("array", arr, typeof arr, "isArray==>", Array.isArray(arr), arr instanceof Array)
console.log("function", func1, func2)
console.log("date", date, typeof date, date.format('yyyy-MM-dd hh:mm:ss'))


/**
 * 后续新增类型
 * Symbol：唯一值。
 * BigInt：任意精度的整数
 */
var symbol = Symbol()
var int = 2024n

console.log("新增类型==--==")
console.log("symbol", symbol, typeof symbol)
console.log("bigint", int, typeof int)
