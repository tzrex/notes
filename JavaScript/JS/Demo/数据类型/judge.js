/**
 * typeof 关键字
 * 对基础类型有效，无法区分
 */
const varTypeofOne = typeof "1234"
const varTypeofTwo = typeof []
const varTypeofThree = typeof {}
console.log("typeof==>", varTypeofOne, varTypeofTwo, varTypeofThree)

/**
 * instanceof 关键字
 * 可对引用类型进行区分，对基础类型无效
 */
const varInstanceofOne = [] instanceof Array
const varInstanceofTwo = {} instanceof Object
const varInstanceofThree = "abc" instanceof String
console.log("instanceof==>", varInstanceofOne, varInstanceofTwo, varInstanceofThree)

/**
 * constructor 属性
 * 可判断元素的原类型。
 */
const varConstructorOne = "abc".constructor === String
const varConstructorTwo = [].constructor === Array
const varConstructorThree = {}.constructor === Object
console.log("constructor==>", varConstructorOne, varConstructorTwo, varConstructorThree)

/**
 * Object.prototype.toString.call() 方法
 * 该元对象的方法能查看各对象的类型属性。
 * 返回的是一个 [object type] 的字符串
 */
const opt = Object.prototype.toString
const varToStringOne = opt.call(2)
const varToStringTwo = opt.call("a12")
const varToStringThree = opt.call([2])
const varToStringFour = opt.call({})
console.log("toStringCall==>", varToStringOne, varToStringTwo, varToStringThree, varToStringFour)
console.log(
  "toStringCallType==>",
  typeof varToStringOne,
  typeof varToStringTwo,
  typeof varToStringThree,
  typeof varToStringFour
)
