// "use strict"

const obj = {
  value: 1,
  valueOf: function() {
    return this.value ++
  }
}

if (obj == 1 && obj == 2) {
  console.log("true")
}

function Test(arr) {
  if (arr instanceof Array) {
    console.log("args==>", ...arr)
  } else {
    console.log("args.is.not.array")
  }
}

Test([1, 2, 3, 4])

Test.call(this, [1, 2, 3, 4])
Test.apply(this, [1, 2, 3, 4])
