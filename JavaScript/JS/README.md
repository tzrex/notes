# HTML

文档地址：https://developer.mozilla.org/zh-CN/docs/Web/HTML

## 渲染页面

* 浏览器获取HTML和CSS资源。
* 将HTML解析成DOM树，将CSS解析成CSSOM。

* 将DOM和CSSOM合并为渲染树。
* 计算渲染树的各节点（页面样式、布局位置等）。
* 将节点渲染到屏幕上。

## 第五版

> 也叫HTML5

# CSS

文档地址：https://developer.mozilla.org/zh-CN/docs/Web/CSS

## 第三版

> 也叫css3

## 预编译

# JavaScript

文档地址：https://developer.mozilla.org/zh-CN/docs/Web/JavaScript

## 设计理念

> 有时也叫 “设计原理”（不懂为什么要叫这个抽象的名字）

<strong>`事件循环` 和 `调用栈` 共同协作，使 js 能够同时处理同步和异步任务，从而提高执行效率</strong>。

### 开始

最初由网景公司于1995年设计，借鉴了 `c` 和 `java` 中的一些特性。旨在建立一种简单灵活的脚本语言，<strong>为网页开发提供交互性和动态性</strong>。

`javaScript` 是一门<strong>单线程、弱类型</strong> 的 <strong>动态</strong> 编程语言。

### JS引擎

> js能顺利运行的关键。了解即可

<strong>基本功能都是解析、编译和执行JavaScript代码。这是不可或缺的步骤</strong>。

主要是一下步骤：

1. 解析和编译：引擎会首先对代码进行语法分析，之后将代码转换为抽象语法树（AST），之后转为字节码或机器码。
2. 执行：执行时引擎会追踪变量、执行函数调用、处理异常等操作。
3. 优化：现代的js引擎通常包含即使编译器。它会监视代码的执行情况并进行优化，以提高代码的执行效率。优化通常包括：内敛缓存、函数内敛、逃逸分析等。
4. 内存管理：负责管理内存的分配和释放。它会跟踪不再使用的对象，并在适当的时候进行垃圾回收，以释放内存资源。

#### 一些引擎

* V8引擎：由 `Google` 开发，用于 `Chrome` 浏览器<strong>和 `NodeJs` 环境</strong>。
* SpiderMonkey：火狐浏览器使用的js引擎。
* JavaScriptCore：`Safari` 浏览器使用的js引擎。
* Chakra：Edge浏览器使用的js引擎。

### 调用栈

* 当函数被调用时，会创建一个与函数相关的 `栈帧` 并推入栈结构内，`栈帧` 中<strong>包括函数的参数、局部变量、返回地址等</strong>信息。
* 若是递归函数则会创建多个相同函数的 `栈帧`。如果递归层级过深可能会导致栈溢出（Stack Overflow）
* 调用栈时同步执行的主要机制。它会依次执行调用栈中的函数，直到栈为空。

### 异步操作

#### 事件循环

事件循环是 js引擎用于管理异步操作的机制。它<strong>负责监听调用栈和事件队列</strong>。

确保了 `js引擎` 能够实现<strong>非阻塞异步操作的关键</strong>。保持应用程序的响应性。

* 事件队列是在编译时，js引擎会将异步操作的回调函数放到其中进行等待，在合适的时机进行取出。

* 当调用栈为空时，它会检查事件队列中是否有待处理任务。如果有则将任务推入调用栈中。
* 异步事件分为<strong>宏任务和微任务</strong>。当第一个宏任务执行完之后会检测是否有微任务，如果有则执行微任务再执行宏任务。<strong>宏任务执行完再检测微任务队列，以此重复</strong>。

#### 回调函数

它是异步操作中的一个中组成部分。使得js能更加动态的处理各种任务。

实际的开发中回调函数嵌套回调函数虽然不会对性能造成什么影响。但会严重影响开发人员的维护， `Promise` 、`async、await` 就是为解决回调地狱而诞生的。

## 组成

### ECMAScript

可简单理解为编写前端时使用到的脚本语法。

语法每年都有新增，根据时间的不同有不同版本。其中最突出的为 `es6` ，甚至可以说 `es6` 之前和之后完全时不同的代码。

### DOM

文档对象模型

将整个 `HTML` 部分抽象成的接口。能通过提供的API对 `HTML` 进行操作。使用对象 `document`。

#### 事件委托

> 又叫事件代理。

利用事件冒泡的机制进行实现，将子元素的事件绑定到父元素上。

事件冒泡默认未允许，可通过一些方法进行阻止：

* `stopPropagation()`：调用元素原型链上的 `stopPropagation` 方法进行阻止。
* `addEventListener()`：可通过设置第三个参数设置是 `捕获事件` 还是 `冒泡事件`，默认为 `冒泡事件`。

### BOM

浏览器对象模型

对一系列在浏览器环境中使用对象的统称。也就是浏览器环境下的 `window` 对象。

`window` 可以理解为全局对象。

它包含在浏览器中使用很多对象：

* `document`：文档对象，DOM 层的实现。
* `frames`：框架，参考 `HTML` 中的 `frame` 标签。
* `navigator`：浏览器对象
* `history`：历史对象
* `location`：路由对象
* `screen`：显示器对象

## 数据类型

如果按照语法中的严格划分，那么 `ECMAScript` 的数据类型有：

* `Undefined`：未定义，所有值在赋值之前的基本类型。六大假值之一
* `Boolean`：真或假，可以理解为一种状态的两极。其中 `false` 属于六大假值之一
* `Number`：数字类型。包含整数和浮点数。
* `String`：字符串类型。
* `Object`：对象。`Array` 和 `null` 也是一个特殊对象。
* `Function`：js 中的一等公民，可以理解为一段代码的集合体。也可作为类型被返回。

以及后续新增的两种：

* `Symbol`：唯一值。不常用
* `Bigint`：用于表示任意精度的整数，可以理解为是 `Number` 的扩展。在 整数 后面添加 `n` 即变为 `Bigint` 类型。也不常用

### 基础类型

> 传递时是值传递。

`String`、`Number`、`Boolean`、`Undefind`、`null` 都是值传递，不会影响原变量的值

**基础类型是存储在栈内存当中**。

### 引用类型

> 传递时是指针传递。

`Object`、`Array`、`Function` 等都是引用类型。而 `Function` 因为其特殊性暂不做考虑。

而 `Object` 和 `Array`（`{}` 和 `[]`）常常作为存储值的容器，当传递此类变量时，修改参数会导致原变量受影响。

**基础类型是存储在堆内存当中**。

### 内置对象

> 即 `Object` 对象下的举例。typeof 之后 打印出 `object` 的

* 全局对象：`eval()`、`parseFloat`、`parseInt` 等挂载到window上的方法。
* 错误对象：各种错误。`Error`、`SyntaxError`、`typeError`、`RangeError`、`URLError` 等错误对象。
* 数学对象：`Math`。提供数学相关的函数和常量
* 日期对象：`Date`，用于提供时间相关的一些API
* 函数对象：`String`、`Number`、`Boolean`、`Object`、`Array` 这些都作为函数挂载到了 `window` 上。以及关键字 `Function`.
* 正则对象：`RegExp`，正则表达式，常常与字符串一起使用。
* json对象：`JSON`，用于解析和序列化 JSON 格式的数据。

### 数组的操作

> 此处只展示常用的，毕竟Array原型提供的API有点多

详细文档请前往：https://developer.mozilla.org/zh-CN/docs/Web/JavaScript/Reference/Global_Objects/Array

`splice`：此方法包含了增删改的所有实现。可通过参数达到不同的效果，就不单独写到下列分类中

#### 增加

* `push`：往数组的末尾添加元素，可添加多个
* `unshift`：往数组的开头添加元素，可添加多个

#### 修改

* 直接通过下标修改：`youtArr[Index] = newEle`
* `reverse`：翻转数组。
* `sort`：对数组内元素进行排序

#### 删除

* `pop`：删除末尾的一个元素。
* `shift`：删除开头的一个元素。

#### 查看

* 直接通过下标获取对应下标的元素；或者不带下标直接查看整个数组。
* `includes`：是否包含某个元素，返回布尔值。
* `indexOf / lastIndexOf`：从（开头 / 末尾）开始查找元素位置，没找到则返回 -1
* `find`：查找符合条件的首个元素，参数为 布尔函数
* `filter`：查找符合条件的所有元素，参数为 布尔函数

#### 其他操作

* `isArray`：判断某变量是否是数组。参数为需要判断的变量，返回值为布尔值。
* `concat`：合并多个数组并返回新数组。
* `toReversed`：翻转数组，但不改变原数组返回新数组。
* `toSorted`：对数组元素排序，但不改变原数组返回新数组。
* `join`：将数组转为字符串，参数为间隔字符的字符串。
* `map`：遍历数组。可按需修改符合条件的元素，返回新数组。
* `some`：测试数组元素是否有符合条件的，参数为自定义条件的函数。
* `every`：测试数组元素是否全部符合条件。
* `reduce`：数组元素的累加，每此遍历执行时的参数都是上一次计算的结果。
* 元素个数：`.length` 所有数组内置的一个属性，用于标记数组内元素个数。

常见面试题：如何给数组去重。

```javascript
const arr = [1, 2, 2, 3, 4, 5, 5]

// 第一种：使用 es6 提供的 Set
const newArr1 = [...new Set(arr)];

// 第二种：使用 filter 配合 indexOf 使用
const newArr2 = arr.filter((el, index, self) => {
    return self.indexOf(value) === index
})

// 第三种：使用数组自带的 reduce 方法
const newArr3 = arr.reduce((acc, current) =>{
    // 若是已有对应值则会跳过
    if (!acc.includes(current)) {
        acc.push(current)
    }
}, [])
```

### 类型判断

* `typeof`：关键字。能对基础类型进行查看。
* `instanceof`：关键字。能对引用类型进行判断。
* `constrantor`：属性。在数据类型后面添加该属性能查看元类型。
* `Object.prototype.toString.call()`：元对象的方法，因为所有对象的根对象都是 `Object`.

## 六大假值

两个数字，一个字符串，一个对象，一个未定义，一个布尔值

* `Undefind`：此类型先天就是。
* `null`：空对象。
* `false`：布尔值中的 假 也是假值之一。
* `""`：空字符串（`""` 和 `''`）
* `0`：数字类型的零也是假值之一。
* `NaN`：不正确的数字，也是假值之一。

## 函数的妙用

### 闭包

表现形式：函数嵌套函数，内部函数被外部函数返回并保存，且内部函数调用外部变量。

```javascript
function closeFunc() {
    const numb = 0
    return () => {
        console.log("hello", numb)
    }
}
// 此时就产生了闭包
const closeOne = closeFunc()
```

#### 特点

* 可以重复利用变量，并且变量不会污染全局。
* 变量始终保存在内存中，不会被垃圾回收机制回收。但这会有导致内存泄漏的风险；当闭包较多时，会过度消耗内存导致页面性能下降。

#### 使用场景

##### 防抖

将多次重复操作合并为一次。

```js
// 一个简单的实现
function debounce(func, delay) {
    let timer = null
    return function() {
        if (!timer) {
            clearTimeout(timer)
            timer = setTimeout(() => {
            	func.call(this, arguments)
            }, delay)
        }
    }
}
```

##### 节流

当调用动作连续发生时，保证在规定时间内只执行一次动作。

```javascript
function throttle(func, delay) {
    let tag = false
    return function () {
        if (!tag) {
            func.call(this, arguments)
            tag = true
            setTimeout(()=>{ tag = false }, delay)
        }
    }
}
```

#### 内存泄漏

当分配的内存地址长时间未释放或消除时，造成的长期占用资源的情况。会使得内存资源浪费，导致页面卡顿、运行慢等。

### 实例化

> 也就是 `new` 关键字

#### 构造函数

与普通函数不同，普通函数通常是作为相同操作的集合，其目的是执行一些特定的任务。

```javascript
function run(numb) {
    for (let i=0; i<numb; i++) {
        console.log("index==>", i)
    }
    for (let i=1; i<numb/2; i++) {
        console.log("index==>", i)
    }
}

run(20)
```

而构造函数通常是构建一个新的对象，和 `new` 关键字搭配使用。通常用于创建具有相似属性和方法的多个对象。

```javascript
function Persion(value) {
    this.value = value
    this.GetValue = () => {
        return this.value
    }
}

const p1 = new Persion("Rex")
```

#### 原型链

当创建的对象中都具有相同的属性或方法时，在实例化后会创建相同的版本。这会导致过多的占用内存资源。

为避免这种浪费，于是将这些具有相同属性或方法的字段用 `__proto__` 私有属性进行了封装，该私有类型会继承父级的`prototype` 属性。

`prototype` 对象会创建一个共同的堆，子级会通过 `__proto__` 属性调用父级的 `prototype` 从而减少浪费。

```javascript
function Persion(value) {
    this.value = value
}
Persion.prototype.GetValue = () => {
    return this.value
}

const p1 = new Persion("Rex")
const p2 = new Persion("Flover")
```

#### `New` 的过程

1. 创建空对象。
2. 将新对象的原型指向构造函数的原型对象。
3. 绑定 `this` 指向。
4. 初始化新对象的属性。
5. 返回新对象：如果没有显式返回一个对象，则自动创建一个新的对象实例；如果有显示返回则返回该对象

```javascript
// 一个简单的模拟过程
function newFunc(classFunc, ...args) {
    // 创建新对象并将原型指向构造函数的原型
    let newObj = Object.create(classFunc.prototype)
    // 绑定作用域（修改this指向）并传入参数
    let res = classFunc.apply(newObj, args)
    // 判断返回值
    const tag = typeof res === 'object'&& res !== null
    // 根据返回值返回对应的对象
    if (tag) return res
    return newObj
}

// 测试
function Persion(name, age) {
    this.name = name
    this.age = age
}
Persion.prototype.say = function () {
    console.log(this.name +" is say")
}

// 和 new 是一样的效果
const p1 = newFunc(Persion("Rex", 24))
```

### 继承

> 继承原本是面向对象预言的特性。在js中主要是使用原型链进行的实现

声明一个示例原型

```javascript
function Persion(name, age) {
    this.name = name;
    this.age = age;
}
Persion.prototype.getName = function() {
    return this.name
}
Persion.prototype.getAge = function() {
    return this.age
}
Persion.prototype.say = function() {
    console.log(this.name + " say hello")
}
```

* 使用原型链和构造函数实现继承。

  ```javascript
  function Chinese(...args) {
      Persion.call(this, ...args)
  }
  Chinese.prototype = new Chinese()
  ```

* 使用 `es6` 提供的关键字 `extends`

  ```javascript
  class English extends Persion {
      constructor(...args) {
      	super(args)
      }
      this.sex = "1"
  }
  ```

### this 指向

* 在全局对象或全局作用域时指向 `window` 对象（严格模式为 `undefind`）
* 普通函数中的this指向 `window`（严格模式为 `undefind` ）。
* 在非箭头函数中，`this` 指向最后调用它的对象。
* `new` 会将 `this` 变为指向当前对象。
* `apply`、`call`、`bind` 三个方法均会修改 `this` 指向。
* 匿名函数中的指向具有全局性，等同于全局对象的 `this` 指向。

## 严格模式

> 严格模式是 `ES5` 引入的一种 JS 执行模式，提供了一种更加严格的解析和执行环境。

### 启用

像启用十分简单。只需要在需要执行严格模式的代码前添加 `"use strict"` 既可开启。

* 全局启用：在js文件的开头添加上述字样既可。
* 函数内启用：在需要执行的代码顶上添加上述字样。

### 优点

* 有助于发现代码中潜在的错误，提高代码的健壮性和可维护性。
* 禁止了一些不规范的语法。例如：禁止使用八进制字面量、禁止 `with` 语句等。

### 注意事项

`严格模式`（下文用 `严格` 表示） 和 `非严格模式` （下文用 `非严格` 表示）下代码的一些特性会有所区别：

* this指向：非严格下全局作用域指向 `window` ，函数内指向函数的调用方；而严格下则是 `undefined` 。
* 变量声明：非严格下可以隐式创建全局变量（未使用var、let、const 关键字）；而严格则会抛出 `ReferenceError` 错误，并且禁止了 `eval()` 创建变量。
* 属性赋值：非严格下给一个不可写属性或不可配置属性赋值只会是无效操作，并不会报错；而严格则会抛出`TypeError ` 错误。
* 函数参数重复：非严格下参数变量名可以重复，后面的覆盖前面的；但严格会抛出 `SyntaxError` 错误。

## Ajax

> 浏览器的网络请求技术。

作用：能在不刷新整个网页的前提下与服务器交换数据并更新部分内容。

它是一种<strong>技术的统称</strong>，而不是某个库或API。

主要原理：

1. 通过发起 `http` 请求向服务发起异步请求。
2. 从服务器拿到数据之后通过 `js` 操作 `DOM` 更新页面

### XHR

> XmlHttpRequest。是 js 原生自带的一个对象。

1. 通过 `open` 方法发起一个请求。
2. 通过 `send` 方法向地址发送请求。
3. 通过 `onreadystatechange` 事件监听通信状态。通常当 `xhr.readyState === 4 && xhr.status === 200` 时表示请求发送成功

```html
<!DOCTYPE html>
<html lang="en">

<head>
  <meta charset="UTF-8">
  <meta name="viewport" content="width=device-width, initial-scale=1.0">
  <title>Ajax是什么</title>
</head>

<body>
  <script>
    const getXhr = new XMLHttpRequest();
    // 请求类型    请求地址   是否异步
    getXhr.open("GET", "http://localhost:3000?name=Rex&age=24", true)
    getXhr.send() // 发送请求
    // 监听请求的状态变化
    getXhr.onreadystatechange = () => {
      if (xhr.readyState === 4 && xhr.status === 200) {
        // 处理响应数据
        console.log(xhr.responseText)
      }
    }
    
    // post 请求
    const postXhr = new XMLHttpRequest();
    const params = {name:"Rex", age:24}
    postXhr.open("POST", "http://localhost:3000", true)
    postXhr.send(params)
    postXhr.onreadystatechange = () => {
      if (xhr.readyState === 4 && xhr.status === 200) {
        // 处理响应数据
        console.log(xhr.responseText)
      }
    }
  </script>
</body>

</html>
```

### fetch

> 现代浏览器提供的方法。很久远的浏览器没有该方法。

基于 `Promise` 的一个网络请求工具。通过回调的方式处理请求来的数据，也是现在流行的方式。

```html
<!DOCTYPE html>
<html lang="en">

<head>
  <meta charset="UTF-8">
  <meta name="viewport" content="width=device-width, initial-scale=1.0">
  <title>Ajax是什么</title>
</head>

<body>
  <script>
    // 默认为 get 请求
    fetch("http://localhost:3000").then((res) => res.json()).then((data) => {
      console.log("data", data)
    })
    // 发起 post 请求，给服务器传递的参数存放在 body 属性上
    fetch("http://localhost:3000", {
      method: "POST",
      body: JSON.stringify({
        name: "Rex",
        age: 24
      })
    }).then((res) => res.json()).then((data) => {
      console.log("data", data)
    })
  </script>
</body>

</html>
```

### axios

> js生态中常用的发起请求的库。需要下载或引入才能正常使用。

基于 `promise` 封装的网络请求库。在前端和 `NodeJs` 环境中均可使用。

```javascript
import axios from "axios"
const axios = axios.create()

axios.get()
axios.post()
```

## ES6新特性

### 块级作用域

使用关键字 `let` 、`const`。这俩关键字不存在声明的变量不存在变量提升的问题。

* `let`：用于声明变量，同一作用域下不可重复声明。
* `const`：用于声明常量，且声明时必须赋值。同一作用域下不可重复声明。

#### 暂时性死区

这是为了解决 `var` 关键字声明变量时导致的变量提升的问题。

```javascript
"use strict"
console.log("x", x) // 此时会报错，因为严格模式不允许隐式声明变量
console.log("y", y) // 打印 undefined。因为发生了变量提升，变量在下一行显式的声明了。
var y = 10
```

暂时性死区会导致开发人员的维护变得困难，且可能导致一些意料之外的错误发生。

### 类定义

> 也就是 class 和 extends 关键字

### 参数默认值

允许给参数设置默认值，为传参时使用。不再像之前的一个 `undefined`。

```javascript
function Main(args = {}) {
    console.log("args==>", typeof args) // object
}
```

### 箭头函数

不完整的函数，没有 `this` 指向。

```javascript
const func = () => {}
```

### 解构赋值

也就是三个点。浅拷贝的一种。

```javascript
const obj = {
    name: "Rex",
    age: 24
}
// 将 obj 对象中的全部属性依次创建并赋值
const copyObj = { ...obj }
```

### Promise

* 解决了回调地狱的问题（但没完全解决）

* 将异步操作队列化。通过自身属性规范了异步事件。

  * `all`、`resolve`、`reject`、`race` 方法
  * 原型上有 `then`、`catch`、`finally` 方法。

* `Promise` 将异步事件设置了三个状态：

  `pending`（初始状态）、`fulfilled`（操作成功）、`rejected`（操作失败）

#### async和await

> 这两个关键字是在 `es7` 新增的，用于处理 `Promise` 的 `then` 回调问题。

* 同步代码做异步执行。`async` 关键字表明函数为异步函数，`await` 是组成 `async` 的表达式。

* `await` 的结果取决于它的等待的内容：如果是 `Promise` 则返回对应的结果；如果是普通函数则进行链式调用。
* 如果 `await` 后的异步函数是 `rejected` 状态，则整个 `async` 函数将停止执行。

### 模块化

> import、export

以前一个js文件就是一个模块，没有单独区分的说法。现在可以通过 `export` 关键字导出自己想导出的部分。

```javascript
import mode from "your/module/path/one"
import { one } from "your/module/path/two"

const three = "three"
const two = "two" 
export two // 只导出 two 变量
```

### Generator

它可以在函数执行过程中暂停并保存当前的状态，然后在需要的时候恢复执行。通过在 `function` 关键字后面紧贴着添加 `*` 符号既可以变为 `Generator` 函数。

主要特点：

* 状态的暂停和恢复：可通过 `yield` 关键字将函数进行暂停并保存当前状态。可使用 `next` 方法逐步执行。

```javascript
function* Example() {
    yield 1;
    yield 2;
    yield 3;
}

const gen = Example()

console.log("value==>", gen.next().value) // 1
console.log("value==>", gen.next().value) // 2
```

`Generator` 函数在异步编程、迭代器、状态管理等方面具有重要的应用价值，在实际开发中经常被用于处理复杂的异步流程、生成无限序列等场景。

### 新数据类型

>Symbol 唯一

它会确保创建的值不一致。

```javascript
const varOne = Symbol("one")
const varTwo = Symbol("one")

varOne == varTwo // false
```

### 新数据结构

#### Set

> 可以理解为无重复值的数组

<strong>用来存储一组不重复的值，并且具有高效的查找和删除操作</strong>。

主要特点：

* 唯一性：`Set` 中的元素是唯一的，不存在重复的值。两个对象总是不相等的。
* 无序性：`Set` 中的元素是无序排列的，即插入时不会进行排序。
* 迭代性：可以使用 `for...of` 和 `forEach` 方法进行遍历。

#### Map

> 可以理解为对象的扩展

主要特点：

* 任意类型的键和值：`Map` 的键不同于 `{}`，可以是任意类型的，包括原始值、对象、函数等。
* 顺序性：`Map` 会按照插入的顺序维护键值对。
* 迭代性：可以像 `{}` 一样进行迭代。

```javascript
const obj = new Map();

// 添加键值对
obj.set("name", "Rex")

// 获取键值对
const name = obj.get("name")

// 删除键值对
obj.delete("name")

// 遍历 Map
for(let [k, v] of obj) {
    console.log("key=>", k, "value==>", v)
}

obj.forEach((val, key) => {
    console.log("key=>", key, "value==>", val)
})
```

在许多情况下比传统的对象更适用，尤其是需要存储任意类型的键和值，并且需要维护插入顺序的场景下。
