## 基本类型

### 布尔值

> 与 `JavaScript` 中一样

```typescript
// 只有 true 和 false 两个值
let bool:boolean = false;
let bol:boolean; // 默认为false
```

### 数字

> 在 TS 中，所有的数字都是浮点数（没有定数浮点数之分），都是 number

```typescript
/** TypeScript支持ES6中引入的二进制和八进制字面量。 */

// 十进制：默认就是10进制。
let ten = 16;
// 十六进制：以 0x 开头的会被判定为16进制
let sexteen = 0x15b3;
// 二进制：以 0b 开头的会被判定为2进制
let two = 0b1011;
// 八进制：以 0o 开头的会被判定为8进制
let eight = 0o7641;
```

### 字符串

> 与 `JavaScript` 中一样，可以使用 （ " " ）（ ' ' ）（`）三个符号表示字符串

```typescript
// 声明变量类型
let str:string = "rex";

// 隐式定义类型
let str1 = "hello";
let str2 = 'word';
let str3 = `你好：${str1}`;
```

### 数组

> 有两种声明方式

```typescript
// 在声明的元素类型后加上 "[]" 即可表示为：arr是一个number类型的数组
let arr:number[] = [1, 2, 5];

// 使用数组泛型
let list:Array<number> = [1, 3, 5];

arr[3] = 9 // 允许的，跟JS的数组一样


// 只读数组，没有数组的相关操作。定义了就无法更改。
let a: number[] = [1, 2, 3, 4];
// 设置 ro 变量为只读数组并赋值
let ro: ReadonlyArray<number> = a;
ro[0] = 12; // error!
ro.push(5); // error!
ro.length = 100; // error!
a = ro; // error!
```

### 元组

> 数组存储相同类型，可扩容。而元组则相反

```typescript
// 定义元组，使用 [] 包裹
let tup:[string, number] = ["hello", 15]

// 不允许，编译不会通过
tup = ["hello", 45, true]
```

### Any

> 任意类型，可以像 JS 一样任意改变数据类型

```typescript
let anyData:any = "string"

anyData = 4512 // true
anyData = true // true
anyData = [1,3,5,2] // true
```

### Void & Null & Undefined

> 无值，指向 undefined 和 null

### never

> 表示用不存在的值，通常用于 catch

```typescript
function err(msg:string):never {
    throw new Error(msg)
};

function fail(){
    return err("bad request")
}
```

### Object

> 非初始类型

```typescript
declare function create(obj: object | null):void;

create({ prop:0 }); 	// true
create(null); 			// true
```

### 断言

> 类似于数据类型转换，但不进行编译和执行

```typescript
let val:any = "this is string";

// 尖括号断言：告诉TS这是一个string类型的参数
let length:number = (<string>val).length

// 第二种方式，使用 as 关键字
let length:number = (val as string).length
```

### 枚举

```typescript
enum Color {Red, Blue, Yellow} // 默认值为下表 0 1 2

// 不是标准的json，所以使用 = 赋值
enum style {
    Red={color:red},
	Blue={color:blue},
	Yellow={COLOR:yellow}
};

let css:style = style.Red
```

## 接口

> 可以理解为：在Object中定义各字段的类型和是否必须

**只要传入的对象满足上面提到的必要条件，那么它就是被允许的**。也就是说，字段可多

```typescript
// 使用关键字： interface
interface Label {
    name:string;
    age:number;
    sex?:string
};

function print(val:Label){
    console.log("val value is", val)
};

let obj = {
    name:"Rex",
    age:21,
    abc:"abc"
};

// 接口中未定义的 abc 同样会接收
print(obj); // { name:"Rex", age:21, abc:"abc" }
```

### readobly & const

> 二者应用场景不一样。一个声明常量，一个用于属性类型声明

### 可选属性

> 在 key 后面加上 ?

```typescript
interface Schema {
    width?:number | string;
    height?:number | string
}
```

### 只读属性

> 传入的值不能修改，只能 get 不能 set

```typescript
interface Schema {
    name:string;
    age:number;
    readonly phone:number;
}

let obj = { name:"Rex", age:21, phone:1234567890 }

obj.name = "Flover"		// true

// 只读属性不可修改
obj.phone = 1234567891	// error
```

### 函数属性

> 除开普通类型的外形之外，接口还可以描述函数（返回）类型

```typescript
// 设置函数属性的接口
interface FunSchema {
    // 定义第一个参数为 source 且是字符串类型；第二个参数为 sub 且为字符串类型。
   	// 返回值为 布尔值
    (source:string, sub:string):boolean;
}

// 但定义的函数的参数名不一定要与接口内定义的 key 一样。
// 只需要同位置的参数类型一样和返回值一样。
let myFun:FunSchema = function(src, sub):boolean {
    let result = src.search()
}
```

### 可索引属性

> 简单将就是，通过 [] 拿到的数据的类型

```typescript
interface indexSchema {
    // 定义在 [] 里面的值的属性为 number 类型，且获取到的值类型是 string
    [x: number]:string
}

let myArr:indexSchema = ["one", "two"];
let str = myArr[0];
```

** `TypeScript` 支持两种索引签名：字符串和数字。 可以同时使用两种类型的索引，但是数字索引的返回值必须是字符串索引返回值类型的子类型**。

```typescript
// 定义两个类：一子一父
class Animal {
    name:string;
    age:number
};
class Dog extends Animal {
    breed:string;
};

// error
// number 索引的类型必须是 string类型的子类型。
interface ErrAnimalSchema {
    [x: number]:Animal;
    [y: string]:Dog
};

// 正确的应该是
interface TrueAnimalSchema {
    [x: string]:Animal;
    [y: number]:Dog
}
```

#### 注意

索引型接口使用的一个注意事项

```typescript
// 通常也不会把索引类型和普通类型混用......
interface Schema {
    // 通常要设置只读属性，防止被修改
    readonly [x: string]: number;
    length:number;
    name: string // 这里会报错，下列会标明原因
};

let obj:Schema = { length: 2, name:"Rex" };
obj["length"] = 45	// 报错，因为上面的 [x: string] 设置了只读属性
typeof obj["name"]	// 这里是string类型；与定义的 [x: string] 要求的返回值不一样
```

### 类 类型

> 用于明确强制一个类的某种约定

```typescript
interface ClassSchema {
    // 属性约束
    currentDate:Date;
    // 方法约束
    Timeout(d: Date);
};

// 使用 implements 给类添加约束
// 主要描述的是公有部分的属性，不会约束私有的
class Clock implements ClassSchema {
    constructor(hh:number, mm:number){}
    // 书写的类里面需要有这个属性，且该属性的值类型为 Date
    currentDate: Date;
    Timeout(d:Date){
        return "this time is" + d;
    }
}
```

### 接口继承

#### 继承接口

```typescript
interface Shape {
    color:string;
}

interface Square extends Shape {
    side: number;
};

let square = <Square>{
    color:"#fff",
    side:10
}
```

#### 继承类

## 类（Class）

```typescript
// 父类
class Animal {
    // 类自带的 构造器 函数
    constructor(msg:string){
        this.greet = msg
    };
    greet:string;
    setGreet(){
        return `Hello, ${this.greet}`;
    }
}

// 子类：继承了 Animal 
class Dog extends Animal {
    constructor(){
        // 子类必须调用 super 函数 TS 强制要求执行的一条规则
        super()
    }
}
```

### Public

> 所有声明的属性（方法）默认就是 public 属性的

```typescript
class Animal {
    public name:string;
    age:number;
    constructor(name:string){
        this.name = name;
    }
}

let dog = new Animal("Cat").name // Cat
```

### Private

> 当成员被标记成 `private` 时，它只能在声明它的类访问。

```typescript
class Animal {
    private name:string;
    age:number;
    constructor(name:string){
        this.name = name;
    }
};

class Dog extends Animal {
    constructor(name:string, age:number){
        super(name, age)
    }
    print(){
        return this.name // error
    }
}

// 只能在 Animal 类中使用
let dog = new Animal("Cat").name // error
```

### protected

> 与 `Private` 关键字类似，但可以在起子类中访问

```typescript
class Animal {
    constructor(name:string, age:number){
        this.name = name;
        this.age = age;
    };
    protected name:string;
    age:number;
};
class Dog extends Animal {
    constructor(name:string, age:number){
        super(name, age)
    }
    print(){
        return this.name // true
    }
};

let dogs = new Dog("哈士奇", 9)
dogs.name // error
```

### readonly

> 只读属性必须在声明时或构造函数内初始化

```typescript
class Person {
    constructor(name:string){
        this.name = name;
    };
    readonly type:number = 1;
    readonly name:string;
};

let Chinese = new Person("Rex"); // true
Chinese.name			// 可以读取
Chinese.name = "Flover"	// 但不能修改
```

### 存取器

> 可以理解为 类 中自有的两个属性：`get` 和 `set`。

```typescript
class Employee {
    fullName:string;
};
let emp = new Employee();

// public 属性的可以随意修改
emp.fullName = "Bob";
```

**使用**。

```typescript
// 在 set 中模拟一定的判定条件（实际情况看）
let passCode = "4396";

class Employee {
    constructor(code:string){
        
    }
    private fullName: string;
    // 以 属性名 作为 get 的函数名，函数的返回类型为 string
    get fullName():string {
        return this.fullName;
    };
    
    // 如果不设置 set 只设置 get. 会被默认为 readonly 类型
    set fullName(newName:string){
        if(passCode && passCode === "4396"){
            this.fullName = newName;
        } else {
            return "Error"
        }
    }
}
```

### static

### abstract

## 装饰器

### 前提条件

> 必须在命令行或 `tsconfig.json` 里启用 `experimentalDecorators` 编译器选项

```shell
# 命令行
tsc --target ES5 --experimentalDecorators
```

**tsconfig.json**：

```json
{
    "compilerOptions":{
        "target": "ES5",
        "experimentalDecorators":true
    }
}
```

### 装饰器执行顺序

```typescript
function f(){
    console.log("f():start")
    return function(target){
        console.log("f():callback")
    }
};

function g(){
    console.log("g():start")
    return function(target){
        console.log("g():callback")
    }
};

class c {
    // 等价于： f( g( method(){} ) )
    @f()
    @g()
    method(){}
}
// 执行顺序
/**
	f():start
	g():start
	g():callback
	f():callback
*/
```

#### 不同声明的执行顺序

### 类装饰器

```typescript
// 类装饰器与类紧挨着

@seale
class Greeter{
    constructor(msg:string){}
}
```

## 声明文件

