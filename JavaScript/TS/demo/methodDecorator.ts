/**
 * @title 方法装饰器
 * @description 用来监视，修改或者替换方法定义
 */

// 方法装饰器有三个基本参数
const MethodDec:MethodDecorator = (
  // target:Object,
  // keys:string | Symbol,
  // descript: PropertyDescriptor
  ...arg:any[]
)=>{
  // console.log("target is", target)
  // console.log("keys is", keys)
  // console.log("descript", descript)
  // descript.value()
  console.log('arg', arg)
  arg[0].show()
}

class MethodPerson {
  @MethodDec
  public show(){
    console.log("this is show", this)
  };
  public run(){
    console.log("run function")
  }
}
