/**
 * @title 方法装饰器
 * @description 用来监视，修改或者替换方法定义
 */
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
// 方法装饰器有三个基本参数
var MethodDec = function () {
    // target:Object,
    // keys:string | Symbol,
    // descript: PropertyDescriptor
    var arg = [];
    for (
    // target:Object,
    // keys:string | Symbol,
    // descript: PropertyDescriptor
    var _i = 0; 
    // target:Object,
    // keys:string | Symbol,
    // descript: PropertyDescriptor
    _i < arguments.length; 
    // target:Object,
    // keys:string | Symbol,
    // descript: PropertyDescriptor
    _i++) {
        // target:Object,
        // keys:string | Symbol,
        // descript: PropertyDescriptor
        arg[_i] = arguments[_i];
    }
    // console.log("target is", target)
    // console.log("keys is", keys)
    // console.log("descript", descript)
    // descript.value()
    console.log('arg', arg);
    arg[0].show();
};
var MethodPerson = /** @class */ (function () {
    function MethodPerson() {
    }
    MethodPerson.prototype.show = function () {
        console.log("this is show", this);
    };
    __decorate([
        MethodDec
    ], MethodPerson.prototype, "show");
    return MethodPerson;
}());
