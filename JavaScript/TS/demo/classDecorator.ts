/**
 * @title 类装饰器
 * @description 类装饰器用于监视，修改或替换类定义
 */


function ClassDecFun(target:Function){
  console.log('target to func is', target)
};

// 类型推论
const ClassDecConst:ClassDecorator = function(target:Function){
  console.log('target to const is', target)
};

@ClassDecFun
class TankClass {}

@ClassDecConst
class PeoperClass {}

// 同样也可以使用装饰器工厂
function WorksClassDec(type:string){
  return (target:Function)=>{
    console.log(`target Work and type value is ${type}`, target)
  }
};

@WorksClassDec('one')
class ClassPlayer {}
