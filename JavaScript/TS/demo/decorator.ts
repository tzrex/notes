/**
 * 装饰器本身就是一个函数，用于给被装饰物添加某种特性
 */

function move(target:Function){
  console.log("target is", target)
};

// 第二种定义方式
const Message:ClassDecorator = (target:Function)=>{}

// 装饰器工厂
function Returnmove(typeName:string){
  return (target:Function)=>{
    console.log("target return is", target, typeName)
  }
}

@move
class Tank{}

@move
class Player {}

@Message
class Moved {}

@Returnmove('person')
class Person {}

@Returnmove('peoper')
class Peoper {}
