```typescript
// 当前 TS 学习版本：
tsc -v // 4.4.4
```

## 装饰器与继承

### 装饰器

修饰关系，给某个事物添加另一个（些）事物。装饰器与被装饰物之间没有特定的关系。

### 继承

父子级的关系，子类可以使用父类的部分功能

## 装饰器使用

### 装饰器的本质

> 装饰器本身就是一个函数

```typescript
function move(target:function){
    console.log("target is", target)
};

// 执行打印出：target is [Function: Tank]
@move
class Tank {};

// 执行打印出：target is [Function: Player]
@move
class Player{}
```

可以看到：声明的 `Tank` 和`Player` 类已经作为参数传递到了 `move` 函数中。那么如果项给被装饰类增加某些方法。

**可以理解为**：

```typescript
// 先声明，以方便下文使用
function move(target:Function){};
class Tank{};

// 装饰器形式
@move
class Tank {};

// 函数参数的形式
move(Tank);
```

装饰器就是在类外面套了一层方法。

多个装饰器同时使用时，按照从上往下的执行顺序执行。（套娃）

```typescript
function a(target:Function){};
function b(target:Function){};
function c(target:Function){};

@a
@b
@c
class Player {};

// 等同于：
a(
	b(
    	c(
        	Player
        )
    )
)
```

### 装饰器与装饰器工厂

> 简单理解：带 () 的装饰器就是装饰器工厂

在上面我们知道装饰器就是一个方法，如果想让同一个装饰器在不同情况下有不同的表现，就需要用到 ‘装饰器工厂’

```typescript
// 普通装饰器
function DecoratorOne(target:Function){};

@DecoratorOne
class Person {};

// 装饰器工厂
function DecoratorNull(){
    // 外层包裹的方法的参数变更为装饰器在运行时调用的参数
    return (target:Function)=>{
        // 这里的 target 就是被装饰的构造函数
    }
};

@DecoratorNull()
class Player {};

function DecoratorParam(type:boolean){
    return (target:Function)=>{
        if(type){
            console.log("type value is true")
        }else {
            console.log("type value is false")
        }
    }
};

// 此处传入的 true 就是用上文的 type 接收
@DecoratorParam(true)
class Tank {};
```

#### 执行顺序

> 简单理解：return 出的方法是异步执行的

```typescript
function One(){
    console.log("one")
    return (target:Function)=>{
    	console.log("return one")
    }
}

function Two(){
    console.log("two")
    return (target:Function)=>{
    	console.log("return two")
    }
}

function Three(){
    console.log("three")
    return (target:Function)=>{
    	console.log("return three")
    }
}

@One
@Two
@Three
class Player {}
```

以上的装饰器多个同时调用的执行顺序：

`one` -> `two` -> `three` -> `return one` -> `return two` -> `return three`

### 类装饰器

> `ClassDecorator` ：类装饰器应用于类构造函数，可以用来监视，修改或替换类定义。

```typescript
// 用定义变量的方式定义
const Decorators:ClassDecorator = (target)=>{
    // target 就是类本身
    // 调用时不需要加() :  @Decorators
}

// 直接写function也可以，默认就是类装饰器
function Decorators(target:Function){}

// 上面两种装饰器都不能在调用时添加 ‘()’
@Decorators
class Player {}
```

### 方法装饰器

> `MethodDecorator`：用来监视，修改或者替换方法定义。

```typescript
// target：标识当前方法的上下文
// key：该方法的名称
const Method:MethodDecorator = (
	target:Object,
    key:string
)=>{
    // 打印 { show: [Function (anonymous)] }
    console.log("target", target);
    // 打印 show
    console.log("key", key)
};

class Person {
    @Method
    run (){
        console.log("this is function run")
    }
}
```

#### 模拟文本高亮