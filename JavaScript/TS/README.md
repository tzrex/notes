# 是什么

它是 `JavaScript` 的超集，在 `JavaScript` 的基础上扩展了许多的操作。并且具有类型检查的特点。

## 类型

### 类型批注

> 是一种用于给变量、函数参数、函数返回值等标识符指定类型的语法。

```typescript
const num: number = 10; // 将 num 变量批注为 number 类型
const str: string = "abc"
// 将函数 test 的参数 name 批注为 string 类型；函数返回值批注为 string 类型
function test(name:string): string {
    return "hello"
}
```

### 类型推断

> 当没有进行类型批注时，TS会根据数据进行自行推断

```typescript
const str = "hello" // 会默认推断为 string 类型。
// 函数的默认参数推断为 any；返回值默认推断为 void
function test(name) {}
```

### 类型擦除

> 它是与 `js` 无缝交互的基础之一。

在 `TypeScript` 向 `JavaScript` 编译的过程中，会将原本的类型信息进行去除，以保证编译后的 `JavaScript` 代码能顺利执行。

## 接口

> 用于描述对象类型。每个对象都是不一样的。

```typescript
// 该接口描述了包含且只包含 name 和 age 属性的对象。
interface Persion {
    name: string
    age: number
}
// 将 rex 变量批注为 Persion 类型。
const rex: Persion = {
    name: "Rex",
    age: 24
}
```



## 枚举

## Mixin

## 泛型

## 元组