# 一个简单的工程化
## 库列表

使用到的构建工具：

+ 语法约束：`eslint`
  + `module` 约束：也可以理解为 `es6` 之后的代码风格约束，`eslint-plugin-import`.
  + 配合 `prettier` 的插件：`eslint-plugin-prettier`.
  + 在 `vue` 框架中使用的插件：`eslint-plugin-vue`.
  + 常用的一些约束规则配置：`eslint-config-airbnb-base`. 需要前置库：`eslint-plugin-import`.
  + 结合 `prettier` 的配置：`eslint-config-prettier`.
+ 代码规整：包名 `prettier`，用于规范代码编辑的一些样式。
+ 样式约束：包名 `stylelint`.

## 下载

使用命令：

```shell
pnpm install \
eslint \
eslint-plugin-import \
eslint-plugin-prettier \
eslint-plugin-vue \
eslint-config-airbnb-base\
eslint-config-prettier \
prettier \
-D
# 这些包都是只在开发环境下使用，所以需要加上 `-D` 参数
```

安装的时候会提示需要下载一些扩展包。直接下载即可。

