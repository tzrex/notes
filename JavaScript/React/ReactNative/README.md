### 使用 svg 图片

调用 `react-native-vector-icons` 的rn第三方库。

```javascript
// 引入库名, 需要选择分类
import Icon from "react-native-vector-icons/AntDesign";
// Icon 组件有三个重要的属性 name  size  style
// name 属性的选择：参看文档网站。size为图片的大小。style是设置其他的一些属性
`<Icon name="home" size="24" style={{color:"blue"}}></Icon>`
```



### RN 支持 jsx 或者 js 的导出

在 RN 项目中，在项目配置文件中运行导出

```javascript
// 在 metro.config.js 这个文件中添加允许导出的命令
resolver: {                   
    // 允许导出的文件后缀
    sourceExts: ['jsx', 'js', 'ts', 'tsx'],
},
```



### RN链接原生库

**有些功能是基于安卓底层的API进行开发的，所以需要链接到原生的 java 虚拟机环境中，使能正确调用原生功能**

本编笔记的 `react-native` 版本为 0.64

```javascript
// 下载还是一样的下载
npm install {包名}
// 使用命令行链接
npx react-native link
// or
npx react-native link {需要链接的库名}
```

但有些时候会出现问题：（仅针对安卓）

```java
// 手动链接：
/*
1. 在 MainApplication.java 文件中引入原生包
2. 再在此文件中的 getPackages() 函数中实例化引入的包
3. 在 /setting.gradle 文件中引入包的地址
4. 在 /app/build.gradle 的 dependencies 对象中引入在 setting.gradle 中声明的包
*/

// setting.gradle
include ':react-native-camera'
project(':react-native-camera').projectDir = new File(rootProject.projectDir, '../node_modules/react-native-camera/android')
// /app/build.gradle
dependencies:{
    ...
    implementation project(":react-native-camera")
}
android:{
    ...
    defaultConfig:{
        ...
        missingDimensionStrategy 'react-native-camera', 'general'
    }
}
// AndroidManifest.xml
+ <uses-permission android:name="android.permission.CAMERA" />
+ <uses-permission android:name="android.permission.READ_EXTERNAL_STORAGE" />
+ <uses-permission android:name="android.permission.WRITE_EXTERNAL_STORAGE" />
+ <uses-permission android:name="android.permission.RECORD_AUDIO"/>
```

#### 安装包之后项目跑不起来

这时候可能会出现这样的报错：

```javascript
Task :app:compileDebugJavaWithJavac FAILED
// 然后让你检查环境依赖是否安装好
Failed to install the app. Make sure you have the Android development environment set up:
// 最后有个报错总结
 Command failed with exit code 1.
```

这种情况时，别慌。讲无法解析的包删除即可

```javascript
// android/app/src/main/java/com/{项目名称}/MainApplication.java
// 将上面这个路径中里面引用到的无法解析的库删掉
// 不知道该删哪些，可从报错的黑窗口进行查看
```

### 注意事项

`react-native` 的三方库各自依赖的 `Android SDK` 版本号不一样，需要进入 `Android Studio` 统一各自的版本号。版本号不一样虽然也可以跑起来，但会有隐患

