## 预备

### 前排提醒

该笔记的内容并不全面，只能说能解决日常工作中需要的技能。更多内容请前往官网查看：`https://webpack.docschina.org/concepts/`.

### 安装

> 需要 `NodeJs`

在自己的项目创建之初输入命令

```shell
# 不推荐全局安装
npm install webpack -D
npm install webpack-cli -D
npm install webpack-dev-server -D
```

查看是否安装成功

```shell
# 看到版本号即表示安装成功
webpack -v
```

### 为什么

因为各浏览器支持的 `JavaScript` 版本不一样，浏览器不支持 `ES6` . 所以通过打包时打包后的代码能在各平台上运行。

## 配置

在项目根目录下创建文件：`webpack.config.js`

```js
// 在花括号内书写配置项即可
module.exports = {}
```

### entry

> 定义入口文件，根文件

默认为：`./src/index.js`

```js
module.exports = {
    entry:'./src/index.js'
}
```

### output

> 输出属性

```shell
const { resolve } = require("path");

module.exports = {
    output:{
    	// 打包输出的文件名
    	filename:"main.js",
    	// 打包输出的文件夹路径（上面这个main.js文件放到哪）
    	path:resolve(__dirname, "dist")
    }
}
```

### mode

> 定义环境

* `development`：开发环境。本地写代码的环境
* `production`：生产环境。部署到线上运行的环境

```js
module.exports = {
    mode:"development" // 开发环境
}
```

### loader

> 处理非 `JavaScript` 和 `json` 文件。

```js
module.exports = {
	module:{
		rules:[
            // 详细 loader 配置
        ]
	}
}
```

#### 样式资源

> `css`、`less`、`sass`、`scss` 等都是样式资源。

```js
rules:[{
	// 匹配所有以 .css 结尾的文件
	test:/\.css$/,
	use:[
		// 以 style 的形式解析：创建 style 标签进行插入
        "style-loader",
        // 以 CommonJs 模块加载到js中
        "css-loader"
	]
}, {
	test:/\.less$/,
	use:["style-loader","css-loader","less-loader"]
}]
```

#### 图片资源

> 在 `img` 标签或样式资源中引入的图片等相关资源资源

**处理样式资源中的图片**：

```js
rules:[{
    test:/\.(jpg|png|gif)$/,
    // 需要下载 url-loader 和 file-loader
    loader:"url-loader", // use:["url-loader"] 同理
    // 配置项
    option:{
        // 小于8kb的图片资源会被 base64 处理成字符串
        limit: 8 * 1024,
        // 只使用 CommonJs 编译图片，否则会影响 html 中的图片
        esModule:false,
        name:"[hash:12].[ext]"
    }
}]
```

**`html` 中的图片**：

```js
rules:[{
    test:/\.html$/,
    // 处理 img 文件中的图片
    loader:"html-loader"
}]
```

#### 其他资源

> 类似于字体资源

```js
rules:[{
    // 排除这些后缀的文件
    exclude:/\.(css|js|html|less|scss|sass)/,
    loader:"file-loader",
    option:{
        name:"[hash:8].[.ext]"
    }
}]
```

### plugins

> 插件。扩展更多的功能。大多数功能都是由它处理

#### HTML资源

> 需要 `html-webpack-plgin` 插件

```js
const HtmlWebpackPlugin = require("html-webpack-plugin");

module.exports = {
    plugins:[
        // 参数传递根 html 文件的路径
        new HtmlWebpackPlugin({
            template:"./src/index.html",
            minify:{
                // 去除空格
                collapseWhitespace: true,
                // 去除注释
                removeComments:true
            }
        })
    ]
}
```

### 热更新

> 解决每次修改之后都需要跑命令的繁琐且重复问题

```js
const { resolve } = require("path");

module.exports = {
    // 在内存中打包，不会输出到磁盘
    devServer:{
        port:4500, // 端口
        open:false, // 是否打开浏览器
        // 构建后路径，与上文打包后路径推荐一样
        contentBase: resolve(__dirname, "dist"),
        // 启用 gzip 压缩
        compress:true
    }
}
```

## 环境搭建

### 样式文件

下载插件：`mini-css-extract-plugin`.

#### 提取样式

```js
const MiniCssExtractPlugin = require('mini-css-extract-plugin');

module.exports = {
    module:{
        rules:[{
            test:/\.css$/,
            use:[
                // 不再以 style 标签进行渲染，改用插件的 link 方式
                MiniCssExtractPlugin.loader,
                "css-loader"
            ]
        }]
    },
    plugins:[
        new MiniCssExtractPlugin({
            // 对输出的文件进行重命名
            filename:'css/main.css'
        })
    ]
}
```

#### 兼容性处理

> 下载 `postcss-preset-env` 和 `postcss-loader` 两个包。

设置 `webpack` 项

```js
module.exports = {
    module:{
        rules:[{
            test:/\.css$/,
            use:[
                MiniCssExtractPlugin.loader,
                // 修改 postcss 的默认配置
                {
                    loader:"postcss-loader",
                    options:{
                        ident:'postcss',
                        plugins:()=>[require('postcss-preset-env')()]
                    }
                }
            ]
        }]
    }
}
```

默认走 `production` 生产环境，与 `webpack-config.js` 中设置的 `mode` 无关。

```json
"browserlist":{
    "development":[
        "last 1 chrome version",
        "last 1 firefox version",
        "last 1 safari version"
    ],
    "production":[
        ">0.1%",
        "not dead",
        "not op_mini all"
    ]
}
```

#### 压缩

> 使文件更小，加载更快。

使用插件：`optimize-css-assets-webpack-plugin`.

```js
const OptimizeCssAssetsWebpackPlugin = require('optimize-css-assets-webpack-plugin');

module.exports = {
    module:{},
    plugins:[
        new OptimizeCssAssetsWebpackPlugin()
    ]
}
```

###  语法检查

> 推荐使用 `eslint-config-airbnb-base` 库

`eslint` 语法检查，针对于 `JavaScript` 的语法检查。

#### 安装相关库

```shell
# eslint-loader
# eslint
# eslint-config-airbnb-base
# eslint-plugin-import
npm install eslint-loader eslint eslint-config-airbnb-base eslint-plugin-import -D
```

#### 书写配置项

> 在 `npm` 的 `eslint-config-airbnb-base` 网站下有详细文档

在 `package.json` 中配置：

```json
"exslintConfig":{
    "extends":"airbnb-base"
}
```

在 `webpack` 中配置 `eslint` 检查

```js
module.exports = {
    rules:[{
        test:/\.js$/,
        // 忽略 node-module 里面的 js 文件（忽略第三方库）
        exclude:/node_modules/,
        loader:"eslint-loader",
        options:{
            fix:true // 自动修改代码中的不严谨部分
        }
    }]
}
```

#### 兼容性处理

> 将 `es6` 以上的代码转为 `es5` 的代码

需要 `babel-loader` 和 `@babel/presets-env` 包

##### 基本兼容

安装依赖包

```shell
# babel
# @babel/core
# babel-loader
# @babel/presets-env
```

```js
module.exports = {
    module:{
        rules:[{
            test:/\.js/,
        	exclude:/node_modules/,
            loader:'babel-loader',
            options:{
                presets: ['@babel/presets-env']
            }
        }]
    }
}
```

##### 全局兼容

下载包：`@babel/polyfill`. 全局引入即可。

##### 局部兼容

```shell
# 安装 core-js
```

书写配置：

```js
module.exports = {
    module:{
        rules:[{
            test:/\.js$/,
            exclude:/node_modules/,
            loader:"babel-loader",
            options:{
                presets:[
                    ['@babel/preset-env', {
                        useBuildIns:'usage',
                        corejs:{
                            version:3
                        },
                        target:{
                            chrome:'60',
                            firefox:'60',
                            ie:'9',
                            safari: '10',
                            edge: '17'
                        }
                    }]
                ]
            }
        }]
    }
}
```

### 代码压缩

生产环境自动压缩 `JavaScript` 代码，只需将 `mode:production` 即可。