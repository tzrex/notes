### 事件总线

> 用于任意组件之间通信

在项目的 `Vue` 实例的平级再挂载一个实例用于调用 `Vue` 的 `API`。

### 实现el-form

#### v-model

自定义组件使用 `v-model` 需要在组件的表单元素上绑定 `:value` 和 `@input`  两个属性

```vue
<!-- sct/components/children.vue -->

<template>
	<div>
        <input
               :value="value"
               @input="onInput"
               v-bind="$attrs"
        />
    </div>
</template>

<script>
export default {
    name:"rex-input",
    props:{
      value:String  
    },
    methods:{
        onInput(e){
            this.$emit("input", e.target.value)
        }
    }
}
</script>
```

像上面的代码，就可以在该组件上使用 `v-model` 实现双向绑定

```vue
<!-- src/page/father.vue -->
<template>
	<div class="father">
        <child v-model="formData.username"></child>
    </div>
</template>

<script>
import child from '@/components/children.vue';
    
export default{
    name:"father",
    components:{
        child
    },
    data(){return{
        formData:{
            username:""
        }
    }},
    
}
</script>
```

#### form组件

> 以 `el-input` 举例。此处仅为实现基本功能，样式什么的不管。

```vue
<template>
  <div class="custom-input">
    <!-- v-model其实就是 :value 和 @input 的语法糖 -->
    <input :value="value" @input="onInput" v-bind="$attrs" />
  </div>
</template>

<script>
export default {
  // 防止 custom-input 继承 attrs 的属性
  inheritAttrs:false,
  
  name:"rex-input",

  props:{
    value:{
      type:String,
      default:''
    }
  },

  // 获取form和formItem父组件
  inject:["rexForm", "rexFormItem"],

  methods:{
    onInput(e){
      // 事件响应，告知父组件内容的变化
      this.$emit("input", e.target.value);

      // 当 input 单用时是没有 rexFormItem 组件的
      if(this.rexFormItem){
        this.rexFormItem.$emit("validate", e.target.value)
      }
    }
  }
}
</script>
```

#### form-item

> 校验规则从form组件获取，想在`form-item` 也能使用 `rules` 自行设置 `props.rules` 即可。

```vue
<template>
  <div class="from-item">
    <label v-if="label">{{ label }}</label>
    <slot></slot>
    <p v-if="errorMsg">{{ errorMsg }}</p>
  </div>
</template>

<script>
import Schema from "async-validator";

export default {
  name:"rex-from-item",

  provide(){
    return {
      rexFormItem: this
    }
  },
  
  inject:['rexForm'],
  
  props:{
    label:{
      type:String,
      default:''
    },

    value:{
      type:String,
      default:''
    },
    
  },

  data(){return{
    errorMsg:""
  }},

  methods:{
    validate(){
      // 获取校验规则
      const rule = this.rexForm.rules[this.value];
      // 获取表单数据
      const value = this.rexForm.model[this.value];

      // 创建数据校验，此处使用到 async-validator 库
      const schema = new Schema({
        [this.value]: rule
      });

      // return 为全局数据校验做准备
      return schema.validate({
        [this.value]: value
      }, (err)=>{
        if (err) {
          this.errorMsg = err[0].message;
        } else {
          this.errorMsg = "";
        }
      })

    }
  },

  mounted(){
    // 执行数据校验，具体何时执行按自身需求即可
    this.$on("validate", ()=>{
      this.validate();
    })
  }
}
</script>
```

#### form 组件

```vue
<template>
  <div class="custom-from">
    <slot></slot>
  </div>
</template>

<script>
export default {
  name:"rex-from",
  props:{
    model:{
      type:Object,
      required:true
    },
    rules:Object
  },
  provide(){
    return {
      rexForm: this
    };
  },
  data(){ return {

  }},
  methods:{
    validate(call){
      // 获取formItem 项，并执行数据校验
      const task = this.$children.filter(item => item.value).map((item)=>item.validate());

      // 通知校验结果
      Promise.all(task).then(()=>{
        call(true)
      }).catch((err)=>{
        call(false)
      })
    }
  }
}
</script>
```



