class Stack {
    constructor(){
        this.list = [];
    };

    // 入栈
    push(ele){
        this.list.push(ele)
    };

    // 出栈
    pop(){
        return this.list.pop();
    };

    // 获取栈顶元素
    peek(){
        return this.list[this.list.length - 1]
    };

    // 是否为空
    isEmpty(){
        return this.list.length === 0
    };

    // 栈内元素个数
    size(){
        return this.list.length;
    };

    // 清空栈
    clear(){
        this.list = []
    };

    // 序列化
    parse(){
        return this.list.toString()
    }
};

let demoStack = new Stack();
let stacked = new Stack();

demoStack.push("1")
demoStack.push("2")
demoStack.push("3")

stacked.push("hello")

console.log("stack", demoStack.list, stacked.list)

module.exports = Stack;
