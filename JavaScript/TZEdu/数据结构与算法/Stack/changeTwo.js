const Stack  = require("./index");

function ChangeTwo(numb){
    let stack = new Stack();
    let rem = 0;
    let _numb = "";
    while (numb > 0) {
        rem = numb % 2;
        stack.push(rem);
        numb = Math.floor(numb / 2);
    };

    console.log(":", stack.isEmpty())

    while (!stack.isEmpty()) {
        _numb += stack.pop()
    }

    return _numb
};

console.log(ChangeTwo(4))
