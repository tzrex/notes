## 面向对象

**概括一类事物的特征从而进行抽象**。

### JS中代码示例

#### ES5

```javascript
function People(name, age, sex, height){
    this.name = name;
    this.age = age;
    this.sex = sex;
    this.height = height;
};
People.prototype.say = function(){
    console.log(this.name + "正在说话")
};

var Rex = new People("Rex", 28, "男", 168);
Rex.say(); // Rex正在说话
```

#### ES6

```javascript
class People{
    constructor(name, age, sex, height){
        this.name = name;
        this.age = age;
        this.sex = sex;
        this.height = height;
    };
    say(){
        console.log(this.name + "正在说话")
    }
};
let Rex = new People("Rex", 28, "男", 168);
Rex.say(); // Rex正在说话
```

### 三要素

```javascript
class Father{
    constructor(name, age){
        this.name = name;
        this.age = age;
    };
    say(){
        console.log(`${this.name}正在说话`)
    }
}
```

#### 继承

>获取父类中的所有东西

```javascript
// super 关键字继承。参数为需要继承的父类属性
class One extends Father{
    constructor(name, age, number){
        super(name, age);
        this.number = number;
    }
}
```

#### 多态

> 同一接口不同表现

```typescript
// 同一方法，返回不同的结果
class One extends Father{
  ...
  say(){
      console.log(this.name+"会说话")
  }
};
class Two extends Father{
    ...
    say(){
        console.log(this.name+"能说话")
    }
};
```

#### 封装

> 减少耦合，给数据增加权限

```typescript
class Person {
    constructor(name, age, number) {
        this.name = name;
        this.age = age;
        this.number = number;
    };
    static type:number = 0; // 静态属性
    protected name:string; // 私有的，仅当前类可用
    private age:number; // 子类能访问，示例不能访问
    number:number;  // 默认为：public
}
```

