## 设计原则

### 什么是 ‘设计’

**按照一种思路或标准来实现功能；同样功能可以有不同设计方案**。

### 设计准则

> 参考《Linux/Unix设计思想》

1. 小既是美

2. 让每个程序只做好一件事（单一职责）

3. 快速创建原型（某个样板，大致特征）

4. 舍弃高效率，取可移植性

   （真正的高效率往往是与某个场景相结合的，无法应用到其他场景）

5. 采用纯文本存储数据（提高读取效率）

6. 充分利用软件的杠杆效应（复用）

7. 避免强制性的用户界面（可视化界面是一个程序，会耗费大量资源）

8. 让每个程序都成为过滤器

   （每个程序输出想返回的结果，不想或不该返回的过滤掉）

* 寻求 90% 的解决方案 （满足绝大部分的需求，个别的不追求满足）

* 各部分之和大于整体

  （一个庞大的程序应该由许多完整的程序组合而成）

* 沉默是金

  （当程序的输出结果与想要的数据不相干时，尽量返回最简单的结果）

### 单一职责原则

* 一个程序只做好一件事
* 功能过于复杂则拆分开，各部分独立（参看 “各部分之和大于整体” 准则）

#### 例子

```typescript
/**
* 假设我们抽象一个动物类
*/
class Animal {
    // 动物的基本功能：跑
    run(animalName){
        console.log(`${animalName} can run`)
    };
    // 动物的基本功能：呼吸空气
    breath(animalName){
        console.log(`${animalName} can breathe air`)
    }
};

// 但是当我们实例时，就会发现有问题
let Dog = new Animal();
Dog.run('husky') // success 哈士奇可以跑动 结论正常

let Fish = new Animal();
Fish.run('carp') // error 鱼时水生动物，不能跑。输出错误

/**
* 很显然我们无妨将所有动物的基本特征全抽象出来
* “所有动物” 显然过于庞大
* 那么我就可以针对某一类的特征进行
*/
class LandAnimal {
    // 陆地动物的基本功能：跑
    run(animalName){
        console.log(`${animalName} can run`)
    };
    // 陆地动物的基本功能：呼吸空气
    breath(animalName){
        console.log(`${animalName} can breathe air`)
    }
}

class WaterAnimal {
    // 水生动物的基本功能：游
    run(animalName){
        console.log(`${animalName} can swimed`)
    };
    // 水生动物的基本功能：过滤水中的氧气
    breath(animalName){
        console.log(`${animalName} can filtered water`)
    }
}

let Dog = new LandAnimal();
Dog.run('husky') // success

let Fish = new WaterAnimal();
Fish.run('carp') // success
```

### 开放封闭原则

* 对扩展开放，对修改封闭
* 增加需求时，扩展新代码而不是修改已有代码

#### 例子

```typescript
// 不符合 “开闭原则” 的代码：
class Animal {
	constructor(type){
        this.type = type;
    };
    makeSound(){
        switch(this.type){
            case "dark":
                console.log(`dark can sound`)
                break;
            case "chicken":
                console.log(`chicken can sound`)
                break;
            case "dog":
                console.log(`dog can sound`)
                break;
        }
    }
}

let dark = new Animal("dark")
let chicken = new Animal("chicken")
let dog = new Animal("dog")

/**
* 所有动物都会叫
* 但随着我们需求的变化会不断的修改，动物的不断增多
* Animal 类里面的 makeSound 内容也在不断修改
* 这是不符合 “开闭原则” 的
*/
```

**正确的实例**：

```typescript
/**
* 同样的功能，但此时就不需要修改 Animal 类中的 makeSound 方法
* 减少程序代码的同时提高了代码稳定性
*/
class Animal {
    constructor(type){
        this.type = type;
    }
    makeSound(){
        console.log(this.type + "can sound")
    }
};

let dark = new Animal("dark")
let chicken = new Animal("chicken")
let dog = new Animal("dog")
```

### 李氏置换原则

> 核心思想就是：继承

* 引用基类（父类）的地方必须能透明的使用其子类对象
* 子类可以扩展父类功能，但不能改变父类原有功能

#### 例子

```typescript
// 先定义一个父类
class Parnt {
    constructor(a, b){
        this.a = a;
        this.b = b;
    };
    add(){
        const num = this.a + this.b;
        console.log("num", num)
        return num
    };
    other(){ return }
};
```

**子类继承父类**：

```typescript
// 错误的操作方法
class ChildErr extends Parnt {
    constructor(a, b){
        super(a, b) // 继承父类
    };
    // Error 子类不能修改父类的功能
    add(){
        const num = this.a - this.b;
        console.log("num", num)
        return num
    };
    // 子类可以修改父类中的抽象方法，也可以叫做多态
    // 但不能修改父类中的具体方法，必须保持透明
    // Success 这种是可以的，因为父类并没有具体指明该方法是什么
    other(){
        return this.a * this.b;
    }
};

let parnt = new Parnt(5, 4);
let child = new Child(5, 4);

parnt.add() // Success 理想结果 9
child.add() // Error 打印结果 1 不符合
```

**正确的使用**：

```typescript
class childSuccess extends Parnt {
    constructor(a, b){
        super(a, b)
    };
    // 想要实现 “删减” 这种操作
    // 应该进行扩展而不是修改父类原有的 “add”
    sub(){
        const num = this.a - this.b;
        console.log("num", num)
        return num
    }
}
```

**解释**：

`必须能透明的使用其子类对象` ：父类的方法子类继承之后，应该返回与父类同样的结果

```typescript
let parnt = new Parnt(5, 4);
let child = new Child(5, 4);

// Success 两种方法返回的结果是一样的
// 符合 “李氏置换原则”
parnt.add(); // 9
child.add(); // Success 9

```



### 接口独立原则

* 接口保持单一对立，避免 “胖接口”，建立单一的接口，同时接口中的方法尽量少
* 类似 “单一职责原则” （业务逻辑），这里更注重接口（方法数量）。

#### 解释

**接口**：例如安卓手机的充电接口，以前的 Android 和现在的 Type-C 总是一一对应的，厂家在生产 Type-C 时也并没有说要去兼容曾经的 Android 口。如果为了兼容曾经的 Android 口就做 “胖” 了。

而像插座这类，你在插上（对接）的时候也不能说缺项，不然会有安全隐患。

所以意思就是说：一个规定的接口，里面的内容不能多，也不能少。

#### 例子

```typescript
// 一个胖接口：
interface Animal {
    public eat():void;
    public sleep():void;
    public run():void;
    public walk():void;
};

// 实现该动物接口：狗
class Dog implements Animal {
    // 一切正常，看起来没有问题
    public eat():void {
        console.log("can eat")
    }
    public sleep():void {
        console.log("can sleep")
    }
    public run():void {
        console.log("can run")
    }
    public walk():void {
        console.log("can walk")
    }
}

// 实现该动物接口：蛇
class Snake implements Animal {
    public eat():void {
        console.log("can eat")
    }
    public sleep():void {
        console.log("can sleep")
    }
    // 这里就出现了问题，蛇不能跑和走，他只会爬
    public run():void {
        console.log("can not run")
    }
    public walk():void {
        console.log("can not walk")
    }
}

// 实现该动物接口：兔
class Rabbit implements Animal {
    public eat():void {
        console.log("can eat")
    }
    public sleep():void {
        console.log("can sleep")
    }
    // 兔子并不会跑。他只会跳
    public run():void {
        console.log("can not run")
    }
    public walk():void {
        console.log("can walk")
    }
}
```

由上面的例子可以看到：在一个胖接口中，不仅有时实现起来麻烦，而且也会导致维护成本的上升。

```typescript
// 符合 “接口独立原则” 的书写方式
interface Animal {
    public eat():void;
    public sleep():void;
}
 interface run {
     public run():void;
 }
 interface walk {
     public walk():void;
 }


// 实现该动物接口：狗
class Dog implements Animal, run, walk {
    // 一切正常，看起来没有问题
    public eat():void {
        console.log("can eat")
    }
    public sleep():void {
        console.log("can sleep")
    }
    public run():void {
        console.log("can run")
    }
    public walk():void {
        console.log("can walk")
    }
}

// 实现该动物接口：蛇
class Snake implements Animal {
    public eat():void {
        console.log("can eat")
    }
    public sleep():void {
        console.log("can sleep")
    }
}

// 实现该动物接口：兔
class Rabbit implements Animal, walk {
    public eat():void {
        console.log("can eat")
    }
    public sleep():void {
        console.log("can sleep")
    }
    public walk():void {
        console.log("can walk")
    }
}

// 此时，各实现也合理了许多。同时维护成本也降下来了
```

### 依赖倒置原则

> 简单理解：尽量面向对象，而不是面向过程

* 面向接口编程，依赖于抽象而不依赖具体
* 高层模块不应该依赖低层模块，二者都应该依赖其抽象而不是具体细节

#### 解释

**高层模块**：总是会依赖于低层模块，高层模块离不开低层模块

**低层模块**：最基础的模块，最纯净的，没有任何依赖

#### 例子

```typescript
/**
* 举例子：人吃食物
* 各类食物：低层模块，食物的宿命就是被吃掉
* 吃：一种抽象化的行为
* 人：高层模块，如果不依赖于食物人就会死亡
*/

class Person {
    constructor(name, gender){
        this.name = name;
        this.gender = gender;
    };
    gotoEat(food){
        console.log(this.name, food.go())
    }
};

class Eat {
    // 一个抽象的行为：去吃
    go():void { return }
};

class Banana extends Eat {
    // 实现的具体：吃一个香蕉
    go(){
        return "eat banana to one"
    }
};

class Apple extends Eat {
    go(){
        return "eat apple to one"
    }
};

class Orange extends Eat {
    go(){
        return "eat orange to one"
    }
};

let Rex = new Person("rex", "男");
let eatOrange = new Orange();

// 人拥有吃这种行为，吃什么？选择了橘子
Rex.gotoEat(eatOrange);
```

## 工厂模式

## 单例模式

