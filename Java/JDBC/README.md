## 连接服务器

> DriverManager

### 前置条件

需要驱动和jdbc包：

```xml
<!-- 需要jdbc驱动 -->
<dependencies>
    <!-- 驱动版本要 >= 当前服务器版本 -->
    <dependency>
        <groupId>mysql</groupId>
        <artifactId>mysql-connector-java</artifactId>
        <version>8.0.25</version>
    </dependency>
</dependencies>
```

### 开始连接

```java
package com.rex;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.SQLException;

// mysql5.x连接方式
// 协议号：数据库类型：// 数据库地址 / 数据库名

//mysql8 会多一个时间参数。 ?servcieTimeZone=UTC
public class App {
    public static void main( String[] args ) {
        // 连接数据库
        String url = "jdbc:mysql://localhost:3306/jdbc"+
            // mysql8 需要增加一个时间参数
                "?serviceTimeZone=Asia/Shanghai"+
            // 指定数据库以什么格式编译数据
                "&characterEncoding=utf8mb4"+
            // 当 characterEncoding = gb2312 && gbk 时为true
                "&useUnicode=false"
            // 传输保密
                "&useSSL=true";

        Connection connect = null;

        try {
            connect = DriverManager.getConnection(url, "jdbc", "mP7yd6D4YDC36JXw");
            System.out.println("success :" + connect);
        } catch (SQLException e){
            System.out.println("SQL error");
            e.printStackTrace();
        }finally {
            try {
                // 关闭数据库连接
                connect.close();
            }catch (SQLException e){
                e.printStackTrace();
            }
        }
    }
}
```

### 获取语句对象

> 连接之后只是一个实体，具体操作语句还需要再获取。（statement）

#### createStatement

```java
public static void main(String[] args){
    Connection connect = null;
    Statement state = null;
    ResultSet result = null;
    
    String Sql = "SELECT * FROM student WHERE" +
        "id='" + id + "'";
    
    try {
        // 连接数据库
        connect = DriverManager.getConnection(url, "jdbc", "mP7yd36JXw");
        
        // 不推荐使用，因为存在SQL注入的问题
        state = connect.createStatement();
        
        // 查询操作时使用 executeQuery
        // 增删改操作使用 executeUpdate
        result = state.executeQuery(Sql);
    } catch (SQLException e){
     	e.printStackTrace()
    } finally {
        try {
            if(result != null) result.close();
            if(state != null) state.close();
            if(connect != null) connect.close();
        }catch (SQLException e){
            e.printStackTrace();
        }
    }
}
```



#### prepareStatement

```java
public static void main(String[] args){
    Connection connect = null;
    PreparedStatement statement = null;
    ResultSet result = null;
    
    String Sql = "SELECT * FROM student WHERE name=? and gender=?";
    
    try {
        // 连接数据库
        connect = DriverManager.getConnection(url, "jdbc", "mP7yd6D4YDC36JXw");
        
        // 针对SQL注入而创造的方法，推荐使用
        statement = connect.prepareStatement(Sql);
        statement.setString(1, "张三");
        statement.setString(2, "男");
        
        result = state.executeQuery();
        
    } catch (SQLException e) {
        e.printStackTrace()
    } finally {
        try {
            if(result != null) result.close();
            if(state != null) state.close();
            if(connect != null) connect.close();
        }catch (SQLException e){
            e.printStackTrace();
        }
    }
}
```

## 增删改查

### 增加

```java
public class App{
    private Connection connect = null;
    
    public static void main(String[] args) throw Exception {
        insert()
    }

    public static void inert() throw Exception {
        String Sql = "insert into student values (null,?,?,?)";
        
        connect = DriverManager.getConnection(url, "jdbc", "mP7yd6D4YDC36JXw");
        
        PreparedStatement state = connect.prepareStatement(Sql);
        
        state.setString(1, "Rex");
        state.setString(2, "男");
        state.setDate(3, Date.valueOf("2000-4-4"));
        
        int i = state.executeUpdate();
        System.out.println("新增了" + i + "条数据");
        
        // 关闭连接
        if(state != null) state.close();
        if(connect != null) connect.close();
    }
}

```

### 修改

```java
public class App{
    private Connection connect = null;
    
    public static void main(String[] args) throw Exception {
        Update()
    }

    public static void Update() throw Exception {
        String Sql = "update student set name=? where id=?";
        
        connect = DriverManager.getConnection(url, "jdbc", "mP7yd6D4YDC36JXw");
        
        PreparedStatement state = connect.prepareStatement(Sql);
        
        state.setString(1, "flover");
        state.setInt(2, 4);
        
        int i = state.executeUpdate();
        System.out.println("修改了" + i + "条数据");
        
        // 关闭连接
        if(state != null) state.close();
        if(connect != null) connect.close();
    }
}
```

### 删除

```java
public class App{
    private Connection connect = null;
    
    public static void main(String[] args) throw Exception {
        Remove()
    }

    public static void Remove() throw Exception {
        String Sql = "delete from student where id=?";
        
        connect = DriverManager.getConnection(url, "jdbc", "mP7yd6D4YDC36JXw");
        
        PreparedStatement state = connect.prepareStatement(Sql);
        
        state.setInt(1, 5);
        
        int i = state.executeUpdate();
        System.out.println("删除了" + i + "条数据");
        
        // 关闭连接
        if(state != null) state.close();
        if(connect != null) connect.close();
    }
}
```

### 查找

```java
public class App{
    private Connection connect = null;
    
    public static void main(String[] args) throw Exception {
        Find()
    }

    public static void Find() throw Exception {
        String Sql = "SELECT * FROM student where gender=?";
        
        connect = DriverManager.getConnection(url, "jdbc", "mP7yd6D4YDC36JXw");
        
        PreparedStatement state = connect.prepareStatement(Sql);
        ResultSet result = state.executeQuery();
        
        state.setString(1, "男");
        
        while(result.next()){
            int id = result.getInt("id");
            String name = result.getString("name");
            Date birth = result.getDate("birth");
            
            System.out.println(
            	"编号：" + id +
                "姓名：" + name +
                "生日：" + birth 
            )
        }
        
        // 关闭连接
        result.close();
        if(state != null) state.close();
        if(connect != null) connect.close();
    }
}
```

## 事务

> 使用的是 Mysql 数据库。

```java
public class App{
    private Connection connect = null;
    PreparedStatement stateOne = null;
    PreparedStatement stateTwo = null;
    
    public static void main(String[] args) throw Exception {
        Carry()
    }

    public static void Carry() {
        String sqlOne = "UPDATE account set money=money-? WHERE id=?";
        String sqlTwo = "UPDATE account set money=money+? WHERE id=?";
        try{
            connect = DriverManager.getConnection(url, "jdbc", "mP7yd6D4YDC36JXw");

            // 取消自动提交，== set @@autocommit=0
            connect.setAutoCommit(false);

            stateOne = connect.prepareStatement(sqlOne);
            stateTwo = connect.prepareStatement(sqlTwo);

            // 模拟两人转账业务
            stateOne.setSInt(1, 500);
            stateOne.setString(2, 1);

            stateTwo.setSInt(1, 500);
            stateTwo.setString(2, 4);

            // 手动提交事务
            connect.commit();
            System.out.println("转账成功");
        } catch (Exception e){
            e.printStackTrace();
            try {
                connect.rollback(); // 事务回滚
                // 手动提交事务。出错时也需要提交事务，否则会一直占用
                connect.commit();
            	System.out.println("转账失败");
            } catch (SQLException err){
                err.printStackTrace();
            }
        } finally {
            try {
                // 关闭连接
                result.close();
                if(state != null) state.close();
                if(connect != null) connect.close();
            } catch(SQLException e) {
                e.printStackTrace();
            }
        }
    }
}
```

## 锁

## 数据库连接池

### c3p0

> 很古老的jar包。

#### 配置文件

`c3p0.properties`. 放置于 `src` 下。

```properties
c3p0.driverClass=com.mysql.cj.jdbc.Driver
c3p0.jdbcUrl="jdbc:mysql://localhost:3306/jdbc?serviceTimeZone=Asia/Shanghai&characterEncoding=utf8&useUnicode=false&useSSL=true"
c3p0.user=jdbc
c3p0.password=mP7yd6D4YDC36JXw
c3p0.maxPoolSize=20
c3p0.minPoolSize=5
c3p0.maxStatement=30
c3p0.maxIdleTime=150
```

属性解释

```shell
# driverClass	数据库驱动名称
# jdbcUrl		数据库地址
# user			数据库用户名
# password		数据库密码
# maxPoolSize	最大连接池数量
# minPoolSize	最小连接池数量
# maxStatement	最多创建statement数量
# maxIdleTime	闲置多久过期
```

#### 使用示例

```java
package com.tzrex;

import com.mchange.v2.c3p0.ComboPooledDataSource;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;

public class App {
    public static void main( String[] args ) {
        ComboPooledDataSource dataSource = new ComboPooledDataSource();
        Connection connect = null;
        PreparedStatement statement = null;

        try {
            // 除连接的getConnection之外，其余的不变
            connect = dataSource.getConnection();
            
            String Sql = "SELECT * FROM student";
            statement = connect.prepareStatement(Sql);

            ResultSet result = statement.executeQuery();

            System.out.println("result :" + result);

        } catch (SQLException e) {
            e.printStackTrace();
        }
    }
}
```

### Druid

> 阿里的开源jar包。

#### 配置文件

```properties
driverClassName=com.mysql.cj.jdbc.Driver
url="jdbc:mysql://localhost:3306/jdbcserviceTimeZone=Asia/Shanghai&characterEncoding=utf8&useUnicode=false&useSSL=true"
username=jdbc
password=mP7yd6D4YDC36JXw
initialSize=5
maxActive=10
maxWait=150
```

属性解释：

```shell
# driverClassName	数据库驱动名称
# url				数据库地址
# username			数据库用户名
# password			数据库密码
# initialSize		最大连接池数量
# maxActive			最多创建statement数量
# maxWait			闲置多久过期
```

#### 使用示例
