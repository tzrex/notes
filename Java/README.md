## 规定字符

### 注释

#### 单行注释

```java
// 双斜杠就是单行注释，斜杠后面的内容不会执行
// int num = 0
```

#### 多行注释

```java
// "/**/" 就是多行注释，在星号内部的内容不会执行
/**
* 此部分内容不会执行
*/
```

#### 文档注释

```java
// 文档注释就是在多行注释内加入装饰器，结合 javaDoc 生成文档
/**
 * @author Rex
 */
```

## 数据类型

> 声明变量：type key = value / value[];

### 数值类型

#### 整型

> 不包含小数点的数字

byte 范围：【-128，127】

short 范围：【-32768，32767】

int 范围：【-2147483648，2147483647】

long 范围：【-9223372036854775808，9223372036854775807】

```java
long longNumb = 50L // long 整型在声明时需要在数字后面加个 L 标识出来
```

#### 浮点型

float 范围：占据四字节（int 也占四字节）

```java
float numb = 50.4F // float 声明时需要用 F 标识出来
```

double 范围：占据八字节（long 也占八字节）

#### 进制表示

```java
int ten = 10;		// 表示十进制
int eight = 010;	// 8
int sexteen = 0x10;	// 16
```

### 字符类型

#### 字符

char：字符。一个汉字、数字、字母都是字符

```java
char ministr = 'A'
```

#### 字符串



## 类型转换

不能把对象类型转换为不相干的类型

在类型转换的时候需要注意 `内存溢出` 的情况

### 强制转换

高转低强制转

```java
// 同类型往小转
int numb = 450;
short miniNum = (short)numb;

// 不同类型进行转换
char l = '9';
byte i = (byte)l;
```

注意内存溢出：

```java
String l = "128";
byte i = (byte)l;

System.out.print("byte", i); // -128 结果不对

/** 因为 byte 的最大值为 127，但转换之后是数字 128 超过了最大值。
* 于是它跳到了下一个 byte 的第一位，也就是 -128
* 内存溢出导致的问题之一
*/
```

### 自动转换

低转高自动转

```java
short numb = 100;
int intNum = numb;
```

注意内存溢出：

```java
int money = 10_0000_0000; // JDK7新特性
int years = 20;

// 计算20年能挣的钱
long total = money * years; // != 200_0000_0000 结果超出预期
/**
* 因为在计算时已经发生了内存溢出，溢出后的值再赋值时是救不回来的。
* 正确操作如下：
*/

// 1:
long money = 10_0000_0000;
int years = 20;

long total = money * years;

// 2:
int money = 10_0000_0000;
int years = 20;

long total = ((long)money) * years;
```

### 隐式转换

```java
int numb = 10;
int num = 20;
String str = "";


```



## 运算符

### 算术运算符

1. `+`：相加
2. `-`：相减
3. `*`：相乘
4. `/`：相除以
5. `%`：取余数
6. `++`：累加
7. `--`：累减
8. `+=`：自身相加，举例 `a+=b == a=a+b` 下列原理相同。
9. `-=`：自身相减
10. `*=`：自身相乘
11. `/=`：自身相除以

### 关系运算符

1. `>`：大于
2. `<`：小于
3. `>=`：大于等于
4. `<=`：小于等于
5. `==`：等于
6. `!=`：不等于
7. `instanceof`：

### 逻辑运算符

1. `&&`：与
2. `||`：或
3. `!`：非

### 位运算符

1. `&`：
2. `|`：
3. `^`：
4. `~`：
5. `>>`：
6. `<<`：
7. `>>>`：

### 条件运算符

```java
int numb = 10;
String str = "";

numb > 20 ? str = "true" : str="false";

// 上面的那段话等同于：
if(numb > 20){
    str = "true";
}else{
    str="false";
}
```

## 包机制

> 包的本质就是文件夹，用于更好的组织项目	`package pkg1`

1. 一般利用公司域名倒置作为包名
2. 如果需要使用别人的包。需要使用 `import ` 关键字

```java
/*

例如公司的域名是：www.baidu.com

|- com
|---baidu
|----- www
|------- Hello.java
|----- baike
|------- world.java

package com.baidu.www;

import com.baidu.www.hello

*/
```

## javaDoc

>  配合文档注释，用于生成自己的 `API` 文档。

`javadoc -encoding UTF-8 -charset UTF-8`

```java
package main;

/**
* @author Rex
* @version 1.0.0
* @since 1.8
*/
public class Document{
    String demo;
    
    /**
    * @param name
    * @return 
    */
    public GetName(String name){
        return name;
    }
}
```

## 流程控制

### Scanner类

> java.util.Scanner  java5增加的特性

用于获取用户的输入。

```java
public class Hello{
  public static void main(String[] args){
    Scanner scanner = new Scanner(System.in);

    System.out.print(scanner);

	// 判断用户输入 hasNext
    if (scanner.hasNext()){ // 或者 hasNextLine()
      String str = scanner.next(); // 或者 nextLine()
      System.out.print("str is :", str);
    }

    // 关闭 scanner 节省资源
    scanner.close();
  }
}
```

#### 区别

`next()`：

1. 一定要读取到有效字符后才会结束。
2. 对于有效字符之前的空白会忽略，之后的空格会作为分割符或结束符。
3. 不能得到带空格的字符串。

`nextLine()`：

1. 以 `Enter` 为结束符，返回按回车之前的所有字符。
2. 可以获得空白。

### 选择

```java
package com.baidu.www;

public class Hello{
    public static void main(String[] args){
        static int numb = 20;
        static String str = "hello";
        
        if(numb > 30){
            System.out.print("大于");
        } else if(numb == 20) {
            System.out.print("等于20");
        } else {
            System.out.print("小于");
        }
        
        switch(str){
            case "hello":
            	System.out.print("str is", str);
                break;
            case "world":
            	System.out.print("str is", str);
                break;
            default:
            	System.out.print("str default", str);
                break;
        }
    }
}
```

### 循环

> 执行次数确定的情况下使用 for；不确定的情况使用 while

```java
package com.baidu.www;

public class Hello{
    public static void main(String[] args){
        int i = 0;
        
        // 先判断，后执行
        while(i < 100){
            i++;
            System.out.print("i data is", i);
        }
        
        // 先执行再判断
        do{
            System.out.print("i out put to", i)
        }while(i < 100);
        
        // 遍历
        for(int i=0; i< 100; i++){
            System.out.print("item is：", i);
        }
        
        // for 也可以这样：
        // 内部的条件都可以抽出来
        for(i<100; i++){
            System.out.print("item is：", i);
        }
    }
}
```

```java
package com.baidu.www;

public class Hello {
    public static void main(){
        int[] number = {10,20,30,40,50,60};
        
        // 主要用于循环数组
        for(int x:number){
            System.out.print("number item:", x);
        }
    }
}
```

### 习题：画个三角形

```java
package com.baidu.www;

public class Hello{
  public static void main(String[] args){
    for(int i=0;i<5;i++){
      for(int j = 5; j >= i; j--){
        System.out.print(" ");
      }

      for(int j = 1; j<=i; j++){
        System.out.print("*");
      }

      for(int j = 1; j<=i; j++){
        System.out.print("*");
      }

      System.out.println();
    }
  }
}
```

## 方法（函数）

> 一段用于完成特定功能的代码片段

```java
/**
* 修饰符 返回值类型 方法名 （参数类型 参数名）{
* 	代码块
* 	return 返回值;
* }
*
*/
public class Demo{
    public static void main(String[] args){
        int put = add(5, 10);
        System.out.print("put data:", put); // 15
    }
    
    public static int add(int a, int b){
        return a+b;
    }
}
```

### 参数特点

**`java` 和大多数语言一样，参数是值传递。不会改变实参的值**！！

```java
public class Hello {
  public static void main(String[] args) {
    int numOne = 0;
    int num = 0;
    numOne = change(num); // 此处传递的是 num 的值

    System.out.println(numOne); // 10
    System.out.println(num); // 0
  }

  static int change(int numb){
    numb = 100;
    return 10;
  }
};
```

### 重载

> 在一个类中：有相同的函数名称，但参数类型不同

```java
public class Hello {
  public static void main(String[] args) {

      double doub = add(20.00, 10.00);
      int numb = add(20, 10)
      
    System.out.println(doub); // 30.00
    System.out.println(numb); // 30
  }

    // 作为标记 A
  public static int add(int a, int b){
      return a + b;
  }
    
    // 作为标记 B
  public static double add(double a, double b){
      return a + b;
  }
};
```

### 命令行传参

> 希望程序运行时再传递给它消息，靠命令行参数给 `main()` 函数

```java
// 命令行执行：java Hello this is hello app

public class Hello {
  public static void main(String[] args) {
    for (int i = 0; i < args.length; i++) {
      /**
      * args = this
      * args = is
      * args = hello
      * args = app
      */
      System.out.println("args = " + args[i]);
    }
  }
};
```

### 可变传参

**一个方法中只能指定一个可变参数，必须是方法的最后一个参数**！！

```java
public class Hello {
  public static void main(String[] args) {
    Hello hello = new Hello();
    hello.test("test", 5, 5, 78, 123);
  }

  public void test(String str, int... ints){
    for (int i = 0; i < ints.length; i++) {
      /**
      * test=5
      * test=5
      * test=78
      * test=123
      */
      System.out.println(str + "=" + ints[i]);
    }
  }
};
```

### 递归

> 简单理解：函数调用自身，并有一个弹出条件退出循环调用。

```java
public class Hello {
  public static void main(String[] args) {
    Hello hello = new Hello();
    hello.test(12);
  }

  public void test(int zero){
    System.out.println("zero =" + zero);
    zero --;
    if (zero == 0) { // 弹出条件
      System.out.println("out put");
    } else {
      test(zero); // 不断调用自身
    }
  }
};
```

## 数组

1. 数组是引用类型。
2. 内部元素可以是任意数据类型
3. `java` 的数据对象本身存放在堆中

### 多维数组

> 简单理解：数组套数组。

```java
// 一维数组
int[] arr = {1,12,5,8,40}; // 静态声明
/* or */
int[] arr;
arr = new int(10); // 动态声明

// 二维数组
int[][] a = {
    {1,2,5},
    {7,2,8},
    {4,1,9}
};
int[][] a = new int[5][5]

// 三维数组
int[][][] mach = {
    {
        {4,7},
        {2,3}
    },
    {
        {8,7},
        {5,6}
    },
}
int[][][] mach = new int[10][10][10];


// ......
```

### Array 类

```java
// java 书写的类

import java.util.Arrays;
```

### 排序

#### 冒泡排序

随意举个例子

```java
public class Hello {
  public static void main(String[] args) {
    int[] arr = {21, 14, 2, 44, 89, 40, 74, 8};

    sort(arr, false);

    System.out.println(Arrays.toString(arr));
  }

  public static void sort(int[] arr, boolean type){
    int numb = 0;

    for (int i = 0; i < arr.length - 1; i++) {
      for (int j = 0; j < arr.length - 1 - i; j++) {
        boolean isTrue = type ? arr[j+1] > arr[j] : arr[j+1] < arr[j];
        if(isTrue){
          numb = arr[j];
          arr[j] = arr[j+1];
          arr[j+1] = numb;
        }
      }
    }
  }
};
```

### 稀疏数组

> 当一个数组中大部分元素都 是零或者相同 时，可以使用稀疏数组进行保存。

处理方式：

1. 记录数组一共几行几列，有多少不同值。
2. 将具有不同值的元素的 行列值 信息记录在一个小规模的数组中，从而缩小规模。

## 面向对象

> 属性 + 方法 = 类

**从认识角度：先有对象后有类。对象：具体的事物，类：抽象概括某种（些）特性**！！

**从代码运行：先有类再有对象，类是对象的模板**。

```java
// Main.java

package demo.src;

public class Main{
    public static void main(String[] args){
        
        // 调用并实例化对象
        // 使用 new 关键字，本质就是在调用构造器
        Person Rex = new Person("Rex", 22);
        
        System.out.println("Rex name: " + Rex.name);
    }
}
```

随便书写一个 demo

```java
// Student.java

package demo.src;

public class Student{
    static String name;
    static int age;
    
    // 类默认自带的构造器的样子
    public Student(){}
    
    // 构造器本身也是一个函数，所以可以通过重载进行扩展
    public Student(String name, int age){
        this.name = name;
        this.age = age;
    }
}
```

### 执行顺序

1. 加载 `main` 类，将类中的静态属性和方法放入 `方法区` 中。
2. 加载 `Student` 类，将 `Student` 的静态属性和方法放入 `方法区` 中。
3. 使用变量引用类，将对象存放到 `堆` 中。变量指向堆里面对象的内存地址。
4. 赋值对象中的各实参。
4. 完成 `new` 操作。

### 三大特征

#### 封装

程序设计追求：“ 高内聚，低耦合 ”。

* 高内聚：类内部数据操作细节自己完成，不允许外部干涉
* 低耦合：仅暴露方法给外部使用

#### 继承

> java 中的类只有单继承，没有多继承。

继承是**类与类之间**的关系，除此之外类之间的关系还有 **依赖、组合、聚合**。

```java
// extends 关键字

public class Demo extends Father {
    public Demo(){}
}
```

在继承父类的时候，子类构造器第一行默认有一个 `super ` 函数。

```java
public class Demo extends Father {
    public Demo(){
        super() // 调用父类的构造器
    }
}
```

#### 多态

> 同一方法根据不同发送对象的不同而采用不同的行为方式。

重载与重写的区别：

```java
// 重载：即同一方法进行扩展，增加新特性
public class Hello{
    public int getNum(int a){
        a = a^2;
        return a;
    }
    
    public String getNum(int a){
        return "" + a;
    }
}

// 重写只能发生在父子类之中。关键字修饰符名称都需要与父类相同
public class Rex extends Student {
    
}
```

##### 注意事项

1. 多态是方法的多态，属性没有多态。
2. `static` 方法不能进行重写。`final` 也不能重写。
3. 父类子类之间的关系转换，有时会类型转换异常（`ClassCastException`）。

### 关键字修饰符

**private**：私有的。仅自己能使用。

**protected**：受保护的。对象外部不能访问，可通过继承让子类获取。

**static**：静态的。不需要new，可直接通过 `.` 进行访问。属于类的。

**public **：公开的。实例之后通过 `.` 获取。属于实例对象的。

### 抽象类

> 抽象类无法使用 `new` 关键字。类的其它功能依然存在，成员变量、成员方法和构造方法的访问方式和普通类一样。

抽象类

```java
// 声明了 abstract 关键字的类就是抽象类
package demo.src;

// 抽象类
public abstract class Abst {
  
  // 抽象方法
  public abstract void doSomething();
    
  // 正常方法
  public String demoStr(){
    return "hello";
  }
}
```

#### 作用

将各个类中的公共功能进行抽离，使代码做到更多复用

### 接口

> 约束类，类的规范

```java
// 定义接口
interface ActionSchema {
  // public 和 abstract 关键字在接口中默认就有的
  // 正式书写时可不写这两关键字
  public abstract void run();
}

// 类使用接口。可通过 `,` 来实现多个
public abstract class Abst implements ActionSchema {
    
}
```

### 内部类

> 在一个类里面定义一个类。

```java
// 一个java类只能有一个 public class
public class Other {
  private int id;

  public void out(){
    System.out.println("this is out class");
  }

  // 内部类
  public class Inner {
    public void isin(){
      System.out.println("this is inner class");
    }

    // 可以获取外部类的私有方法
    public int getOutId(){
      return id;
    }
  }
    
  // 局部内部类
  public void method(){
    class InnerToMethod {
      public void in(){
        
      }
    }
  }
    
  // 静态内部类：只能访问静态属性or方法
  public static class StaticInner {
    
  }
}

// 这种也叫内部类，不过是在java文件内。
// 可用于书写测试模块
class Demo {
    
}
```

## 错误机制

> 出现不可预期的问题或结果都有可能触发 错误 机制。

1. 检查时异常
2. 编译时异常
3. 错误`Error`

### 异常

```java
/*
try: 捕获异常
catch：发生异常时执行
finally：异常之后执行
throw：类抛出异常
throws：方法上抛出异常
*/
public class Test {
    public static void main(String[] args){
        public int numOne = 0;
        public int numTwo = 1;
        
        try {
            System.out.println(numbTwo / numOne);
        } catch (Exception e) {
            System.out.println("出错了，错误等级：Exception")
        } catch (Throwable e){
            System.out.println("出错了，错误等级：Throwable")
        } finally {
            System.out.println("结束了")
        }
    }
}
```

### 自定义异常

> 继承于 `Exception` 类即可。

```java
public class MyError extends Exception {
  protected String a;

  public MyError(String msg){
    // super(msg); // 由父类发送信息

    this.a = msg;
  }

  @Override
  public String toString(){
    return "error is " + this.a;
  }
}
```

## 泛型

## 枚举

## 多线程

### 概念

#### 任务

