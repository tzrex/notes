## 概念

### 是什么

一款**持久层**框架，支持：`定制化SQL`、`存储过程`、`高级映射`。开源使用 XML 或注解来配置和映射原生类型、接口和 `java POJO` 为数据库中记录。

`Mybatis` 避免了几乎所有的 `JDBC` 代码和手动设置参数及获取结果集。

### 持久化

> 简单理解：让数据在断电之后重联依然可以正常访问的操作。

* 内存可以存储数据，但是断电之后内存中的数据就消失了
* 数据库、IO文件持久化等都是长期有效保存数据的手段

### 持久层

> 完成持久化工作的代码块。`Dao`、`Service`、`Controller` 等。

## 配置

### maven安装

```xml
<!-- https://mvnrepository.com/artifact/org.mybatis/mybatis -->
<dependency>
    <groupId>org.mybatis</groupId>
    <artifactId>mybatis</artifactId>
    <version>3.5.7</version>
</dependency>

```

### 配置环境

> 此处只列举基本的最关键的部分

在 `main\resources` 下添加 `mybatis-config.xml` 文件。

```xml
<?xml version="1.0" encoding="UTF-8" ?>
<!DOCTYPE configuration
        PUBLIC "-//mybatis.org//DTD Config 3.0//EN"
        "http://mybatis.org/dtd/mybatis-3-config.dtd">
<configuration>

    <environments default="development">
        <environment id="development">
            <transactionManager type="JDBC"></transactionManager>
            <dataSource type="POOLED">
                <property name="driver" value="com.mysql.cj.jdbc.Driver" />
                <property name="url" value="jdbc:mysql://localhost:3306/jdbc?serviceTimeZone=Asia/Shanghai&amp;characterEncoding=utf8&amp;useUnicode=false&amp;useSSL=true" />
                <property name="username" value="jdbc"/>
                <property name="password" value="mP7yd6D4YDC36JXw"/>
            </dataSource>
        </environment>
    </environments>
    
</configuration>
```

### 开始使用

#### mybatis工具类

> 需要使用文件IO流获取文件

正常书写 `java` 代码即可，获取文件并执行。

```java
package com.tzrex.utils;

import org.apache.ibatis.io.Resources;
import org.apache.ibatis.session.SqlSession;
import org.apache.ibatis.session.SqlSessionFactory;
import org.apache.ibatis.session.SqlSessionFactoryBuilder;

import java.io.IOException;
import java.io.InputStream;

public class Mybatis {
    public static SqlSessionFactory SqlSession;

    static {
        String resource = "mybatis-config.xml";
        try {
            InputStream stream = Resources.getResourceAsStream(resource);
            SqlSession = new SqlSessionFactoryBuilder().build(stream);
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    // SqlSession.openSession == JDBC的getConnection
    public static SqlSession Connect(){
        return SqlSession.openSession();
    }
}
```

#### 书写实体类

> 字段与服务器表字段相同即可

```java
package com.tzrex.pojo;

public class Student {
    private long id;
    private  String name;
    private String gender;
    private String password;
    private String account;

    public Student(){}

    public Student(long id, String name, String gender, String password, String account){
        this.id = id;
        this.name = name;
        this.gender = gender;
        this.password = password;
        this.account = account;
    }

    public long getId(){
        return id;
    }

    public void setId(long id){
        this.id = id;
    }

    public String getName(){
        return name;
    }

    public void setName(String name){
        this.name = name;
    }

    public String getGender(){
        return gender;
    }

    public void setGender(String gender){
        this.gender = gender;
    }

    public String getPassword(){
        return password;
    }

    public void setPassword(String password){
        this.password = password;
    }

    public String getAccount(){
        return  account;
    }

    public void setAccount(String account){
        this.account = account;
    }
}
```

#### 实体类接口

> 类接口，定义类应该实现的内容

```java
package com.tzrex.dao;

import com.tzrex.pojo.Student;

import java.util.List;

public interface StudentDao {
    List<Student> getStudentList();
}
```

#### 映射SQL语句

> 配置环境时是：configuration，映射是：mapper

以 `XML` 文件映射 `Mybatis` 的 `SqlSession` 类。替代之前的以接口实现类实现的数据库操作。

```java
<?xml version="1.0" encoding="UTF-8" ?>
<!DOCTYPE mapper
        PUBLIC "-//mybatis.org//DTD Mapper 3.0//EN"
        "http://mybatis.org/dtd/mybatis-3-mapper.dtd">
    
    <!-- namespace：类接口的包位置 -->
<mapper namespace="com.tzrex.dao">
    
    <!-- id：接口中定义的方法名 -->
    <select id="getStudentList" resultType="com.tzrex.pojo.Student">
        select * from student where id = #{id}
    </select>

</mapper>
```

#### 注册Mapper

> 在之前书写的 `mybatis-config.xml` 文件中配置。

```xml
<?xml version="1.0" encoding="UTF-8" ?>
<!DOCTYPE configuration
        PUBLIC "-//mybatis.org//DTD Config 3.0//EN"
        "http://mybatis.org/dtd/mybatis-3-config.dtd">
<configuration>
    ...
    <mappers>
        <!-- resource：书写xml映射文件路径 -->
    	<mapper resource="com/tzrex/dao/StudentMapper.xml" />
    </mappers>
</configuration>
```

#### 添加maven配置

> 由于 `maven` 约定大于设置，所以默认不会执行 `java` 之外的文件。

```xml
<build>
	<resources>
    	<resource>
        	<directory>src/main/resources</directory>
            <includes>
                <include>**/*.properties</include>
                <include>**/*.xml</include>
            </includes>
        </resource>
        
    	<resource>
        	<directory>src/main/java</directory>
            <includes>
                <include>**/*.properties</include>
                <include>**/*.xml</include>
            </includes>
        </resource>
    </resources>
</build>
```

#### 测试接口

> 测试文件存放到 `test` 目录下。

```java
package com.tzrex.dao;

import com.tzrex.pojo.Student;
import com.tzrex.utils.Mybatis;
import org.apache.ibatis.session.SqlSession;
import org.junit.Test;

import java.util.List;

public class StudentDaoTest {
    @Test
    public void test(){

        // 获取 SqlSession 对象
        SqlSession connect = Mybatis.Connect();

        // 执行SQL
        StudentDao studentMap = connect.getMapper(StudentDao.class);
        List<Student> list = studentMap.getStudentList();

        for (Student student:list){
            System.out.println("student :" + student);
        }

    }
}
```

