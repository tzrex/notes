# 基础

## 概念

###  基本概念

**传统项目管理分析**：

* `jar` 包不统一，不兼容。
* 工程升级维护繁琐。

#### 是什么

一个项目管理构建工具，将项目开发和管理过程抽象成一个项目模型（POM）。

POM：项目对象模型。

![what is maven](./maven.png)

蓝色框内的就是`maven`。

#### 作用

* 项目构建：提供标准的、跨平台的自动化项目构建方式。

* 依赖管理：方便快捷的管理依赖的资源（jar 包），避免版本之间的冲突。

* 统一开发结构：标准的、统一的项目结构。

  ```java
  // 标准java工程结构
  /**
  * |- src
  * |-- main			主业务
  * |---- java		业务代码书写路径
  * |---- resources	配置文件架
  * |-- test			测试模块
  * |---- java		测试代码书写
  * |---- resources	测试环境书写
  * |- pom.xml		maven 配置项
  */
  ```

### 仓库

> 用于存储资源，包含各种 `jar` 包。

#### 分类

* 本地仓库：自己电脑上存储的资源仓库，可通过获取远程仓库丰富库内容
* 远程仓库：为本地提供资源的仓库。
  * 中央仓库：`maven` 官方的库，都是开源的。
  * 私服：个人 / 公司范围内的仓库，也可从中央仓库获取资源。

### 坐标

> 定位资源在仓库中的位置，方便查询

仓库文档地址：`https://mvnrepository.com`。

#### 主要组成

* `groupId`：`maven` 项目隶属的组织（通常为域名反写）
* `artifactId`：项目的独一的名称
* `version`：版本号
* `package`：打包方式
* `build`：打包配置

## 开始

### 文件夹介绍

```java 
/**
* |- bin					可运行指令
* |--- mvn.cmd					maven 运行执行
* |- boot					maven 启动项
* |---plexus-classworlds.jar	maven 类加载器
* |- conf					配置管理
* |--- settings.xml				配置文件
* |- lib		maven 运行时依赖的 jar 包
*/
```

### 配置环境

**需要配置环境变量：`JAVA_HOME`、`MAVEN_HOME`**。

在 `cmd` 黑窗口内输入：

```shell
# 检查 java
java -version
# 检查maven
mvn
```

### 配置仓库

**修改 `maven` 中的 `conf/settings.xml` 文件**。

#### 安装地址

```xml
<!-- 在 settings 标签下将注释了的 localRepository 标签解放 -->

<localRepository>{{新的maven地址}}</localRepository>

<!-- 例如 -->
<localRepository>D:\Download\maven\repository</localRepository>
```

#### 下载路径

```xml
<!-- 在 settings 标签下将 mirrors 内注释了的 mirror 标签解放 -->
<!-- 此处使用的是华为镜像地址 -->

<mirror>
    <id>huaweicloud</id>
    <mirrorOf>*</mirrorOf>
    <url>https://repo.huaweicloud.com/repository/maven/</url>
</mirror>
```

### 构建命令

```shell
mvn compile			# 编译
mvn clean			# 删除
mvn test			# 测试
mvn package			# 打包
mvn install			# 下载包至本地
```

### 使用maven

#### 项目流程

1. 在项目的根目录添加 `pom.xml` 文件，并配置项目信息（参看 “工程结构” 中的示例）
2. 使用 `mvn compile` 进行项目编译，会默认生成一个 `target` 的文件夹。
3. 删除打包后的文件夹，此步骤不是必须的：`mvn clean`
4. 如果需要进行测试：`mvn test` 。
5. 项目打包，此步骤包含打包和测试：`mvn package` ，会在 `target` 目录下生成项目的压缩包。

`mvn install`：会下载 `package` 命令打好的包至模块安装目录下。

#### 创建模板

> `mvn archetype:generate`：在命令行中通常命令传参自动生成项目模板。

```shell
# 命令格式与参数

mvn archetype:generate
	-DgroupId={project-packing}
	-DartifactId={project-name}
	-DarchetypeArtifactId=maven-archetype-quickstart
	-DinteractiveMode=false
```

**示例**：模板有许多，以两个常用的举例子

```shell
# 创建java工程
mvn archetype:generate
	-DgroupId=com.itheima
	-DartifactId=java-project
	-DarchetypeArtifactId=maven-archetype-quickstart
	-DinteractiveMode=false

# 创建web工程
mvn archetype:generate
	-DgroupId=com.itheima
	-DartifactId=web-project
	-DarchetypeArtifactId=maven-archetype-webapp
	-Dversion=0.0.1-snapshot
	-DinteractiveMode=false
```

## 工程结构

> 介绍 `pom.xml` 的各项

**示例**：

```xml
<?xml version="1.0" encoding="UTF-8"?>
<project
  xmlns="http://maven.apache.org/POM/4.0.0"
  xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance"
  xsi:schemaLocation="http://maven.apache.org/POM/4.0.0 http://maven.apache.org/maven-v4_0_0.xsd">
  
  <modelVersion>4.0.0</modelVersion>
  <!-- <packaging>war</packaging> 默认编译为jar包 -->

  <!-- 项目信息 -->
  <groupId>rex</groupId>
  <artifactId>demo</artifactId>
  <version>1.0.0</version>
  <packaging>jar</packaging>

  <!-- 项目名称 -->
  <name>demo</name>
  <description>
    this is rex java demo project
  </description>

  <!-- jar依赖包的信息 -->
  <dependencies>
    <dependency>
      <groupId>junit</groupId>
      <artifactId>junit</artifactId>
      <version>4.13.1</version>
    </dependency>
  </dependencies>
    
  <build>
    <!-- 插件集合 -->
    <plugins>
      <!-- 插件内容 -->
      <plugin>
        <groupId>org.apache.tomcat.maven</groupId>
        <artifactId>tomcat7-maven-plugin</artifactId>
        <version>2.1</version>
        <!-- 插件配置项 -->
        <configuration>
          <port>1987</port>
          <path>/</path>
        </configuration>
      </plugin>
    </plugins>
  </build>
</project>
```

### 依赖构建

> 项目运行所需要的其他包，在 `dependsncies` 标签下配置

```xml
<dependencies>
    <!-- 需要的jar包的信息 -->
	<dependency>
        <groupId>log4j</groupId>
        <artifactId>log4j</artifactId>
        <version>1.2.13</version>
    </dependency>
    <!-- 一个包就配置一个dependency标签 -->
	<dependency>
        <groupId>junit</groupId>
        <artifactId>kunit</artifactId>
        <version>4.1.12</version>
    </dependency>
    <!-- 也可以使用别人写的项目，会将调用的项目的依赖进行传递 -->
	<dependency>
        <groupId>com.rex</groupId>
        <artifactId>demo</artifactId>
        <version>1.0.0</version>
    </dependency>
</dependencies>
```

### 依赖冲突

* 路径优先：当出现相同资源时，层积越深优先级越低；越浅越高。
* 声明优先：当相同资源在同层被依赖时，靠前的配置覆盖靠后的。

**隐藏依赖**：在能操作的jar包中进行修改，只是看不见。

```xml
<dependencies>
    <dependency>
        <groupId>log4j</groupId>
        <artifactId>log4j</artifactId>
        <version>1.2.14</version>
        <!-- 在自己的项目中可添加此来让别人在引用时获取不到此依赖 -->
        <optional>true</optional>
    </dependency>
</dependencies>
```

**排除依赖**：主动断开依赖，不获取jar包资源。

```xml
<dependencies>
    <dependency>
        <groupId>com.rex</groupId>
        <artifactId>demo</artifactId>
        <version>1.0.0</version>
        <!-- 需要排除的依赖集合 -->
        <exclusions>
            <!-- 具体需要排除的包信息 -->
        	<exclusion>
                <!-- 包坐标 -->
                <groupId>log4j</groupId>
                <artifactId>log4j</artifactId>
            </exclusion>
        </exclusions>
    </dependency>
</dependencies>
```

### 依赖范围

> 设定jar包的使用范围

**举例说明**：

* `compile`：默认值，任何地方都能使用。
* `test`：仅测试环境可使用。
* `provided`：都可以使用，但不参与打包上传。
* `runtime`：仅打包。

|  scope   | 主代码 | 测试 | 打包 |   包举例    |
| :------: | :----: | :--: | :--: | :---------: |
| compile  |   √    |  √   |  √   |    log4j    |
|   test   |        |  √   |      |    junit    |
| provided |   √    |  √   |      | servlet-api |
| runtime  |        |      |  √   |    jdbc     |

**书写举例**：

```xml
<dependencies>
    <dependency>
        <groupId>junit</groupId>
        <artifactId>junit</artifactId>
        <version>4.1.12</version>
        <!-- 通过 scope 标签来设置范围 -->
        <scope>test</scope>
    </dependency>
</dependencies>
```

**依赖传递性**：优先级与构建层级一样，越浅优先级越高。

## 生命周期

**例如**：从编译到可下载的过程时不可调换的。

```java
// compile -> test-compile -> test -> package -> install
```

执行 `test` 时会按照顺序执行完 `compile` 和 `test-compile` 再执行。

### 插件详解

插件地址：`https://maven.apache.org/plugins/index.html`.

* 插件与生命周期内的阶段绑定，在执行到对应生命周期时执行对应的插件功能
* 插件有默认执行的内容，可通过插件自定义其他功能。
* 实际上插件不仅仅在生命周期中使用，也可作为项目功能扩展的手段。

#### 举例

```xml
<!-- plugins 标签用于配置环境 -->

<build>
    <plugins>
        <plugin>
            <groupId>org.apache.maven.plugin</groupId>
            <artifactId>maven-source-plugin</artifactId>
            <version>2.2.1</version>
            <!-- 执行 -->
            <executions>
                <execution>
                    <goals>
                        <goal>jar</goal>
                    </goals>
                    <!-- 插件执行声明周期 -->
                    <phase>generate-test-resources</phase>
                </execution>
            </executions>
        </plugin>
    </plugins>
</build>
```

# 高级

## 模块开发

> 将一般的三层架构项目拆分成每层都是单独的工程，工程之间通过接口访问。

```shell
# |- src
# |-- main
# |---java
# |---- com.rex
# |----- controller
# |----- dao
# |----- domain
# |----- service
# |-- test
# |--- java
# |---- com.rex
# |----- DemoTest.java
# |- pom.xml
```

将其中的 `controller`、`dao`、`domain`、`service` 模块拆分成独立的项目。

```shell
# |- ssm_controller
# |-- src
# |-- pom.xml
# |-- ssm_service.iml
# |
# |- ssm_dao
# |-- src
# |-- pom.xml
# |-- ssm_dao.iml
# |
# |- ssm_domain
# |-- src
# |-- pom.xml
# |-- ssm_domain.iml
# |
# |- ssm_service
# |-- src
# |-- pom.xml
# |-- ssm_service.iml
# |
```

