##### 开始

每一个 `go` 的项目都必须有一个 `main` 的根包，该项目中的其他包都是该包的子包。且必须有一个名为 `main` 的函数

```go
package main

func main() {}
```

`build`  ：仅编译项目

```bash
go build main.go
```

`run` ：编译并运行项目，但不生成 `.exe` 结尾的包

```shell
go run main.go
```

## 四类数据类型

### 基本类型

#### 数字

```go
// 整数
var integer int = 10
var integer8 int8 = 127
var integer16 int16 = 32767
var integer32 int32 = 2147483647
var integer64 int64 = 9223372036854775807
// 浮点数
var floatVar32 float32 = 2147483646.99999999
var floatVar64 float64 = 9223372036854775806.99999999
```

数字相加超出当前类型的最大边界时会溢出，所以需要注意

```go
var intOne int8 = 100
var intTwo int8 = 190
fmt.Println("end : ", intOne + intTwo) // -66
```

#### 字符串

字符串有一个个字符组成

```go
var singleStr = 'R' // 字符
var str = "Rex" // 字符串
```

#### 布尔值

布尔值只有 `true` 和 `false` 两个值

```go
var boolVar bool = false
var boolVar bool = true
```

### 聚合类型

#### 数组

* 特点：一组固定长度及类型的数据结构。
* 注意事项：
  * 必须在声明或初始化时定义数组的长度。
  * 数组是 `切片` 和 `映射` 的基础！！！

常见的声明方式

```go
func main() {
    var arr1 = [4]int{}
    var arr2 [5]int
    arr3 := [3]int{2, 4, 6}
    arr4 := [...]int{}
    arr5 := [...]int{1, 2, 3, 4, 5}
    arr6 := [...]bool{9: true}
    
    // len函数是go内置的一个方法，用于获取数组、切片或映射中的长度。
    fmt.Println("arr5 =>", len(arr5), arr5)
    // 在数组的第9项初始化了一个true的值，所以go会默认该数组的长度为10
    fmt.Println("arr6 =>", len(arr6), arr6)
}
```

##### 多维数组

```go
// 数组中支持每项数据类型为数组的套娃现象，这也是多维数组的基础条件
func main() {
    // 声明一个长度为4，内置数据类型为长度为3的int类型数组
    var doubleArr = [4][3]int{}
	fmt.Println("doubleArr =>", doubleArr)
    
    // 打印结果：
    // doubleArr =>[[0 0 0] [0 0 0] [0 0 0] [0 0 0]]
}
```

#### 结构

定义结构类型时，需要使用 `type` 关键字，并用 `struct` 关键字标注为 结构数据类型。

使用时则于调用其他数据类型一样的使用。

```go
// 定义结构
type Student struct {
    id int
    name string
    age int8
}

func main() {
    // 使用结构，不写基础值时则均为默认值
    var Rex1 Student
    var Rex2 = Student{}
    
    // 也可以在声明时赋予基础值
    var Rex3 = Student{1, "Rex", 23}
    // 上述情况会根据定义内部属性时的顺序进行赋值，多数情况还是带上属性名为好
    var Rex4 = Student{
        id: 1,
        name:"Rex",
        age:23
    }
    
    // 也可以在后续的操作中赋予值
    Rex1.name = "Rex"
    Rex2.age = 23
}
```

##### 组合结构

定义的 `Student` 其实可以将其看作一种自定义数据类型

```go
// 定义时直接将类型当作变量名写入，可在使用时少书写一层
type Employee struct {
    Student
    message string
}

type EmployeeTwo struct {
    persion Student
    message string
}

func main() {
    var employeeTwo EmployeeTwo
    
    // 当组合结构中定义了具名属性，则需要多一次 · 操作符选取
    employeeTwo.persion.age = 23
    
    var employee = Employee{
        Student: Student{
            id: 1,
            name:"Rex",
            age:23
        },
        message: "组合式结构"
        
        // 赋值时少一层
        employee.age = 32

        fmt.Println("employee", employee)
        // 也可以直接查看值
        fmt.Println("employeeDetail", employee.name, employee.address)
        }
}
```

##### 使用 JSON 编码和解码

可使用结构来对 JSON 中的数据进行编码和解码。

`struct` 内的属性<strong>只有是开头大写字母的</strong>才可被 `json` 识别！！

```go
type Shcema struct {
    Id int
    Name string
    Address string
    // Desc 属性会在json中被改变成 description 字段
    Desc string `json:description`
    Email string
}

func main() {
    shcemaOne := Shcema {
        Id: 1,
        Name: "shcema",
        Address: "成都市成华区",
        Desc: "这是一条说明",
        Email: "xxxx@gmail.com",
    }
    
	result, _ := json.Marshal(shcemaOne)
    fmt.Printf("%s\n", result)
    // {"Id":1,"Name":"shcema","Address":"成都市成华区","description":"这是一条说明","Email":"xxxx@gmail.com"}
}
```



### 引用类型

#### 指针

在 Go 中，有两个运算符可用于处理指针：

- `&` ：调用对象保存的地址位置（指针）
- `*` ：根据指针获取对应的值，只能作用于 `&` 

```go
func main() {
    var str = "Rex"
    // 打印内容
	fmt.Println("string =>", str) // Rex
    // 打印一个16进制的数字，表示在内存中对应的位置
	fmt.Println("pointer =>", &str) // 0xc000088220
}
```

#### 切片

与数组更重要的区别是切片的大小是动态的，不是固定的。<strong>切片只是基础数组的一个子集</strong>。

切片是数组或另一个切片之上的数据结构。 我们将源数组或切片称为基础数组。 通过切片，可访问整个基础数组，也可仅访问部分元素。

```go
func main() {
    // 与定义数组的区别就在于没有给定长度，此时会在内存中创建一个长度等于容量的同类型数组
    months := []string{"January", "February", "March", "April", "May", "June", "July", "August", "September", "October", "November", "December"}
    fmt.Println("Array =>", months)
    fmt.Println("Length =>", len(months))
    fmt.Println("Capacity =>", cap(months))
}
```

##### s[i:p]

切片运算符，意为从数组或另一个切片上切下一部分

`len()` 获取数组或切片的长度，`p-i = len()`

`cap()` 访问原数组的剩下的容量，`len(s)-i = cap()`

```go
func main() {
	var arr = [20]int{}

	one := arr[0:10] // 从 arr 的下标0开始，向后切割10个元素
	two := one[2:4]
	three := arr[0:6]

	fmt.Println(one, len(one), cap(one)) // [0 0 0 0 0 0 0 0 0 0] 10 20
	fmt.Println(two, len(two), cap(two)) // [0 0] 2 18
	fmt.Println(three, len(three), cap(three)) // [0 0 0 0 0 0] 6 20
}
```

##### 操作

```go
func main() {
    var base = []int{1, 2, 3, 4}
    
    fmt.Println(base, len(base), cap(base)) // [1 2 3 4] 4 4
    
	// 增加：go提供了追加数据的方法append
	base = append(base, 5)
    
    // 当追加数据超过切片的最大长度时，会替换底层生成的数组，并将其原数组的长度扩大2倍
	fmt.Println(base, len(base), cap(base)) // [1 2 3 4 5] 5 8
    
    // 修改：直接操作下标重新赋值即可。
    // 但是需要注意的是：此修改本质是修改其对应的原数组的该下标的值
    base[0] = 10
    fmt.Println(base, len(base), cap(base)) // [10 2 3 4 5] 5 8
    
    // 删除：go并没有提供删除操作的方法，但我们可以通过 切片运算符 重写切割以达到删除的效果
    var index = 2 // 假设删除下标为2的元素
    // 三个点，表示解构运算
    base = append(base[0:index], base[index+1:]...)
	fmt.Println(base, len(base), cap(base)) // [10 2 4 5] 4 8
}
```

##### 创建副本

Go 具有内置函数 `copy(dst, src []Type)` 用于创建切片的副本。 你需要发送目标切片和源切片。

```go
func main() {
	letters := [5]int{1, 2, 3, 4}

	sliceOne := letters[0:3]
	sliceTwo := letters[1:4]
	fmt.Println("Before sliceOne =>", sliceOne) // Before sliceOne => [1 2 3]
	fmt.Println("Before sliceTwo =>", sliceTwo) // Before sliceTwo => [2 3 4]

	sliceOne[2] = 10
	fmt.Println("After sliceOne =>", sliceOne) // After sliceOne => [1 2 10]
    // 可以看到，当修改 `sliceOne` 的值时，`sliceTwo` 也发生了改变。
	fmt.Println("After sliceTwo =>", sliceTwo) // After sliceTwo => [2 10 4]
}
```

使用 `copy` 关键字

```go
func main() {
    letters := [5]int{1, 2, 3, 4}

	sliceOne := letters[0:3]
    sliceTwo := make([]int, 3)
    copy(sliceTwo, letters[1:4]) // 复制 letters[1:4] 返回的值到 sliceTwo 这切片中
	fmt.Println("Before sliceOne =>", sliceOne) // Before sliceOne => [1 2 3]
	fmt.Println("Before sliceTwo =>", sliceTwo) // Before sliceTwo => [2 3 4]

	sliceOne[2] = 10
	fmt.Println("After sliceOne =>", sliceOne) // After sliceOne => [1 2 10]
    // sliceTwo 的值没有受到影响
	fmt.Println("After sliceTwo =>", sliceTwo) // After sliceTwo => [2 3 4]
}
```

#### 映射

需要使用 `map` 关键字来创建映射数据结构。其使用方法为：`map[T]T{}`

```go
func main() {
    // 第一种创建方式
    var StudentAge = map[string]int{
        "Rex": 23,
        "Flover": 24,
        "xv": 26
    }
    
    // 第二种创建方式（任选其一既可）
    var StudentAge = make(map[string]int)
    
    fmt.Println(StudentAge) // map[Flover:24 Rex:23 xv:26]
}
```

##### 操作

```go
func main() {
	var studentsAge = map[string]int{
		"Rex": 23,
	}

	// 给映射增加数据：映射的内容是动态的，可以任意添加
	studentsAge["Flover"] = 24
	studentsAge["Hello"] = 100

    // 访问数据：直接以键进行获取就行，访问没有的值时会返回对应类型的默认值
	fmt.Println("studentsAge", studentsAge["Xv"]) // 26
	fmt.Println("studentsAge", studentsAge["Red"]) // 0

	// 删除数据
	delete(studentsAge, "Hello")
	delete(studentsAge, "World") // 删除不存在的键时，会执行无效操作

	fmt.Println("studentsAge", studentsAge) // map[Flover:24 Rex:23]

	// 映射的循环。range关键字也可以作用于数组和切片
	for name, age := range studentsAge {
        // 打印的结果：
        // Rex     23
        // Flover  24
		fmt.Printf("%s\t%d\n", name, age)
	}
}
```

#### 通道

> 通道用于协程之间的通信。

```go
func main() {
    // 以 go 关键字开启协程
    go func () {
        
    }
}
```

### 接口类型

#### 接口

## 函数

数据类型之一，由于学习顺序的问题，暂时没有放在 `引用类型` 下。对，函数也是一种引用类型

### 根函数

一个 `go` 的项目有且只有一个 `main` 函数，且在 `main` 包中

```go
package main

import "fmt"

func main() {
    fmt.Println("hello world")
}
```

##### 传参

可使用 `os` 包的 `os.Args` 获取，`os.Args` 变量包含传递给程序的每个命令行参数。

```go
package main

import (
	"fmt"
	"os"
)

func main() {
	// os.Args[0]：main.go 的位置
	fmt.Println("env is :", os.Args[1]) // env is : development
}
```

执行该项目

```shell
go run main.go development
```

### 自定义函数

使用 `func` 关键字声明，所有自定义函数最终都会运行在 `main` 函数中

```go
func test() {}
```

#### 传参

```go
func main() {
    test("Rex")
}

func test(name) {
    fmt.Println("name is : ", name) // name is : Rex
}
```

#### 返回值

```go
func main() {
    singleOne := singleOneReturn()
    singleTwo := singleTwoReturn()
    
    manyOne, manyTwo := manyReturn()
    manyThree, _ := manyReturn() // _ 是特殊的变量，常用于忽略某个函数的返回值
}

func singleOneReturn() int {
    return 10
}

func singleTwoReturn() (result int) {
    result = 10
    return
}

func manyReturn() (one int, two int) {
    one = 10
    two = 20
    return
}
```

#### 值传递

参数传递的是变量的值

```go
func main() {
    name := "Rex"
    test(name)
    fmt.Println("name : ", name) // name : Rex
}

func test(name string) {
    name = "Flover"
}
```

#### 指针传递

若想调用同一个变量，可使用该变量的指针。

```go
package main

import "fmt"

func main() {
    name := "Rex"
    updateName(&name)
    fmt.Println(name) // Flover
}

// 传递字符串数据的指针（地址）
func updateName(name *string) {
    *name = "Flover"
}
```

## 控制流

#### if else

```go
func main () {
    // 在if语句外声明变量
    judOne := true
    if judOne {
        fmt.Println("this is true")
    } else {
        fmt.Println("this is false")
    }
    
    // 在语句内声明变量，numb在当前if流中都可以使用（从第一个if到最后一个）
    if numb := 10; numb < 10 {
        fmt.Println("small")
    } else if numb > 10 {
        fmt.Println("big")
    } else {
        fmt.Println("default")
    }
}
```

#### switch

##### 单个分支

```go
package main

import (
	"fmt"
	"math/rand"
	"time"
)

func main() {
	sec := time.Now().Unix()
	rand.Seed(sec)
	i := rand.Int31n(10)

	switch i {
	case 0:
		fmt.Println("zero")
	case 1:
		fmt.Println("two")
	case 3:
		fmt.Println("three")
	default:
		fmt.Println("no match")
	}
}

```

##### 多个分支

```go
func main() {
    sec := time.Now().Unix()
    rand.Seed(sec)
    i := rand.Int31n(10)
    
    switch i {
    case: 1,3,5,7,9:
        fmt.Println("this is odd number")
        case 2,4,6,8:
        fmt.Println("this is even number")
        default:
        fmt.Println("no match")
        
    }
}
```

##### 函数条件

```go
func main() {
    switch time.Now().Weekday().String() {
        case "Monday", "Tuesday", "Wednesday", "Thursday", "Friday":
        fmt.Println("this is work day")
        default:
        fmt.Println("time is reset")
    }
}
```

##### 省略条件

`case` 由原本的条件分支转变为判断布尔值，为 `true` 时就执行该分支

```go
func main() {
    rand.Seed(time.Now().Unix())
    r := rand.Float64()
    
    switch {
        // 当满足 r > 0.1 这个条件时就会进入该分支
        case r > 0.1:
        fmt.Println("90%")
        default:
        fmt.Println("10%")
    }
}
```

##### fallthrough

若在满足条件的 `case` 分支中有该关键字，则会使该分支临近的下一条语句忽略条件直接执行。

了解即可。

```go
func main() {
    switch numb := 15; {
        case numb < 20:
        fmt.Printf("%d less to 20 \n")
        fallthrough
        case numb > 30:
        fmt.Println("%d geater to 30") // 该条语句会因为fallthrough关键字而执行
    }
}
```

#### for

##### 基本用法

```go
func main() {
    sum := 0;
    for num := 0; num < 10; num ++ {
        sum += num
    }
    fmt.Println("sum of 1 to 10 is :", sum)
}
```

##### 预处理和后处理

`go` 中没有其他语言的 `while` 语句，但可以使用预处理达到 `while` 语句的效果

```go
func main() {
    var numb = int64
    rand.Seed(time.Now().Unix())
    // 当 numb 等于 5 的时候就停止循环
    for numb != 5 {
        numb = rand.Int63n(15)
        fmt.Println("numb :", numb)
    }
}
```

##### 死循环

和其他语句一样，当没有结束条件时循环就会一直执行，直到系统宕机

```go
func main() {
    for {
        fmt.Println("for running...")
    }
}
```

但也可以通过在内部使用 `break` 关键字跳出循环

```go
// 和其他语言一样，break 的作用是跳出循环
func main() {
    var num int32
    sec := time.Now().Unix()
    rand.Seed(sec)
    for {
        // 当 num == 5 时便跳出循环不再执行语句
        if num = rand.Int31n(10); num == 5 {
            fmt.Println("for is break")
            break
        }
    }
}
```

当然 `go` 也有其他语言中所有的 `continue` 关键字，作用也一样

```go
// 由于 continue 的作用是跳过此次执行而没有跳出循环体，所以 continue 并不能打断死循环
func main() {
    for i := 0; i < 10; i++ {
        // 当 i == 4 时，就跳过当前执行，进入下一次循环
        if i == 4 {
            continue
        }
    }
}
```

### 函数控制流

#### defer

`defer` 关键字会延迟执行函数。多个 `defer` 时会按照 `栈` 结构的特性执行

```go
func main() {
    // 打印结果：1 2 3 -3 -2 -1
    for i := 1; i < 3; i++ {
        defer fmt.Print(-i, "")
        fmt.Print(i, "")
    }
}

// 每执行到 defer 时就会推入到一个栈中，当执行栈时则会按照后入先出的原则执行
```

一般用于需要延迟执行的操作中，例如文件的关闭操作

```go
package main

import (
    "io"
    "os"
    "fmt"
)

func main() {
    newfile, error := os.Create("learnGo.txt")
    if error != nil {
        fmt.Println("Error: Could not create file.")
        return
    }
    // 文件执行完之后需要关闭，但又不能马上关闭
    defer newfile.Close()

    if _, error = io.WriteString(newfile, "Learning Go!"); error != nil {
	    fmt.Println("Error: Could not write to file.")
        return
    }

    newfile.Sync()
}
```

`defer` 是作用于函数的关键字，可以简单理解为是将需要执行的语句倒放到了函数的最后面

```go
package main

import "fmt"

func main() {
    // 执行之后打印的结果：
    // one : 1
	// one : 2
	// defer one : -2
	// defer one : -1
	// defer one : 0
	// two : 0
	// two : 1
	// two : 2
	// defer two : -2
	// defer two : -1
	// defer two : 0
	oneDefer()
	twoDefer()
}

func oneDefer() {
	for i := 0; i < 3; i++ {
		defer fmt.Println("defer one :", -i)
		fmt.Println("one :", i)
	}
}

func twoDefer() {
	for i := 0; i < 3; i++ {
		defer fmt.Println("defer two :", -i)
		fmt.Println("two :", i)
	}
}

```

#### panic函数

`panic()` 可以手动使程序崩溃，会在所有的 `defer` 执行完之后执行。

在发生未预料到的严重错误时，系统通常会对 `panic()` 函数进行调用以终止程序的执行。

```go
func main() {
    higtlow(2, 0)
    fmt.Println("proce is end")
}

func higtlow( higt int, low int) {
    if higt < low {
        panic("low greater than higt")
    }
    defer fmt.Println("defer : (", higt, ",", low, ")")
    fmt.Println("return : (", higt, ",", low, ")")
    
    higtlow(higt, low + 1)
}

// 执行顺序：
// return : (2,0)
// return : (2,1)
// return : (2,2)
// defer : (2,2)
// defer : (2,1)
// defer : (2,0)
// low greater than higt
// ... (一长串的异常信息)

// 程序由于 panic() 而终止，所以不会再执行 `fmt.Println("proce is end")` 语句
```

#### recover函数

`recover` 函数只能配合 `defer` 使用，当程序正常运行时会返回 `nil` ，而当程序发生错误时（也可以理解为当`panic` 函数执行时）则会捕获其异常信息。

`recover` 函数<strong>并不能</strong>阻止程序的错误，但可以在错误发生的前一步做出应对，以避免出现更多的问题

```go
func main() {
    defer func() {
        handle := recover()
        if handle != nil {
            fmt.Println(handle)
        }
    }()
    
    higtlow(2, 0)
    fmt.Println("is success")
}

func higtlow(higt int, low int) {
    if higt < low {
        panic("low greater than higt")
    }
    defer fmt.Println("defer : (", higt, ",", low, ")")
    fmt.Println("return : (", higt, ",", low, ")")
    higtlow(higt, low + 1)
}

// 执行顺序：
// return : (2,0)
// return : (2,1)
// return : (2,2)
// defer : (2,2)
// defer : (2,1)
// defer : (2,0)
// low greater than higt

// panic函数的错误在 recover() 中被捕获，所以不会再抛出一长串的错误信息
```

### 反射

> 在程序运行期间对程序本身进行访问修改的一个操作。

`reflect` 包。

#### 使用场景

* 使用类型 `interface{}` 时不知道传递的是什么类型
* 序列化、反序列化：将文本类数据二进制数据之类的东西转换成go语言能用的结构
* `orm` 读取的数据库的数据加载成 `golang` 能操作的数据
* 配置文件解析，例如将 `yaml` 文件读取并转换为对应的结构体之类的

#### 方法详解

* `typeOf()` ：获取到对应属性的类型

  ```go
  package main
  
  import (
  	"fmt"
  	"reflect"
  	"testing"
  )
  
  type Cat struct{}
  type Dog struct{}
  
  func getType(d interface{}) {
  	v := reflect.TypeOf(d)
  
  	fmt.Printf("type: %s\n", v)
  }
  
  func TestFunc(t *testing.T) {
      // 基础数据类型
  	var numb uint = 450
  	getType(numb) // type: uint
  
  	var str string = "demo"
  	getType(str) // type: string
  
  	var isTrue bool = false
  	getType(isTrue) // type: bool
  
      // 复杂数据类型
  	var c Cat
  	getType(c) // type: main.Cat
  
  	var d Dog
  	getType(d) // type: main.Dog
  }
  
  ```

* `valueOf()` ：获取到对应属性的值

## 并发

`Golang` 的并发分为两种：

* `Goroutines` 自身所特有的协程，是在轻量线程中的并发活动，而不是在操作系统中进行的传统活动
* 通过线程实现的传统的并发方式

### 协程

一个重要的关键字：`go` ，`main` 函数其实就是一个 `Goroutine`。要想创建其他的协程活动则需要在函数前加上该关键字

### 协程通信

> 不是通过共享内存通信，而是通过通信共享内存。

可以理解为 `Channel` 成为了内存的代理人，由它管理并发活动的数据。协程活动只需要访问它

#### 无缓冲

例子：

```go
package main

func main() {
    // 声明一个通道，用于传递int类型的数据
    ch := make(chan int)
    
    apis := []string{
        "https://management.azure.com",
        "https://dev.azure.com",
        "https://api.github.com",
        "https://outlook.office.com/",
        "https://api.somewhereintheinternet.com/",
        "https://graph.microsoft.com",
    }
    
    for _,api range apis {
        go checkApi(api, ch)
    }
    
    fmt.Print(<-ch) // 正常执行
    fmt.Print(<-ch) // 正常执行
    fmt.Print(<-ch) // 正常执行
    fmt.Print(<-ch) // 正常执行
    fmt.Print(<-ch) // 正常执行
    fmt.Print(<-ch) // 正常执行

    fmt.Print(<-ch) // 系统会报错，因为阻止操作这个特性，程序会再次一直等待数据的读取（只有读到数据了才会返回），直到报错
}

func checkApi(api string, ch chan string) {
    _, err := http.Get(api)
    if err != nil {
        ch <- err.Error()
    	return
    }
    ch <- fmt.Sprintf("success: %s \n", api)
}
```

* 发送消息：`ch <- 20` 给管道中的数据设置为20

* 接收消息：

  `<- ch`:读取管道中的数据

  `x = <- ch`:读取数据并用 `x` 变量接收

* 关闭管道：`close(ch)` 

发送和接收消息均属于阻止操作（阻止程序运行）。

`make` 创建的通道默认是 `无缓冲` 的。

#### 有缓冲

