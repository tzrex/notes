## 安装

> Go 中文管网：https://studygolang.com/ 下载安装包

> Go 包文档地址：https://studygolang.com/pkgdoc

### 环境搭建

> `GOPATH` 是一个环境变量，声明 Go 的工作区。用于存放书写的Go语言代码

```bash
# 任意目录，在目录下创建 `bin`、`pkg`、`src` 三个文件夹
# bin 存放Go的可执行文件
# pkg 存放Go的引用包
# src 存放自己的Go代码

```

<strong>三个目录在你设置的 `GOPATH` 路径中添加，并且再添加一个可执行文件的路径（bin）目录</strong>。

#### Windows

<strong>在环境变量中添加变量 `GOPATH` ，与 `path` 同级</strong>。

在电脑的环境变量中，添加你想存放Go代码的路径。

```bash
# 可在 cmd 窗口中输入命令查看是否配置成功
go env
```

#### Linux

<strong>在 `Linux` 环境下，需要在系统配置文件中进行添加</strong>。

```bash
# 打开 ~/.bashrc 文件
vim ~/.bashrc

# 在打开的文件的末尾添加路径语句
# GOROOT 为Go的安装路径
export GOROOT=/usr/local/go

# GOPATH 为Go的工作路径，些Go代码的地方
export GOPATH=$HOME/go

# 导出工作区书写的指令集
export PATH=$PATH:$GOROOT/bin:$GOPATH/bin

# 重新加载
source ~/.bashrc
```

<strong>添加之后可通过指令查看是否配置成功</strong>。

```bash
# 查看版本号
go version

# 查看Go配置
go env
```

### 配置 `Go` 包代理

> Go 语言默认的是国外的网站 https://goproxy.io	国内访问会非常慢，甚至访问失败

```bash
# 修改为国内包代理路径
go env -w GOPROXY=https://goproxy.cn,direct
```

<strong>为之后的开发时包下载提供良好的下载体验</strong>。

### 下载基础包

在 `vs code` 编译器中：

```bash
# 按下 ctrl + shift + P 打开 vscode 的终端窗口
# 在出现的窗口中输入 go install
# 选择 `go:install/update tools` 。
# 将出现的 10 项全选并确认，之后等待下载。
```

## Hello World

<strong>在你配置的 `GOPATH` 路径下的 `src` 路径下创建你的项目</strong>。

```bash
# 此处举例子：创建一个名为 Demo 的项目
# 路径 D:/Go/src/gitee.com/Demo
```

<strong>创建一个名为 `main` 的Go文件</strong>。

```go
// main.go 时程序的入口文件。和 java的main.java；JS的main.js 同理
// 文件保存位置：D:/Go/src/gitee.com/Demo/main.go

// Go语言不需要像 java 一样，必须在句末加 “；” 号，语言会在编译时自动添加
package main

// 引用 Go 的基础包，参考 NodeJs 的包导入。同理
import "fmt"

// 文件的入口函数
func main(){
    fmt.PrintIn("hello world")
}
```

### 编译

```bash
# 使用 Go 自带的指令集进行构建
# 使用说明： go build <打包的文件路径>

# D:/Go/src/gitee.com/Demo
go build main.go

# 其他位置想构建 main 时，需要输入 src 之后的路径名
go build gitee.com/Demo/main.go
```

<strong>打包的 .exe 文件的存放位置：在哪 build 就存放在哪</strong>。

#### 跨平台编译

<strong>跨平台编译同样使用 build 指令</strong>。

```shell
# Go 默认时编译当前环境的运行包
# 要跨平台编译时，需要设置一些环境参数。

# 在此之前需要禁用 CGO
SET CGO_ENABLED=0

# 设置 GOOS 参数
# linux 表示编译linux环境的运行包
# windows 表示编译微软环境的运行包
# darwin 表示编译苹果环境的运行包
SET GOOS=linux
SET GOARCH=amd64
# SET 是 windows 的修改指令，不同系统的修改指令不同
```

### 执行

```bash
# 在窗口执行你打包好的 .exe 文件
main.exe
# 终端打印出：hello world

# 也有缩写的指令 run
go run main.go
# 这个指令相当于先执行了 build 操作，再运行了 main.exe
# 但不会保留 main.exe 运行包

# 此指令不仅相当于时在 bin 目录下进行的编译操作
go install main.go
# 执行之后会在你的 GOPATH/bin 目录下
```

### 注意事项

```go
// 我们先书写一个示例文件
package main

import "fmt"

func main(){
    fmt.PrintIn("hello world")
}
```

1. 所有的 .go 文件在书写前，都需要有一个 `package` 的声明。如果你 声明的 `package` 后面的包名为 `main` 时，它就可以打包为一个可直接执行的可执行文件。其他时候都是工具包。
2. `main` 函数是项目执行时的入口函数，是必须要有的。
3. 函数外只能声明：变量、常量、数据类型等。单独的语句必须在函数内执行。

## 关键字

> Go 语言暂且此 25 个关键词

```shell
break	default		func	interface	select
case	defer		go		map			struct
chan	else		goto	package		switch
const	fallthrough			if		range	var
type	continue	for		import		return

```

### 循环

```go
package main

import "fmt"

func main() {
	var list = [7]string{
		"A", "B", "C", "D", "E", "F", "G",
	}

    // 语句基础格式
    // for {start} {condition}{end}
	for i := 0; i < len(list); i++ {
		fmt.Println("index is:", i) // 0...6
		fmt.Println("items is:", list[i]) // A...G
	}
    
    // for 语句变种格式：将初识语句和结束语句进行拆分
    index := 0
    for index < len(list); {
		fmt.Println("index:", index) // 0...6
		fmt.Println("items:", item) // A...G
        index++
	}
}

```

**操作循环**：

`break`：跳出循环，直接结束循环。

```go
package main

import "fmt"

func main(){
    for i:= 0; i < 5; i++ {
        if(i === 3){
            break
        }
		fmt.Println(i) // 0 1 2
    }
}
```

`continue`：跳过当前循环，只结束当次循环。

```go
package main

import "fmt"

func main(){
    for i:= 0; i < 5; i++ {
        if(i === 3){
            continue
        }
		fmt.Println(i) // 0 1 2 4
    }
}
```

#### for range

> 可对 数组 切片 字符串 map 通道 进行遍历。

遍历返回值有以下规律：

1. 数组、切片、字符串返回索引和值
2. map 返回键和值
3. 通道（channel）只返回通道内的值

```go
package main

import "fmt"

func main() {
	var list = [7]string{
		"A", "B", "C", "D", "E", "F", "G",
	}
    
	for index, item := range list {
		fmt.Println("index:", index) // 0...6
		fmt.Println("items:", item) // A...G
	}
}
```

### 流程控制

#### switch case

> 当需要对每次循环的特定值做操作时可使用

```go
package main

import "fmt"

func main() {
    for i:= 0; i < 12; i++ {
        switch i {
        // 当 i 的值属于 1-5 区间时
        case 1, 2, 3, 4, 5:
            fmt.Println("min", i)
        // 当 i 的值属于 6 区间时
        case 6:
            fmt.Println("middle", i)
        case 7, 8, 9, 10, 11, 12:
        // 当 i 的值属于 7-12 区间时
            fmt.Println("max", i)
        default:
            fmt.Println("over")
        }
    }
}

```

#### * switch fallthrough

> 可以执行满足条件的下一个 case，设计目的是为了兼容 C 语言中的 case 语句。

```go
package main

func main(){
    var s = "a"
    
    // 会执行 case a 和 case b 两部分的内容
    // break 和 fallthrough 不能同时出现
    switch s {
	case "a":
		fmt.Println("switch a")
		fallthrough
	case "b":
		fmt.Println("switch b")
		break
	case "c":
		fmt.Println("switch c")
		break
	default:
		fmt.Println("switch default")
		break
	}
}
```



#### if else

```go
package main

import "fmt"

func main(){
    var boolean bool
    
    if(boolean){
        fmt.Println("success")
    }else{
        fmt.Println("error")
    }
}
```

#### * goto

> 跳转，跳转到某标签。不推荐试用

```go
package main

import "fmt"

func main() {

	for i := 0; i < 10; i++ {
		if i == 3 {
			goto LABEL // 会直接跳出for循环开始执行标签内的操作
		}
		fmt.Println("i", i) // 0 1 2
	}

    // 标签格式		key:{Scope}
LABEL:
	fmt.Println("this is label")
}
```

### map

> 用于声明集合（map）类型的关键字。注意是引用类型

**基本语法**：`map[key]value`

```go
package main

import "fmt"

func main(){
	var person map[string]int

	person["Rex"] = 22
	person["Flove"] = 21
	person["Xv"] = 23
	person["flex"] = 18
    
    // 会报错，因为map是引用类型
	fmt.Println("person", person)
    // panic: assignment to entry in nil map
}
```

#### 注意事项

由于 `map` 类型是引用类型。所以需要用 `make` 关键字进行内存分配

```go
package main

import "fmt"

func main() {
	var person map[string]int

    // 需要使用 make 函数进行内存分配
	person = make(map[string]int, 10)

	person["Rex"] = 22
	person["Flove"] = 21
	person["Xv"] = 23
	person["flex"] = 18

	fmt.Println("person", person)
    // person map[Flove:21 Rex:22 Xv:23 flex:18]
}

```

### new && make

`new` ：用于分配内存地址，返回对应类型的十六进制的指针地址本身。

`make` ：用于分配内存地址，通常用于分配引用类型，返回数据类型本身。

#### 区别

1. `new` 一般用来给基础数据类型申请内存，返回的是对应类型的指针。
2. `make` 用来给 `slice`、`map`、`channel` 分配内存，返回对应类型本身。

```go
import "fmt"

func main(){
    // 声明类型  初识长度  容量
    var section = make([]int, 5, 10)
}
```

## 协程

> golang 核心的东西，golang并发模型

在开发时从代码上看，似乎有父协程、子协程的感觉。

但从调用来看并没有，协程之间属于兄弟关系。硬要说的话可以比喻成 “ 分裂 ” 关系。

### 是什么

传统线程在切换时常常会大量消耗CPU的资源，为了更好的减少不必要的消耗，利用资源。

于是将CPU分为内核空间和用户空间，在用户空间定义好需要执行和处理的任务，之后将任务交给内核线程处理。

在用户空间定义的任务就是**协程**。

### MPG模型

* M（Machine） ：内核线程、宿主机CPU拥有的物理线程。
  * 例如：四核八线程，表示该机器的物理线程为 8，最大并发数为 8
* P （Processer）：调度器。协程与线程之间的切换运作之类的功能都是它的工作。
* G（goroutine） ：协程，`golang` 中的虚拟用户程序，`main` 就是一个主虚拟程序。

### 调度器

> MPG 模型中的，也是很关键的 P 。负责让内核线程执行哪个 `golang` 协程。

`golang` 的协程调度是 **非抢占式** 的，在协程阻塞时会由调度器分配给其他的用户线程。

所以协程和线程之间是多对多的关系。

### 管道

> `chan` 类型，用于多协程之间的信息共享与同步。

```go
package main

var ch = make(chan int)

func testOne() {
    
}

func main() {
    
}
```

### Context

## 数据类型

### 基本数据类型

#### 整型（int）

```bash
int		uint	uintptr
# int 		指带符号的整数
# uint 		不带符号的整数，包括零
# uintptr	无符号整型，用于存放一个指针
```

##### Go表示其他进制数

```go
package main

import "fmt"

func main(){
    // 声明的整型默认是 十进制 的
    var ten int = 10;
    
    // 赋值如果是零开头的会编译为八进制
    var night int = 08;
    
    // 以 0x 开头的会编译为十六进制
    var sixtheen = 0x16;
    
    /** 打印进制转换的正则 */
    // 打印十进制
    fmt.printf("%d\n")
    // 打印二进制
    fmt.printf("%b\n")
    // 打印八进制
    fmt.printf("%o\n")
    // 打印十六进制
    fmt.printf("%x\n")
    
    // 打印数据类型
    fmt.PrintIn("%T\n")
}
```

#### 浮点型（float）

```GO
// Go 语言中的小数默认为浮点型 float64
package main

func main(){
    // 浮点数类型默认为 float64
    var floatNum = 1.2345
    
    // 可以使用 float32 方法来声明 float32 的浮点数
    var floatMin = float32(1.5487)
}
```

#### 布尔型（bool）

> Go语言中，不允许将整型强制转换为布尔值

> 布尔值无法参与数值计算，也无法与其他类型进行转换

```go
package main

import "fmt"

func main(){
    var bol bool
    fmt.Printf("bol值", bol) // false
}
```

#### 字符串（string）

> Go 语言中，字符串是指用 `“” ` 包裹的内容，`‘’` 包裹的叫 字符

```go
/** 字符串转义符 */
// \r	返回行首（回车符）
// \n	换行符（跳转到下一行同列位置）
// \t	制表符
// \'	单引号
// \"	双引号
// \\	反斜杠

/** 多行字符串 */

// `` 定义多行字符串，也可以叫模板字符串
func main(){
    var texteraed = `
    	hello
    	world
    `
};
```

<strong>想要调用有关字符串的方法（切割、组合等），可以使用内置的 `strings` 包</strong>。

<strong>Go 当中的字符串的内核其实是两种：`byte` 和 `rune`</strong>。

byte：所有的字母和阿拉伯数字

rune：汉语、日语等不是由字母数字组成的内容

##### 字符串常用方法

|                 方法                 |       介绍       |
| :----------------------------------: | :--------------: |
|              `len(str)`              |  获取字符串长度  |
|            strings.Split             |    字符串分割    |
|           strings.contains           | 判断是否拥有字符 |
|          strings.hasPrefix           |  字符串前缀判断  |
|          strings.hasSuffix           |  字符串后缀判断  |
|            strings.Index             |   字符所在位置   |
| strings.Join(a[]string, sep strings) |    字符串合并    |

##### 修改字符串

<strong>Go 的字符串不能直接修改，需要进行转换才能进行修改</strong>。

```go
package main

import "fmt"

func main(){
    var nikname = "四川生"
    // 将 nikname 字符串转成 rune 切片
    var nameList = []rune(nikname)
    // 通过修改数组内的值进行修改，因为是修改字符，所以用单引号
    nameList[2] = '省'
}
```

### 复杂数据类型

#### 数组

> Go 语言中的数组是存放匀速的容器。<strong>必须指定存放的元素类型和长度</strong>.

```go
// 定义数组
// 定义一个长度为5，存放字符串类型的数组
var arr [5]string
var boolArr [3]bool

// 数组初始化
// 1. 第一种方式（固定的）
boolArr = [3]bool{true, false, true}
// 2. 第二种（根据内容数量自定义长度）
var arr = [...]int{1,2,3,4,5,6,7,8,41}
// 3. 只定义部分初始值
intArr := [5]int{2,4} // [2,4,0,0,0]

// 多维数组
// 定义一个 长度为2的int数组元素 的长度为3的数组
var manyArr [3][2]int{
    [2]int{2,3},
    [2]int{1,4},
    [2]int{7,9},
} // 

// 遍历数组
for i:=0;i<len(intArr); i++{
    fmt.PrintIn(intArr[i])
}
for i,v := range intArr {
    fmt.PrintIn(i,v)
}
```

##### 注意事项

1. 数组支持 `==` 、`!=` 的操作，因为数组始终有初始值
2. []*T 表示指针数组，\*[]T 表述数组指针。
3. 数组的赋值是拷贝，不是引用：

```go
var arr [3]int{1,2,4}
var list = arr
list[2] = 10
// 是直接将内存地址拷贝了一份
fmt.PrintIn(arr, list) // [1,2,4]  [1,2,10]
```

#### 切片（slice)

> 切片是一个可变长度的序列，是数组的再封装。

1. 切片指向一个底层的数组

   ```go
   // 切片和数组在写法上只有一点点差别：
   var arr [3]int // 数组
   var qie []int // 切片
   
   // 从数组中取出一段：
   var arr1 = [5]int{1,3,5,7,9}
   var qie1 = arr1[0:3] // 遵循左闭右开原则。值为：[1 3 5]
   ```

2. 切片的长度就是当前元素的个数，切片的容量是底层数组从切片的第一个元素到最后一个元素的数量

   ```go
   // 切片会对应一个底层数组
   var arr = [6]int{1,3,5,7,9, 10}
   var qie = arr[2:5] // [5 7 9]
   fmt.PrintIn(len(qie), cap(qie)) // 长度为3，容量为4
   ```

##### 数据追加

```go
func main(){
    var sect = []string{"北京", "上海", "广州"}
	fmt.Printf(len(sect), cap((sect))) // 3 3
    sect[3] = "深圳" // 会报错，超出长度了
    sect = append(sect, "深圳") // 正确的追加写法
	fmt.Printf(len(sect), cap((sect))) // 4 6
    
    // 利用 append 函数进行切片内容的删除
    sect.append(sect[:1], sect[2:]...)
    fmt.PrinTtIn("sect", sect) // [北京 广州]
}
```

##### 重要知识

1. 切片是**引用类型**，用于截取数组中的一段。当指向数组的数据集发生改变时，切片的值也会进行变化

2. 切片不能进行比较。唯一能用的就是 `nil` ，当 `qie == nil` 时说明切片并没有指向某个数组。若想知道切片是否为空，要用 `len(qie) == 0` 进行判断。

3. 切片的修改是直接修改的指向的数组的值。

   ```go
   // 举例说明：
   func main(){
       arr := [...]int{1,2,4,5}
       sect1 := arr[:3]
       sect2 := arr[1:3]
       sect[1] = 9
       // 原数组的值以及相关切片的值发生了变化
       fmt.PrintIn(arr, sect1, sect2) // [1 9 4 5] [1 9 4] [9 4]
   }
   ```

4. 切片的容量追加原则：如果切片的长度小于1024时，新容量是就容量的2倍；而切片的长度大于1024时，新容量为旧容量的125%；如果容量计算溢出，则新容量等于旧容量。

5. 切片在使用 `append` 模拟删除时，底层的数组会发生数据的位移，即：

   `arr[i] = arr[i+1]; arr[len] = arr[len] `

#### 指针

> Go 语言中，不涉及指针的运算。只读不改

```go
func main(){
    // &	获取当前数据保存的地址
    str := "this"
    local := &str
    fmt.PrintIn("指向地址", local)
    fmt.PrintIn("%t\n", local) // *string
    
    // *	根据地址取值
    m := *local
    fmt.PrintIn("m data is", m) // "this"
}
```



#### 集合（map）

> 是 Go 中提供的映射关系容器，其内部是通过散列表（hash）实现的。

<strong>是一种无序的基于 `key-value` 的数据结构，因为是引用类型，所以必须初始化才能使用</strong>。

对某一类数据进行归类

```go
func main(){
    // 用法 map[key]value		需要初始化才能使用
    var m1 = make(map[string]int, 10)
    m1["Rex"] = 18
    m1["Flover"] = 21
    m1["Nana"] = 28
}
```

##### 操作

```go
package main

import "fmt"

func main(){
    // 判断某个值是否存在
    var value, ok = m1["Nana"]
    fmt.Println("value", value) // 28
    fmt.Println("ok", ok) // true
    
    // 遍历
    for key, value := range m1 {
        fmt.Println("m1 item", key, value)
    }
    
    // 删除	delete(map, mapKey)
    delete(m1, "Nana") // 删除 m1 中的 Nana 项
    
    // 排序。map本身没有排序这个概念，想排序只能借用切片进行排序再返回
    
}
```

#### 结构体

#### 函数

> 函数就是一段代码的封装，将一段逻辑抽象出来封装到函数中。使代码更加清晰简洁

<strong>Go 中函数使用 `func` 关键字声明</strong>。

```go
package main

// 没有参数和返回值的有名函数
func fn1(){
    fmt.PrintIn("hello world")
}

// 有参数无返回值的有名函数
// 每个参数需要指定类型
func fn2(x int, y int){
    fmt.PrintIn("x", x, "y", y)
}

// 有参数和返回值的有名函数
// 第一种：在参数后面声明返回变量
func fn3(x int, y int)(ret int){
    ret = x+y
    return // 默认返回定义的 ret
}
// 第二种：直接在函数中返回
func fn4(x int,y int){
    return x+y
}
// 第三种：多个返回值（逗号分割）
func fn5(x int,y int){
    return x, y
}

func main(){
    fn1()
    fn2(1, 3)
    fn3(1, 2)
    fn4(2, 4)
    fn5(5, 4)
}
```

##### defer

> 延迟语句：在函数即将结束或即将进行结果处理时（return）执行

```go
package main

import "fmt"

func main() {
	fmt.Println("A")
	fmt.Println("B")
	defer fmt.Println("C")
	fmt.Println("D")
	defer fmt.Println("E")
	fmt.Println("F")
	fmt.Println("G")
	fmt.Println("H")
	defer fmt.Println("I")
}

// A B D F G H I E C
// defer 属于栈结构，先进后出
```

##### defer和return

**`Golang` 中的 `return` 分为两步**。

1. 返回值赋值
2. 进行返回操作

```go
// 会先创建一个匿名的变量用于接收字符串
func Demo() string {
    return "hello"
}
// 显式的声明返回变量，随时进行返回
func Demo() (str string) {
    str = "hello"
    return
}
```

**defer 的执行会在return之前**，它是一个延迟执行的标志。

```go
package main

import "fmt"

func f1() int {
	x := 5
	defer func() {
		x++ // 修改的是 x 变量，而不是返回值
	}()
	return x // 返回值赋值 = 5
}

func f2() (x int) {
	defer func() {
		x++ // 修改的是返回变量，因为函数体中已经定义了返回变量
	}()
	return 5 // 返回值赋值
}

func f3() (y int) {
	x := 5
	defer func() {
		x++ // 修改的x变量
	}()
	return x // 将 x 的值 5 赋值给返回值 y
}

func f4() (x int) {
	defer func(x int) { // 在函数内部声明了一个变量x
		x++ // 修改的是函数内部的变量
	}(x)
	return 5 // 返回值赋值
}

func main() {
	fmt.Println("f1", f1()) // 5
	fmt.Println("f2", f2()) // 6
	fmt.Println("f3", f3()) // 5
	fmt.Println("f4", f4()) // 5
}
```

## 常用运算符

### 算术运算符

> `++` 和 `--` 在 Go 中是单独的语句，并不是运算符。不能放在 `=` 右边

| 运算符号 |  描述  |
| :------: | :----: |
|    +     |  相加  |
|    -     |  相减  |
|    *     |  相乘  |
|    /     | 相除以 |
|    %     |  求余  |

**例子**：

```go
package main

import "fmt"

func main(){
    var one = 1
    var two = 2
    var three = 3
    var four = 4
    var five = 10
    
    fmt.Println("相加：",one + two) // 3
    fmt.Println("相减：",three - one) // 2
    fmt.Println("相乘：",two * three) // 6
    fmt.Println("相除：", four / two) // 2
    fmt.Println("求余数：",three % two) // 1
}
```

### 关系运算符

> 用于比较两数是否相等，通过对比结果返回 `bool` 类型的 `True` 和 `False`。

| 运算符号 |            描述            |
| :------: | :------------------------: |
|    ==    |      判断两值是否相等      |
|    !=    |      判断两值是否不等      |
|    >     |    判断某值是否大于某值    |
|    >=    | 判断某值是否大于且等于某值 |
|    <     |    判断某值是否小于某值    |
|    <=    |  判断某值是否小于等于某值  |

### 逻辑运算符

| 运算符号 |                描述                 |
| :------: | :---------------------------------: |
|    &&    |          `AND` 逻辑运算。           |
|   \|\|   |           `OR` 逻辑运算。           |
|    !     | `NOT` 逻辑运算，也可以叫 `取反操作` |

### 位运算符

> 对整数（整型数据类型）在内存中的**二进制**位进行操作。

| 运算符号 |               描述               |
| :------: | :------------------------------: |
|    &     |              按位与              |
|    \|    |              按位或              |
|    ^     |             按位异或             |
|    <<    |  左移操作 2^(x+n)，二进制中的乘  |
|    >>    | 右移操作 2^(x-n)，二进制中的除以 |

**示例**：

```go
package main

import "fmt"

func main(){
    var ten = 5
    var two = 2
    
    // 对应位数都是 “1” 才在对应位置显示 “1”
    fmt.Println("按位与", ten & two)
    
    // 对应位数有 “1” 就在对应位置显示 “1”
    fmt.Println("按位或", ten | two)
    
    // 对应位数不一样就在对应位置显示 “1”
    // ^ 在Go 当中不是平方
    fmt.Println("按位异或", ten ^ two)
}
```



### 赋值运算符

| 符号 |               作用               |
| :--: | :------------------------------: |
|  =   | 赋值：右边的数据赋予到左边的变量 |
|  +=  |             相加赋值             |
|  -=  |            相减后赋值            |
|  *=  |            相乘后赋值            |
|  /=  |            相除后赋值            |
|  %=  |            求余后赋值            |
| <<=  |           左位移后赋值           |
| >>=  |           右位移后赋值           |
|  &=  |           按位与后赋值           |
| \|=  |           按位或后赋值           |
|  ^=  |          按位异或后赋值          |

