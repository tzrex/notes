## 前言

**学习的就是 `SQL` 语句，只是在实际使用时会用到 `MySQL` 数据库，所以会根据里面的一些不支持项进行提出**。

### 概念

#### SQL

结构化查询语言。

##### 标准

> 有两个重要的标准：`sql92` 和 `sql99`，现如今使用的依然沿用这些标准。

之后也有不断更新迭代不断完善。不过都是增量更新。

`sql92`：也叫 `sql-2` 标准，书写形式较之更简单。但语句较长，可读性较差。

`sql99`：也叫 `sql-3` 标准。语法更加复杂，但可读性强。

##### 分类

* `DDL`：数据库定义语言（数据库的定义）

  `CREATE`、`ALTER`、`DROP`、`RENAME`、`TRUNCATE` 等操作。

* `DML`：表的操作语言（表数据的增删改查）

  `INSERT`、`DELETE`、`UPDATE`、`SELECT` 等操作。

* `DCL`：数据库控制语言。控制 `DML` 的

  `COMMIT`、`ROLLBACK`、`SAVEPONT`、`GRANT`、`REVOKE` 等操作。

#### 关系型数据库

* 支持 `SQL` 语句。
* `orcale`、`Mysql`、`MsSQL`、`SQLserver` 等都是关系型数据库的产品。

#### 非关系型数据库

* 不支持 `SQL` 语句。
* `mongodb`、`redis` 等都是非关系型数据库产品。

### 安装

#### Windows

下载地址：`https://dev.mysql.com/downloads/mysql/`.

一路默认即可。`mysql8` 默认使用 `ust8mb4` 格式，不需要再修改格式。

```shell
# 查看当前数据库版本号
mysql -V
```

#### Linux

##### 普通安装

##### Docker安装

> 使用前需要下载 Docker.具体使用可参考官方使用文档或我的笔记

使用 `dockerhub` 的镜像资源进行下载：`docker pull mysql`.

或指定版本 `docker pull mysql:8.0.28`.

**记得将数据 `vloumns` 到本地，否则容器删除时数据库数据也就跟着消失了**！

##### 宝塔安装

> 宝塔运维工具是由国人开发的工具。
>
> 安装教程：`https://www.bt.cn/new/download.html` 查看安装脚本

安装——启动——打开图形化控制面板——软件商城——下载。

之后在选择 `数据库` 菜单进行操作即可。

### 规则与规范

#### 基本规则

* `SQL` 各子句分行写，必要时可换行缩进。
* 每条语句以 `;` 结尾。
* 列的别名尽量使用 `""` 符号。
* 数据库名、表名的创建不能超过 30 字符
* 使用 `/\[A-Za-z0-9]$/` 字符。

#### 规范

* 推荐数据库名、表名、字段名、字段别名等小写。
* 关键字、函数名、变量推荐大写。

## DDL

> 定义数据库和数据库的表

### 导入数据库

#### 命令导入

> 需要在命令行中使用

```shell
source P:\student\base.sql;
```

#### 工具导入

> 指 `navicat`、`SQLyog` 等工具

执行想导入的数据库的 `SQL` 脚本即可。

### 数据库管理

#### 创建

> 首先用户需要拥有创建数据库的权限

```sql
# 方式一。最基本的创建方式，所有属性统统默认
CREATE DATABASE school;

# 方式二。在 一 的基础上自定义定义属性的值
# 创建时指定字符集
CREATE DATABASE school CHARACTER SET 'utf8';

# 方式三。在创建之前检测是否存在，如果存在则创建失败
CREATE DATABASE IF NOT EXISTS school;
```

#### 查看

```sql
# 查看数据库
SHOW DATABASES;

# 查看创建的数据库的结构
SHOW CREATE DATABASE school;

# 切换并使用数据库
USE school;

# 查看当前数据库
SELECT DATABASE() FROM DUAL;

# 查看数据库下表
SHOW TABLES;
SHOW TABLES FROM school;
```

#### 修改

```sql
# 修改数据库字符集
ALTER DATABASE school CHARACTER SET 'utf8mb4';

# 修改数据库名
# 没有此操作，创建一个新表并将就数据库数据导入后删除旧库。
```

#### 删除

```sql
# 删除数据库
DROP DATABASE school;

DROP DATABASE IF EXISTS school;
```

### 数据库表

#### 常用数据类型

|     类型     |                           类型举例                           |
| :----------: | :----------------------------------------------------------: |
|   整数类型   |          TINYINT、SAMLLINT、MEDIUMINT、INT、BIGINT           |
|   浮点类型   |                        FLOAT、DOUBLE                         |
|  定点数类型  |                           DECIMAL                            |
|    位类型    |                             BIT                              |
|   时间日期   |            YEAR、TIME、DATE、DATETIME、TIMESTAMP             |
|    字符串    |     CHAR、VARCHAR、TINYTEXT、TEXT、MEDIUMTEXT、LONGTEXT      |
|   枚举类型   |                             ENUM                             |
|   集合类型   |                             SET                              |
|   JSON类型   |                      JSON对象、JSON数组                      |
| 二进制字符串 |   BIGARY、VARBINARY、TINYBLOB、BLOB、MEDIUMBLOB、LONGBLOB    |
|   空间数据   | 单值：GEOMETRY、POINT、LINESTRING、POLYGON<br />集合：MULTIPOINT、MULTILINESTRING、MULTIPOLYGON、GEOMETRYCOLLECTION |

#### 表创建

```sql
# 普通创建
CREATE TABLE IF NOT EXISTS student (
	`id` BIGINT,
    `name` VARCHAR(30),
    `age` TINYINT,
    `math` TINYINT,
    `english` TINYINT,
    `chinese` TINYINT,
    `classId` BIGINT
);

# 继承现有的
# 会继承 student 表的结构以及查询的结果导入数据
CREATE TABLE IF NOT EXISTS copyStudent
AS
SELECT * FROM student;

# 继承其他数据库的表
CREATE TABLE IF NOT EXISTS copyStudent
AS
SELECT * FROM old.student;
```

#### 表修改

```sql
# 方式一：
RENAME TABLE class TO classes;

# 方式二：
ALTER TABLE class RENAME TO classes;
```

#### 表删除

> `DROP` 操作是真删除，无法复原。

```sql
DROP TABLE student;
# 或者加上判断
DROP TABLE IF EXISTS student;
```

#### 清空表数据

```sql
# 保留表的字结构，但各列的数据没有了
# 该操作无法使用 rollback 进行回滚，delete 操作可以回滚
TRUNCATE TABLE student;
```

#### 表字段操作

```sql
# 表增加字段
ALTER TABLE student ADD physics TINYINT;
ALTER TABLE student ADD description VARCHAR(255);

# 指定添加的位置
# frist: 添加到表的最前面；after {key} 添加到某字符串的后面。
ALTER TABLE student ADD phone VARCHAR(11) FIRST;
ALTER TABLE student ADD phone VARCHAR(11) AFTER physics;

# 字段修改类型
# 修改 description 字段的类型并添加默认值
ALTER TABLE student MODIFY description VARCHAR(200) DEFAULT "暂无";

# 字段重命名
ALTER TABLE student CHANGE description `desc` VARCHAR(200);

# 字段删除
# 删除 phone 字段
ALTER TABLE student DROP COLUMN phone;
```

##### 计算列

> `MySQL8` 的新特性。

```sql
# numbs 就是计算列。当 numbOne 或者 numbTwo 变化时
# numbs 会根据声明的关系进行变化
CREATE TABLE test_table(
	id BIGINT,
    numbOne int,
    numbTwo int,
    numbs int GENERRATED ALWAYS AS (numbOne + numbTwo) VIRTUAL
);
```

##### 自增列

> 自动增加：num++

**前置要求**：

* 一个表中最多只能有一个自增列。
* 需要在唯一约束列或者主键列上。
* 只能是定点数类型。

```sql
CREATE TABLE test_table(
	id BIGINT PRIMARY KEY AUTO_INCREMENT;
);
# 或者
ALTER TABLE test_table MODIFY id BIGINT AUTO_INCREMENT;
```

### 约束

> 对表字段的区分

#### 添加约束的情况

* 实体完整性：例如同一个表中，不能存在两条完全相同的记录
* 区域完整性：例如性别（0-120）年龄（男 / 女）等。
* 引用完整性：例如，外键。要保证联表时不会出现差错
* 用户自定义：例如：密码不能为空，用户账号唯一等。

#### 查看约束

```sql
# 直接查看记录各表约束的表
SELECT * FROM information_schema.table_constraints
WHERE table_name = {表名}
```

#### 添加约束

```sql
# 表创建时添加：列级约束
CREATE TABLE test(
    -- not null 只能是列级约束
	id BIGINT NOT NULL
);

# 表创建时添加：表级约束
# 给 id 字段添加 unique 约束并添加别名 uni_test_id
CREATE TABLE test(
	id BIGINT,
    CONSTRAINT uni_test_id UNIQUE(id)
);

# 后期给表添加
# 修改字段的参数
# 如果数据中有该列有不符合太修改后的约束，则修改失败
ALTER TABLE test MODIFY id BIGINT NOT NULL;

#后期添加2
ALTER TABLE test ADD CONSTRAINT uni_test_id UNIQUE(id);
```

#### 删除约束

```sql
# 删除唯一索引
# 唯一性约束添加时会自动创建唯一索引
# 删除唯一约束只能通过唯一索引进行删除
ALTER TABLE student DROP INDEX {索引名};

# 删除主键约束
ALTER TABLE student DROP PRIMARY KEY;

# 删除非空约束
# 直接修改即可
ALTER TABLE student MODIFY name VARCHAR(15);
```



#### 各约束

```sql
# 非空约束		not null
# 表现：设置该约束的字段不能为空
name VARCHAR(15) NOT NULL;
ALTER TABLE student MODIFY name VARCHAR(15) NOT NULL;

# 唯一约束		unique
# 表现：设置该约束的字段不能重复，可以为空（两个null不算重复，因为null之间不能比较）
email VARCHAR(10) UNIQUE;
ALTER TABLE student ADD UNIQUE(email);

# 主键约束		primary key
# 表现：设置该约束的字段不能为空，且不能重复
# 一个表中只能有一个主键约束，且必须有
# 不推荐删除主键，会影响数据的完整性和操作的性能
id BIGINT PRIMARY KEY;

# 外键约束		foreign
# 引用完整性。实际工作中不推荐使用，后期维护很麻烦
classId BIGINT FOREIGN KEY;

# 检查约束		check
# 检查某个字段是否符号要求（mysql5.7不支持）
# 限制 gender 只能输入 男 或者 女 两个值
gender CHAR check('男' OR '女');

# 默认值		default
createTime DATETIME DEFAULT CURDATE();

# 组合约束
# 两个字段不能同时相等
CONSTRAINT uni_test_user UNIQUE(name, pass);
```

## 表数据操作

> 对表数据进行增删改查

### 注释

1. 单行注释

   ```sql
   -- 双减号注释
   # 井号注释
   ```

2. 多行注释

   ```sql
   /*
   多行
   注释
   */
   ```



### 运算符

#### 算术运算符

都是跟数学里的一样。先乘除后加减，优先运行括号内的

```sql
# +			加
# -			减
# *			乘
# /			除（符号）
# div		除（关键字）
# %			取余（符号）
# mod		取余（关键字）
```

#### 比较运算符

```sql
# >				大于
# >=			大于等于
# <				小于
# <=			小于等于
# =				等于
# <=>			完美等于，用于处理NULL参与的筛选
# != 或者 <>		不等于
```

##### 运算符关键字

|    运算符    |    名称    |                       作用                       |
| :----------: | :--------: | :----------------------------------------------: |
|  `IS NULL`   |  为空运算  |        判断：值、字符串、表达式 是否为空         |
| `ISNOTNULL`  |   不为空   |   判断：值、字符串、表达式 是否不为空（有值）    |
|   `LEAST`    |   最小值   |               返回多个值中最小的值               |
|  `GREATEST`  |   最大值   |               返回多个值中最大的值               |
| `BETWEENAND` | 两值的区间 | 是否在两值之间（`[x,y]`），配合 `AND` 关键字使用 |
|     `IN`     |    属于    |           判断值是否属于列表中的某个值           |
|   `NOT IN`   |   不属于   |          判断值是否不属于列表中的某个值          |
|    `LIKE`    |  模糊匹配  | 判断值是否属于列表中的某个值，且符合某种模糊规则 |
|   `REGEXP`   |    正则    |             判断值符合某种正则表达式             |
|   `RLIKE`    |    正则    |             判断值符合某种正则表达式             |

#### 逻辑运算符

```sql
# and		并且（关键字）
# &&		并且（符号）
# or		或者（关键字）
# ||		或者（符号）
# not		不，也可以理解为取反
```

**`OR` 可以和 `AND` 一起操作，但 `AND` 的优先级高于 `OR` **。

### DML

> 数据库表数据的增删改，原本有查的操作。这里进行了拆分

#### 添加数据

```sql
# 手动添加数据
# 默认按照表内字段位置传入值
INSERT INTO student VALUES
(1, '张三', '男', '暂无', 91, 49, NULL),
(2, '李四', '男', '暂无', 54, 64, NULL),
(3, 'Rex', '女', '四川成都', 75, 66, NULL);

# 在表后面声明想添加的字段
INSERT INTO student(id, name, sex, address, math, english, classId) VALUES ...;

# 将查询到的数据添加到新表
# 此种情况需要保证对应的字段数据类型一致
# 当长度不一样时：短存长不影响，但长存短时若是数据装不下时会发生错误
INSERT INTO student(id, name, sex, address, math, english, classId)
SELECT id, name, sex, address, math, english, classId
FROM student_copy
WHERE classId IS NOT NULL;
```

#### 修改数据

> 简单理解：用 `UPDATE...SET...` 替换 `SELECT...FROM...` 语句就是从查询变成修改。

```sql
# 修改数据
# 如果不用 where 删选数据，则默认修改所有。
UPDATE student SET address = '四川都江堰' WHERE name = '李四';

# 修改多个字段的值
UPDATE student SET
address = '广东深圳',
sex = '女'
WHERE id=3;
```

#### 删除数据

> 简单理解：用 `DELETE` 替换 `SELECT...` 语句就是从查询变成修改。

```sql
# 删除
# 不需要像 select 一样设置字段
DELETE FROM student
WHERE id=7;
```

### SELECT

> 查询操作，属于 `DML` 的一种，也有一种将查询拆分出来的说法 `DQL`。查询表的数据

#### 表结构

```sql
# 显示表结构
DESCRIBE student;

# 或者使用缩写的关键字
DESC student;
```

#### 查询

##### 基础查询

```sql
# 查询所有字段
SELECT * FROM student;

# 查询指定字段
SELECT acount,name FROM student;

# 添加别名：as 关键字；或使用空格
SELECT acount as admin, name FROM student;
SELECT acount admin, name FROM student;

# 查询去重：DISTINCT 关键字
# 若是 DISTINCT 后面有多个字段，则进行组合去重
SELECT DISTINCT classId FROM student;

# NULL 参与运算：会都变成 null 值
SELECT numberAll (englishNum + mathNum) FROM student;

# 查询常数
SELECT "classThree", name, account FROM student;
```

##### 数据筛选

```sql
# 过滤数据：WHERE 关键字，该关键字并不是只能在 SELECT 中使用
# WHERE 一定要写在 FROM 后面
SELECT name,age,account FROM student WHERE sex="1";
SELECT name,age,account FROM student WHERE age>=18;

# 查询 NULL 列
SELECT * FROM student WHERE grad <=> NULL;
SELECT * FROM student WHERE grad IS NULL;

# 筛选除开 null 之外的值
SELECT * FROM student WHERE grad IS NOT NULL;
SELECT * FROM student WHERE NOT grad <=> NULL;

# 找出最小值
# 举例：对比数学和英语的得分，把得分少的科目显示出来 contrast 列
SELECT `name`, math, english, LEAST(math,english) AS 'contrast'
FROM student;

# 找出最小值
SELECT `name`, math, english, GREATEST(math,english) AS 'contrast'
FROM student;

# 查找某个区间内的值
# 筛选数学成绩在 60-80 分的同学，包括60和80.
SELECT * FROM student WHERE math BETWEEN 60 AND 80;
SELECT * FROM student WHERE math >= 60 AND math <=80;
SELECT * FROM student WHERE math >= 60 && math <=80;

# 查询集合中的某个值
# 例如：查询数学成绩等于60 70 80的同学
SELECT * FROM student WHERE math in (60,70,80);

# 模糊查询
# 例如：查询姓张的同学
/*
%张%：包含‘张’字符的
%张：以‘张’字符结尾的
张%：以‘张’字符开头的
*/
SELECT * FROM student WHERE `name` LIKE '张%';

# 查询指定位置值
# 指定第二为的值是 e 的数据， ‘_’ 表示占一个字符位。
SELECT * FROM student WHERE `name` LIKE '_e%';
```

##### 排序

```sql
# 从低到高排序（升序），默认就是低到高。
SELECT * FROM student ORDER BY math;
SELECT * FROM student ORDER BY math ASC;

# 从高到低排序（降序）
SELECT * FROM student ORDER BY math DESC;

# 可使用字段的别名进行排序
SELECT IFNULL(math,0) + IFNULL(english,0) AS number
FROM student
ORDER BY number;

# 二级排序：当排序的值相等时，再进行排序
SELECT * FROM student ORDER BY math ASC, english DESC;
```

##### 分页

`MySQL` 中使用 `LIMIT`；`oracle` 中使用 `TOP` 关键字。各数据库并不一样

```sql
# 分页公式：LIMIT (pageNo - 1) * pageSize, pageSize;
# 解释：从第一条开始，查20条（计算机计数是从零开始）
SELECT * FROM student LIMIT 0,20;
SELECT * FROM student LIMIT 20 OFFSET 0;

# 对分页内容排序
SELECT * FROM student
ORDER BY math DESC
LIMIT 0,10;
```

#### 多表查询

##### * 笛卡尔积

指两个集合相乘，交叉连接。在数据库中表示错误的引用了多张表导致的查询结果的相乘。

会出现笛卡尔积的操作：在数据库中是错误的。

```sql
# 第一种
SELECT `name`, sex, classId AS className FROM student,classes;

# 第二种
SELECT `name`, sex, classId AS className
FROM student CROSS JOIN classes;
```

正确操作：

```sql
# 只是示例，工作中不会使用
# 多个表中出现相同名称的字段时，必须指明使用哪个表的字段
SELECT `name`, sex, classId AS className
FROM student.`classId` = classes.`studentId`;
```

##### * 等值连接

```sql
# 以 = 关联的都是等值连接。
# 等值连接只是分类的一种
SELECT a.*, b.`name` AS className
FROM student AS a, class AS b
WHERE a.`classId` = b.`id`;
```

##### * 非等值连接

```sql
# 除 = 之外的大小于与或非都是非等值连接
# 排除 classId 等于 20 的值
SELECT a.`id`, a.`name`, b.`name` AS className
FROM student AS a, merits AS b
WHERE a.`classId` != 20;
```

##### * 自连接

> 自我关联，自己关联自己。

```sql
# 例如：公司的每个员工都有一个顶头上司，而上司也是该公司的员工
SELECT a.`id` AS aId, a.`name` AS aName, b.`id` AS bId, b.`name` AS bName FROM employ AS a, employ AS b
WHERE a.`managerId` = b.`id`;
```

##### * 非自连接

> 去调取其他表进行查询。

##### 内连接

> 合并具有同一列的多个表，返回的结果集不包含其他表不匹配的行

```sql
/*
下列语句的返回集中只是相较 student 表多了一个 className 字段反应 class 表的 name 属性，class表的其他字段并没有展示。
这种就是内连接的一种
*/
SELECT a.*, b.`name` AS className
FROM student AS a, class AS b
WHERE a.`classId` = b.`id`;

# 或者
SELECT a.*, b.`name` AS className
FROM student AS a INNER JOIN class AS b
ON a.`classId` = b.`id`;
```

##### 外连接

> 此处定位两个表进行关联查询，三个及以上建议拆分

左表：关联查询的前（第）一个表；右表：关联查询的后一个表

**左外连接**：

> 查询的结果除了匹配项之外，还有第一张表的其他不符合查询条件的数据

**`sql92` 的标准不是每个数据库产品都支持，`MySQL` 就不支持。而 `Oracle` 却支持**。

```sql
# 查询所有学生的对应班级以及未分班的
# sql92的外连接：在此查询基础上增加 + 
SELECT a.*, b.`name` AS className
FROM student AS a, class AS b
WHERE a.`classId` = b.`id`(+);

# sql99的外连接：使用 JOIN...ON 关键字
# 左外就是：left join 或者 left outer join
SELECT a.*, b.`name` AS className
FROM student AS a LEFT JOIN class AS b
ON a.`classId` = b.`id`;
```

**右外连接**：

> 查询的结果除了匹配项之外，还有后一张表的其他不符合查询条件的数据

```sql
# 查询所有班级，有学生的班级和没有学生的班级
# sql92的外连接：在此查询基础上增加 + 
SELECT a.*, b.`name` AS className
FROM student AS a, class AS b
WHERE a.`classId`(+) = b.`id`;

# sql99的外连接：使用 JOIN...ON 关键字
# 右外就是：right join 或者 right outer join
SELECT a.*, b.`name` AS className
FROM student AS a RIGHT JOIN class AS b
ON a.`classId` = b.`id`;
```

**满外连接**：

> 左表和右表的其他不符合条件的数据全都有。

```sql
# 查询所有学生和班级，并表示两表之间的关系。

# 满外就是：full join 或者 full outer join
# mysql 不支持 full 关键字
SELECT a.*, b.`name` AS className
FROM student AS a FULL JOIN class AS b
ON a.`classId` = b.`id`;

# 使用联合查询 UNION 关键字
# 思路就是查询两个表的所有数据并去重
```

##### 七种JOIN使用

![joins](./img/joins.png)

```sql
# 内连接，中间图
SELECT a.*, b.`name` AS className
FROM student AS a
JOIN class AS b
ON a.`classId` = b.`id`;

# 左外连接，左上图
SELECT a.*, b.`name` AS className
FROM student AS a
LEFT JOIN class AS b
ON a.`classId` = b.`id`;

# 右外连接，右上图
SELECT a.*, b.`name` AS className
FROM student AS a
RIGHT JOIN class AS b
ON a.`classId` = b.`id`;

# 左子集查询，中左图
SELECT a.*, b.`name` AS className
FROM student AS a
LEFT JOIN class AS b
ON a.`classId` = b.`id`
WHERE a.`classId` IS NULL;

# 右子集查询，中右图
SELECT a.*, b.`name` AS className
FROM student AS a
RIGHT JOIN class AS b
ON a.`classId` = b.`id`
WHERE b.`students` IS NULL;

# 满外连接，左下图
# 有些数据库产品不支持 full 关键字
SELECT a.*, b.`name` AS className
FROM student AS a
FULL JOIN class AS b
ON a.`classId` = b.`id`;
# 右外连接加上左表的真子集。也可以左外连接加上右表的真子集
# union 关键字的使用条件是需要连接的两句sql的查询返回字段相同的
SELECT a.`name`, a.`age`, b.`name` AS className
FROM student AS a
RIGHT JOIN class AS b
ON a.`classId` = b.`id`
UNION ALL
SELECT name, age, classId AS className FROM student
WHERE student.`classId` IS NULL;

# 真子集查询
# 左关联和有关联，筛选出不符合 on 的条件。
SELECT a.*, b.`name` AS className
FROM student AS a
LEFT JOIN class AS b
ON a.`classId` = b.`id`
WHERE a.`classId` IS NULL;
UNION ALL
SELECT a.*, b.`name` AS className
FROM student AS a
RIGHT JOIN class AS b
ON a.`classId` = b.`id`
WHERE b.`students` IS NULL;
```

#### 子查询

> 查询语句中嵌套了查询语句。从查询的结果中再进行比较

##### 独立子查询

子查询语句就是普通的sql语句，可以先单独写出来再 XV 到主查询的 `()` 内。

```sql
# 查询英语成绩比 Rex 高的
# 不使用 子查询 的方式：
SELECT `name`, english
FROM student AS a, student AS b
WHERE b.`english` > a.`english`
AND a.`name` = 'Rex';
# 或者
SELECT `name`, english
FROM student AS a
INNER JOIN student AS b
ON b.`english` > a.`english`
AND a.`name` = 'Rex';

# 使用 子查询 的方式：
# 子查询的语句通常放在 () 内
SELECT `name`, english
FROM student AS a
WHERE a.english > (
	SELECT english FROM student WHERE `name` = 'Rex'
)
```

##### 关联子查询

### GROUP BY

> 分组操作。

#### 注意事项

* `select` 中出现的非组函数的字段必须声明在 `group by` 中。
* `group by` 的书写位置：在 `from`、`where` 后面；`oder by`、`limit` 、`HAVING` 前面。

```sql
# 查询各班级学生的英语平均成绩
# avg 数据的平均数
SELECT classId AVG(english) AS average
FROM student
GROUP BY classId;

# 查询各班级男女学生的英语平均成绩
# 先按照班级分组，再按照性别分
SELECT classId, AVG(english) AS average
FROM student
GROUP BY classId,sex;
```

#### 分组过滤

> `having` 关键字必须声明在 `GROUP BY` 后面

```sql
# 查询各班级男学生的英语水平
SELECT classId, sex, AVG(english) AS average
FROM student
GROUP BY classId,sex
HAVING sex = '男';
```

## 函数

### 流程控制

#### IFNULL

```sql
# 如果参数一是null就以参数二来计算。可理解为 三目运算
SELECT IFNULL(math, 0) + english AS number FROM student;
```

#### IF

```sql
# 如果 value 值为true，返回 v1，否则返回 v2
SELECT IF(1>0,'正确','错误') FROM DUAL;
> 正确
```

### 数字函数

|        函数名称        |                     作用                     |
| :--------------------: | :------------------------------------------: |
|         ABS(x)         |               返回 x 的绝对值                |
|        SING(x)         |           返回 x 的符号（正负零）            |
|          PI()          |                    圆周率                    |
|  CEIL(x) , CEILING(x)  |       返回小于或等于 x 条件的最小整数        |
|        FLOOR(x)        |       返回小于或等于 x 条件的最大整数        |
|  LEAST(e1,e2,e3,....)  |              返回列表中的最小值              |
| GREATEST(e1,e2,e3,...) |              返回列表中的最大值              |
|        MOD(x,y)        |            返回 `x / y` 后的余数             |
|    RAND() , RAND(x)    |   返回 `0-1` 的随机数，带 x 时 x 为种子值    |
| ROUND(x) , ROUND(x,y)  | 返回最接近 x 的四舍五入的值，并保留 y 位小数 |
|     TRUNCATE(x,y)      |         返回 x 截断为 y 位小数的结果         |
|        SQRT(x)         |     返回 x 的平方根，`x<0` 时返回 `null`     |

### 字符串函数

|        函数名称         |                          作用                          |
| :---------------------: | :----------------------------------------------------: |
|        ASCII(s)         |                 返回字符串的 ASCII 码                  |
|     CHAR_LENGTH(s)      |                  返回字符串的字符数量                  |
|        LENGTH(s)        |                   返回字符串的字节数                   |
|  CONCAT(s1,s2,s3,....)  |                       连接字符串                       |
| INSERT(str,idx,len,rep) | 替换字符串：从 idx 位置开始 len 长度的字符串替换为 rep |
|    REPLACE(str,a,b)     |               字符串 b 替换 所有的 a 值                |
|        UPPER(s)         |                       转为全大写                       |
|        LOWER(s)         |                       转为全小写                       |
|       LEFT(str,n)       |                返回字符串左数 n 个字符                 |
|      RIGHT(str,n)       |                返回字符串右数 n 个字符                 |
|    LPAD(str,len,pad)    | 字符串 pad 最左边进行填充，知道 str 长度为 len 个字符  |
|    RPAD(str,len,pad)    | 字符串 pad 最右边进行填充，知道 str 长度为 len 个字符  |

### 日期和时间函数

#### 获取时间、日期

|           函数名           |          作用          |
| :------------------------: | :--------------------: |
| CURDATE() , CURRENT_DATE() | 返回当前日期（年月日） |
| CURTIME() , CURRENT_TIME() | 返回当前时间（时分秒） |
|     NOW() / SYSDATE()      |   系统当前日期和时间   |
|         UTC_DATE()         | 返回时间标准时间的日期 |
|         UTC_TIME()         | 返回世界标准时间的时间 |

#### 获取月份、星期、星期数、天数

|                  函数名                   |          作用          |
| :---------------------------------------: | :--------------------: |
|   YEAR(date) / MONTH(date) / DAY(date)    |  返回具体时间的日期值  |
| HOUR(time) / MINUTE(time) / SECOUND(time) | 返回具体时间的日时间值 |
|              MONTHNAME(date)              |     返回月份的英文     |
|               DAYNAME(date)               |     返回星期的英文     |
|               WEEKDAY(date)               | 返回星期几，从零开始计 |
|               QUARTER(date)               |   返回日期对应的季度   |
|       WEEK(date) , WEEKOFYEAR(date)       |   返回一年中的第几周   |
|              DAYOFYEAR(date)              |   返回一年中的第几天   |
|             DAYOFMONTH(date)              | 返回所在月份中的第几天 |
|              DAYOFWEEK(date)              | 返回星期几，从一开始计 |

#### 转换

|                 函数名                  |                   作用                   |
| :-------------------------------------: | :--------------------------------------: |
| UNIX_TIMESTAMP() , UNIX_TIMESTAMP(date) | 返回 date 的时间戳，不带参数返回当前时间 |
|         FROM_UNIXTIME(datetime)         |          将时间戳转换为普通格式          |

### 加密解密

|    函数名     |                           作用                            |
| :-----------: | :-------------------------------------------------------: |
| PASSWORD(str) |       返回字符串的41位的加密版本，加密结果不可逆。        |
|   MD5(str)    | 返回字符串的MD5加密后的值，参数为 `null` 时则返回 `null`. |
|   SHA(str)    |       返回字符串的加密后的值，sha 算法比 md5 更安全       |

## 多行函数

### 聚合函数

> 只列举常用的，数学体系中还有很多同概念的函数

|   函数名   |                 作用                 |
| :--------: | :----------------------------------: |
|   AVG(x)   |       平均值，只适用于数字类型       |
|   SUM(x)   |      相加的值，只适用于数字类型      |
|   MAX(x)   | 最大值，适用于字符串、数字、日期时间 |
|   MIN(x)   | 最小值，适用于字符串、数字、日期时间 |
| COUNT(key) |            计算出现的次数            |

```sql
# 所有学生的英语平均水平
SELECT AVG(english) AS averEnglish FROM student;

# 所有学生中英语分最高的
SELECT MAX(english) AS maxEnglish FROM student;

# 所有学生中英语分最低的
SELECT MIN(english) AS minEnglish FROM student;
```

#### `COUNT` 注意事项

* 列名为主键，`count(key)`会比`count(1)` 快。
* 列名不为主键，`count(1)`会比`count(key)` 快。
* 如果表多个列并且没有主键，则 `count(1)` 的执行效率优于 `count(*)`。
* 如果有主键，则 `count(key)` 的执行效率是最优的。
* 如果表只有一个字段，则 `count(key)` 最优。

## DCL

### 事务

> 也有人将事务从 DCL 中拆分，叫 TCL（其实就是三个关键字的缩写）

```sql

```

