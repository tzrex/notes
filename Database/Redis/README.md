### 是什么？

1. 远程字典服务（Remote Dictionary Server）
2. 基于内存亦可持久化的日志型、key-value数据库。很火爆的 `nosql` 数据库之一

### 能干什么

1. 持久化存储（ `RDB存储机制`、`AOF存储机制` ）
2. 地图信息分析
3. 计时器、计数器
4. 发布订阅系统
5. 用于高速缓存，缓解数据库压力

### 压力测试

<strong>小测一下</strong>：

```bash
# 100个并发 10w请求
redis-benchmark -h localhost -p 6379 100 -n 100000
```

<strong>查看测试打印信息</strong>：

```bash
====== 100 -n 100000 ======
  100000 requests completed in 1.46 seconds
  50 parallel clients
  3 bytes payload
  keep alive: 1
  
# 10w请求用时 1.46 秒
# 一个进程50并发
# 每次写入3个字节
# 处理并发的服务器 一台

97.57% <= 1 milliseconds
99.82% <= 2 milliseconds
99.97% <= 3 milliseconds
100.00% <= 3 milliseconds
68352.70 requests per second

# 请求在 3 毫秒内完成
# 预测每秒处理 68352.7 个请求
```

#### 常用参数

```bash
# redis-benchmark 的常用参数
-h		# 指定服务器主机名	默认为本地
-p		# 指定服务器端口	默认6379
-s		# 指定服务器的socket
-c		# 指定并发连接数量	默认50
-n		# 指定请求数			默认 1w
-d		# 以字节的形式指定 SET/GET 值的大小
-k		# 设置模式	1：keep alive	2：reconnect
-r		# SET/GET/INCR 使用随机key，SADD使用随机值
-P		# p 大写，通过管道传输 <numreq> 请求
-q		# 强制退出redis
--csv	# 以 CSV 格式输出
-t		# 简化打印内容
```

### 基础知识

#### 数据库默认16个

```bash
# 可通过 SELECT {index} 来切换数据库
SELECT 0	# 选择第一个数据库
DBSIZE		# 查看数据库大小
FLUSHDB		# 清空当前数据库数据
FLUSHALL	# 清空所有数据库数据
```

### 常用操作

```bash
ping					# 打印 PONG 表示已启动
EXISTS {name}			# 检查 key 是否存在
keys *					# 查询所有 key

get	{key}				# 获取某个 key
set	{key} {value}		# 设置某个 key	(默认为 string 类型)
mget {key} {key}		# 批量获取
mset {key}{value}		# 批量设置
getset {key}{value}		# 返回上一个值并设置新的值，（例如更新）
setex key "key" 20		# 创建key并设置20秒后过期。会覆盖
setnx key "hello" 20	# 创建key并设置20秒后过期。有则创建失败

EXPIRE {key} {time}		# 设置 key 的过期事件，单位秒
ttl {key}				# 查询过期时间
type {key}				# 检查 key 的类型
```

#### 字符串 ( String )

```shell
# 往字符串中追加数据		APPEND
# 如果没有 name 这个key则创建该key
APPEND name hello

# 加一减一				incr decr
set num 0
incr num				# 1	增加一
decr num				# 0 减少一

# 加N减N				incrby decrby
get num
incrby num 5			# 5 增加五
decrby num 3			# 2 减少三

# 字符串范围				getrange
set name hello,world
getrange name 0 3		# hell 获取字符串下表零到三的数据

# 替换字符串的值
get name
SETRANGE name 5 " "		# 将下表为5的数据替换为空格
get name				# "hello world"
```

#### 哈希 ( Hash )

#### 列表 ( List )

> 可以当作 `栈` 、`队列` 等数据结构进行操作。

```shell
# 获取 keys 列表的所有值
LRANGE keys 0 -1

# 列表添加数据
lpush keys one two three		# 数列keys左添加：lpush
rpush keys zero					# 数列keys右添加：rpush

# 列表删除数据（下标）
lpop keys						# 移除keys最左一个数据：lpop
rpop keys						# 移除keys最右一个数据：rpop
lpop keys 2						# 左移除2个数据
ltrim keys 0 4					# 保留下标0-4的数据，其余删掉

# 列表删除数据（key值）
# 精确删除，输入个数大于已有个数时删除以后的所有的
lrem keys 1 one					# 移除列表左数一个中名为one的数据

# 列表获取值
lindex keys 1					# 获取左数第二位的数据：lindex

# 获取列表长度
llen keys						# 获取keys列表的长度

# 列表修改值
lset keys 0 item				# 下标0修改为"item"，没有值则报错

# 列表中移动数据
rpoplpush keys newkeys			# 将列表中最后一数据移动到新的列表中
```



#### 集合 ( Set )

#### 有序集合 ( Zset )

### 特殊类型
