## 概念

### 是什么

* 非关系型数据库
* 设计的初衷时为了快速开发web应用
* 存储类型为文档结构：一种类似于 `JSON` 格式的结构

## 数据库操作

### 增加

```shell
# 使用 use 关键字可直接选择并使用数据库。
# 如果没有这个数据库，mogodb 会自动添加数据库
use dataOne
```

### 删除

```shell
# db.dropDatabase()
# 先选中数据库，可使用 use 关键字
use dataOne

# db 就是数据库变量
db.dropDatabase()
```

### 查看

```shell
# 查看当前权限下所有数据库
show dbs
```

### 修改名称

没有修改数据库名的操作！！如果确实需要修改库名，可先创建一个你心仪名称的数据库并将就数据库的数据导入进去。之后删除旧数据库。

### 备份

```shell
# 语法：mongodump -h {host} -d {databaseName} -o {copyPath}
# 例如：将本地 mogodb 中的 test 数据库拷贝到 /home/backup/mongo
mongodump -h 127.0.0.1:27017 -d test -o /home/backup/mongo
```

### 恢复



## 表操作

### 增加

#### 注意

* 如果表不存在，将会自动添加表，再添加数据
* 会自动生成主键：`_id` 及其 `hash` 值

### 修改

### 删除

### 查找

