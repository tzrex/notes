## pip使用
通常 `pip` 是对应的 `python@2.x`的保管流工具。
但是通过 `pip3` 更新 `pip` 之后会直接指向 `python@3.x`

### 基本命令
* 下载包
    * `pip install {packageName}`
    * `pip install -r requeirment.txt` 可通过读取配置文件批量下安装包
* 查看已安装的包：`pip list` 或者 `pip freeze`
